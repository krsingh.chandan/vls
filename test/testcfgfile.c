// Test des fonctions de gestion des fichiers de config
// A compiler par 'make -f Makefile.cfgfile'

#include <pthread.h>
#include <stdio.h>
#include "../core/config.h"
#include "../core/common.h"
#include "../core/string.h"
#include "../core/log.h"
#include "../core/cfgfile.h"
#include "../core/debug.h"

// Global logging endpoint
C_Log* pLog = NULL;

int main()
{
  int iRc = 0;

  // Application initialisation
  handle hLog;
  pLog = new C_Log();
  printf("New log object created\n");

  pLog->Init("Testcfgfile.log");
  printf("Log object initiated\n");

  int iFlags = 0 | LOG_DBGMSG | LOG_SCR | LOG_FILE;
  hLog = pLog->Register("TestLog", iFlags);
  printf("Registered Testlog\n");


  // Tests of the cfg file
  printf("!!!!!!!!!!!!!!!!!! BEGIN !!!!!!!!!!!!!!!!\n");
  C_CfgFile CfgFile(hLog);
  CfgFile.Open("Test.conf");

  CfgFile.FindSection("Globals");
  C_String strVarName, strVarVal;
  CfgFile.Read(&strVarName, &strVarVal);
  strVarName.Display();
  strVarVal.Display();

  CfgFile.FindSection("Interfaces");
  CfgFile.FindEntry("VLANdefault", &strVarVal);
  strVarVal.Display();

  CfgFile.Close();
  printf("!!!!!!!!!!!!!!!!!!! END !!!!!!!!!!!!!!!!!\n");

  // Application termination
  pLog->Unregister(hLog);
  printf("TestLog unregistered\n");

  pLog->End();
  printf("Log ended\n");

  delete pLog;
  printf("Log deleted\n");

  return 0;
}
