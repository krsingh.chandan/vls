#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>


int main()
{
  int iSockFd;
  int iSockOpts;
  unsigned long inaddr;
  struct sockaddr_in saSrc, saDst;
  socklen_t iSrcLen, iDstLen;

  saSrc.sin_family = AF_INET;
  saSrc.sin_port = 8888;
  inaddr = inet_addr("138.195.143.250");          /* Use eth1 to send packets */
  memcpy(&saSrc.sin_addr, &inaddr, sizeof(inaddr));
  iSrcLen = sizeof(saSrc);

  saDst.sin_family = AF_INET;
  saDst.sin_port = 9999;
  inaddr = inet_addr("239.0.0.1");           /* Dst = all machines on the LAN */
  memcpy(&saDst.sin_addr, &inaddr, sizeof(inaddr));
  iDstLen = sizeof(saDst);

  iSockFd = socket(AF_INET, SOCK_DGRAM, 0);
  if (iSockFd < 0)
    printf("Socket error: %s\n", strerror(errno));

  iSockOpts = 1;
  if (setsockopt(iSockFd, SOL_SOCKET, SO_BROADCAST, &iSockOpts,
      sizeof(iSockOpts)) < 0)
    printf("Setsockopt error: %s\n", strerror(errno));

  if (setsockopt(iSockFd, SOL_SOCKET, SO_DONTROUTE, &iSockOpts,
      sizeof(iSockOpts)) < 0)
    printf("Setsockopt error: %s\n", strerror(errno));

  if (bind(iSockFd, (struct sockaddr*)&saSrc, iSrcLen))
    printf("Bind error: %s\n", strerror(errno));

  if (connect(iSockFd, (struct sockaddr*)&saDst, iDstLen))
    printf("Connect error: %s\n", strerror(errno));

  if (send(iSockFd, "toto\n", 5, 0))
    printf("Send error: %s\n", strerror(errno));

  return 0;
}
