// Test des fonctions de buffering
// A compiler par 'make -f Makefile.buffer'

#include <pthread.h>
#include <stdio.h>
#include "../core/config.h"
#include "../core/common.h"
#include "../core/debug.h"
#include "../core/string.h"
#include "../core/log.h"
#include "../core/ts.h"
#include "../core/buffer.h"

// Global logging endpoint
C_Log* pLog = NULL;

int main()
{
  int iRc = 0;

  // Application initialisation
  handle hLog;
  pLog = new C_Log();
  printf("New log object created\n");

  pLog->Init("Testbuffer.log");
  printf("Log object initiated\n");

  int iFlags = 0 | LOG_DBGMSG | LOG_SCR | LOG_FILE;
  hLog = pLog->Register("TestLog", iFlags);
  printf("Registered Testlog\n");


  // Tests of the buffers
  printf("!!!!!!!!!!!!!!!!!! BEGIN !!!!!!!!!!!!!!!!\n");
  C_TsPacket Essai;
  C_Fifo test(5);

  while(!iRc)
  {
    iRc = test.Push(&Essai);
    printf("Push ");
  }
  printf("\n");

  C_TsPacket* pPacket = test.Pull();
  iRc = test.Push(&Essai);
  printf("%p %d\n", pPacket, iRc);

  printf("!!!!!!!!!!!!!!!!!!! END !!!!!!!!!!!!!!!!!\n");

  // Application termination
  pLog->Unregister(hLog);
  printf("TestLog unregistered\n");

  pLog->End();
  printf("Log ended\n");

  delete pLog;
  printf("Log deleted\n");

  return 0;
}
