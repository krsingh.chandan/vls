// Test des fonctions de log
// A compiler par 'make -f Makefile.log'

#include <pthread.h>
#include <stdio.h>
#include "../core/config.h"
#include "../core/common.h"
#include "../core/debug.h"
#include "../core/string.h"
#include "../core/log.h"

// Global logging endpoint
C_Log* pLog = NULL;

int main()
{
  handle hLog;

  pLog = new C_Log();
  printf("New log object created\n");

  pLog->Init("Testlog.log");
  printf("Log object initiated\n");

  int iFlags = 0 | LOG_DBGMSG | LOG_SCR | LOG_FILE;
  hLog = pLog->Register("TestLog", iFlags);
  printf("Registered Testlog\n");

  Log(hLog, LOG_NOTE, "Test1: %d, %s, %p", 1, "toto", pLog);
  Log(hLog, LOG_WARN, "Test2: %d, %s", 2, "coucou");
  Log(hLog, LOG_ERROR, "Test3: %d, %s", 3, "lalalere");
  printf("Logged (1 & 2 & 3)\n");

  LogDbg(hLog, "Test3: %d, %s %p", 1, "essai", pLog);

  pLog->Unregister(hLog);
  printf("TestLog unregistered\n");

  pLog->End();
  printf("Log ended\n");

  delete pLog;
  printf("Log deleted\n");

  return 0;
}
