/*******************************************************************************
* converter.cpp: Mpeg converters
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"

#include "mpeg.h"
#include "ts.h"

#include "../server/buffer.h"
#include "../server/program.h"
#include "../server/broadcast.h"
#include "../server/request.h"
#include "../server/input.h"

#include "reader.h"
#include "converter.h"



/*******************************************************************************
* C_MpegConverter
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_MpegConverter::C_MpegConverter(C_Module* pModule,
                                 C_MpegConverterConfig& cConfig)
{
  ASSERT(pModule);
  ASSERT(cConfig.m_hLog);
  ASSERT(cConfig.m_pBroadcast);
  ASSERT(cConfig.m_pReader);
  ASSERT(cConfig.m_pTsProvider);
  ASSERT(cConfig.m_pEventHandler);

  m_pModule             = pModule;
  m_hLog                = cConfig.m_hLog;

  m_pBroadcast          = cConfig.m_pBroadcast;
  m_pReader             = cConfig.m_pReader;
  m_pTsProvider         = cConfig.m_pTsProvider;
  m_pEventHandler       = cConfig.m_pEventHandler;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_MpegConverter::~C_MpegConverter()
{
  m_pModule->Unref();
  delete m_pReader;
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_MpegConverter::InitWork()
{
  m_pReader->Init();
}


//------------------------------------------------------------------------------
// Stop request
//------------------------------------------------------------------------------
void C_MpegConverter::StopWork()
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_MpegConverter::CleanWork()
{
  m_pReader->Close();
}

//------------------------------------------------------------------------------
// Get Next Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_MpegConverter::GetNextTsPackets(C_Fifo<C_TsPacket> *, int)
{
  return MPEG_ENDOFSTREAM;
}

//------------------------------------------------------------------------------
// Get Previous Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_MpegConverter::GetPrevTsPackets(C_Fifo<C_TsPacket> *, int)
{
  return MPEG_BEGINOFSTREAM;
}
