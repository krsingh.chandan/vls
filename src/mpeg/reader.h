/*******************************************************************************
* reader.h: Stream readers
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: reader.h,v 1.11 2003/08/13 23:04:22 adq Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Common interface for stream readers (C_MpegReader).
*******************************************************************************/


#ifndef _MPEG_READER_H_
#define _MPEG_READER_H_


//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------
#define MPEG_BEGINOFSTREAM      FILE_BOF
#define MPEG_ENDOFSTREAM        FILE_EOF
#define MPEG_STREAMERROR        FILE_ERR


class C_MpegConverter;


//------------------------------------------------------------------------------
// C_MpegReader class
//------------------------------------------------------------------------------
// Base class
//------------------------------------------------------------------------------
class C_MpegReader
{
public:
  C_MpegReader(C_Module* pModule, C_Broadcast* pBroadcast);
  virtual ~C_MpegReader();

  void SetConverter(C_MpegConverter* pConverter)
  { m_pConverter = pConverter; };

  virtual void Init() = 0;
  virtual void Close() = 0;

  virtual s64 Read(byte* pBuff, s64 iSize) = 0;
  virtual s64 Seek(s64 iOffset, s64 bStartPos) = 0;

  virtual int GetFrame(byte ** pBuff ,int iSize) = 0;
  virtual int GetAudioFD() = 0; /* for V4L only */     

  virtual s64 Size() = 0;
  virtual s64 GetPos() = 0;

  bool HasDiscontinuity()
  { return m_bDiscontinuity; };
  void ResetDiscontinuity()
  { m_bDiscontinuity = false; };

  virtual bool EndOfStream()
  { return m_bEndOfStream; };
  virtual void ResetEndOfStream()
  { /* By default, this is not supported */ };
   
protected:
  C_Module* m_pModule;

  C_Broadcast* m_pBroadcast;

  C_MpegConverter* m_pConverter;

  bool m_bDiscontinuity;

  bool m_bEndOfStream;
   
  handle m_hLog;
};


// Declaration and implementation of C_MpegReaderModule which has to be
// inherited by each input module => C_????MpegReaderModule
DECLARE_VIRTUAL_MODULE(MpegReader, "mpegreader", C_Broadcast*);


#else
#error "Multiple inclusions of reader.h"
#endif

