/*******************************************************************************
* tsdemux.h: TS demultiplexer
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* The C_TsDemux class is a generic TS demultiplexer.
*
* Input: TS packets ("HandlePacket" method of I_TsPacketHandler)
*
* Output: TS packets to the Targets which have selected PIDs
*
* To select a PID, an object has to implement the I_TsPacketHandler
* interface and call the "SelectPid" method.
* To unselect a PID, an object has to call the "UnselectPid" method.
*
* When a PID is first selected, the demultiplexer calls the "OnSelectPid"
* method so that a derivated class can do hardware stuff.
* When a PID is no more selected, the demultiplexer calls the "OnUnselectPid"
* method for the same reason.
*
* PIDs can be selected or unselected at any time in the same thread.
*
* If another thread wants to avoid demultiplexing for a _short_ time he just
* has to lock and unlock it with the two methods "Lock" and "UnLock".
*
*******************************************************************************/


#ifndef _TSDEMUX_H_
#define _TSDEMUX_H_


//------------------------------------------------------------------------------
// C_TsDemux
//------------------------------------------------------------------------------
class C_TsDemux : public I_TsPacketHandler
{
public:
  C_TsDemux(C_NetList *pTsProvider);
  virtual ~C_TsDemux();

  void SelectPid(I_TsPacketHandler *pTarget, u16 iPid, u8 iType);
  void UnselectPid(I_TsPacketHandler *pTarget, u16 iPid);

  // Demultiplexing lock/unlock
  void Lock();
  void UnLock();

  virtual bool HandlePacket(C_TsPacket* pPacket);
  virtual bool HandlePrefillPacket(C_TsPacket*) { return false; }
  virtual void Shutdown() {}
  virtual void PrefillStart() {}
  virtual void PrefillComplete() {}

protected:
  // Selection hooks
  virtual void OnSelectPid(u16, u8) {};
  virtual void OnUnselectPid(u16) {};

  // Where to get TS packets
  C_NetList *m_pTsProvider;

private:
  // multithread lock
  C_Mutex m_cLock;

  // Interfaces which have selected PIDs
  C_HashTable<u16, C_Vector<I_TsPacketHandler> > m_cTargets;

  // Data protection
  u16 m_iHandledPid;
  C_Vector<I_TsPacketHandler> m_vPendingTargets;
};


#else
#error "Multiple inclusions of tsdemux.h"
#endif

