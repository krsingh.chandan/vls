/*******************************************************************************
* pes.h: PES facilities
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: pes.h,v 1.3 2003/06/02 20:48:09 jpsaman Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*          Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _PES_H_
#define _PES_H_

#define PES_MAX_SIZE 65536

typedef enum { NONE       =-1,
               FORBIDDEN  = 0,
               I_FRAME    = 1,
               P_FRAME    = 2,
               B_FRAME    = 3,
               NOT_USED   = 4,
               RESERVED_1 = 5,
               RESERVED_2 = 6,
               RESERVED_3 = 7 } pes_frame_t;

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_PesPacket
{
 public:
  C_PesPacket();
  C_PesPacket(C_TsPacket *pPacket);
  ~C_PesPacket();
  
  // Alternative ways to init PES packet
  void SetTsPacket(C_TsPacket *pPacket);

  // Direct access to the buffer. Provided only for efficiency reasons
  operator byte*();
  
  // By hand packet header construction
  u8 BuildPacket(u64 Pts, u8 iPayloadType, byte * Payload, unsigned int iPayloadSize);

  // Extract information from PES packet.
  pes_frame_t GetFrameType();

  unsigned int GetSize() {return m_iPesSize;};
  // Reference counter
  unsigned int Ref()
  {
    ASSERT(m_iRefCount + 1);
    return ++m_iRefCount;
  };
  unsigned int Unref()
  {
    ASSERT(m_iRefCount);
    return --m_iRefCount;
  };
  unsigned int RefCount() const
  {
    return m_iRefCount;
  }

 protected:
  C_TsPacket *m_pTsPacket;

  byte * bData;

  unsigned int m_iPesSize;
  unsigned int m_iHeaderSize;
  
  unsigned int m_iRefCount;
};

#else
#error "Multiple inclusions of pes.h"
#endif

