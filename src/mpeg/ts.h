/*******************************************************************************
* ts.h: TsPacket class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Jean-Paul Saman <jpsaman@wxs.n>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _TS_H_
#define _TS_H_


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_TsPacket
{
 public:
  C_TsPacket() { m_iRefCount = 0; };
  ~C_TsPacket();
  
  // Direct access to the buffer. Provided only for efficiency reasons
  operator byte* ();
  
  // By hand packet header construction
  u8 BuildHeader(u16 iPid, bool bUnitStart, u8 iCounter);
  u8 BuildAdaptionField(u64 iPCR);
  u8 AddStuffingBytes(u8 iPayloadLen);
  u8 FillWith(byte * pBuff, int iBeginning, int iEnd);

  // Packet modifications
  void IncrementCounter();
  void SetErrorFlag(bool bError = true);
  bool SetDiscontinuityFlag(bool bDiscontinuity = true);

  // Packet decoding
  u16 GetPid() const;
  bool HasPCR() const;
  u16 GetPMTPid(int number);
  bool IsDiscontinuity() const;
  bool HasDiscontinuityFlag() const;
  s64 GetPCRTime() const;
  byte* GetPayloadStart();
  u8 GetPayloadLength() const;
  bool HasPayloadStartIndicator() const;
  bool HasContinuityCounter() const;
  bool HasAdaptationField() const;
  u8 GetAdaptationFieldLength() const;
  u8 GetContinuityCounter() const;
  u8 SetContinuityCounter(int old_counter);
  bool IsNullPacket() const;

  // Reference counter
  unsigned int Ref()
  {
    ASSERT(m_iRefCount + 1);
    return ++m_iRefCount;
  };
  unsigned int Unref()
  {
    ASSERT(m_iRefCount);
    return --m_iRefCount;
  };
  unsigned int RefCount() const
  {
    return m_iRefCount;
  }

 protected:
  byte bData[TS_PACKET_LEN];

  unsigned int m_iRefCount;
};


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class I_TsPacketHandler
{
public:
  virtual bool HandlePacket(C_TsPacket* pPacket) = 0;
  virtual bool HandlePrefillPacket(C_TsPacket* pPacket) = 0;
  virtual void Shutdown() = 0;
  virtual void PrefillStart() = 0;
  virtual void PrefillComplete() = 0;
  virtual ~I_TsPacketHandler(void) { }


  bool operator == (const I_TsPacketHandler& cHandler) const
  {
    return (this == &cHandler);
  };
};


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// The section must fit in a single TS packet and its data are put immediately
// after the TS header
// The section_syntax_indicator field is set to 1 so if this class is used to
// build a private section the section must follow the generic syntax beyond
// the private_section_length field
//------------------------------------------------------------------------------
class C_PsiSection : protected C_TsPacket
{
 public:
  C_PsiSection();

  // Guess :)
  u8 BuildHeader(u16 iPid, u8 iCounter, u8 iTableId, u16 iPsiId, u8 iVersion,
                 bool bCurrentNext, u8 iCurrentSection, u8 iLastSection);

  // Append data at the end of the section
  inline byte* AppendData(u8 iDataLen);
  // Modify the data at the given position in the psi section
  inline byte* GetDataForUpdate(u8 iDataPos);
  // Read the data at the given position
  inline const byte* GetDataForRead(u8 iDataPos) const;

  //
  inline void Finalize();

  //
  u8 GetVersion();

 protected:
  void UpdateVersion();

  void BuildCrc32Table();
  inline void ComputeCrc32();

  inline void AddFinalStuffing();

 private:
  u32 iCrc32Table[256];
  u8 m_iPsiStart;
  bool m_bIsModified;
};


//------------------------------------------------------------------------------
// At that time, the complete PAT must fit in a single TS packet
//------------------------------------------------------------------------------
class C_Pat : protected C_PsiSection
{
 public:
  // Build the PAT header
  void BuildHeader(u16 iStreamId, u8 iVersion, bool bCurrentNext = true);

  // Add a program to the list of pgrms carried in the stream
  int AddPgrm(u16 iPgrmNumber, u16 iPmtPid);
  // Remove a program from the list of pgrms carried in the stream
  //int RemovePgrm(u16 iPgrmNumber);
  // Remove all the programs from the PAT
  //void EmptyPat();

  // Write the PAT in the given TS packet. Increment the continuity counter
  // at each call
  void Write(C_TsPacket* pPacket);
};


//------------------------------------------------------------------------------
// At that time, the complete PMT must fit in a single TS packet
//------------------------------------------------------------------------------
class C_Pmt : protected C_PsiSection
{
 public:
  // Build the PMT header
  void BuildHeader(u16 iPid, u16 iPgrmNumber, u16 iPcrPid, u8 iVersion,
                   bool bCurrentNext = true);

  u16 GetPcrPid() const;
  void UpdatePcrPid(u16 iNewPid);
  
  // Add a descriptor for the program. Must be called before any AddEs()
  int AddPgrmDescriptor(/*C_Descriptor* pDescriptors*/);

  // Add an elementary stream for that pgrm
  int AddEs(u8 iType, u16 iPid/*, const C_Vector<C_Descriptor*>& cDescriptors*/);

  // Remove an elementary stream for that pgrm
  //void RemoveES(u16 iPid);
  
  // Remove all the descriptors and the ES from the PMT 
  void EmptyPmt();
  
  // Write the PMT in the given TS packet. Increment the continuity counter
  // at each call
  void Write(C_TsPacket* pPacket);

 private:
  bool m_bIsModified;
};


#else
#error "Multiple inclusions of ts.h"
#endif

