/*******************************************************************************
* tsdemux.cpp: TS demultiplexer
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: tsdemux.cpp,v 1.3 2003/10/27 10:58:11 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"
#include "../core/core.h"

#include "mpeg.h"
#include "ts.h"

#include "../server/buffer.h"

#include "tsdemux.h"


//******************************************************************************
// C_TsDemux class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_TsDemux::C_TsDemux(C_NetList *pTsProvider) : m_vPendingTargets(20, NO)
{
  m_pTsProvider = pTsProvider;
  m_iHandledPid = 0xffff;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_TsDemux::~C_TsDemux()
{
}


//------------------------------------------------------------------------------
// PID selection
//------------------------------------------------------------------------------
void C_TsDemux::SelectPid(I_TsPacketHandler *pTarget, u16 iPid, u8 iType)
{
  ASSERT(!(iPid & ~0x1fff));
  C_Vector<I_TsPacketHandler> *pVector = m_cTargets.Get(iPid);

  if(!pVector)
  {
    unsigned int iGrowthFactor = 2;
    // The PAT may be selected a lot of time
    if(iPid == 0x0000)
      iGrowthFactor = 20;

    pVector = new C_Vector<I_TsPacketHandler>(iGrowthFactor, NO);
    ASSERT(pVector);

    m_cTargets.Add(iPid, pVector);

    // Hook
    OnSelectPid(iPid, iType);
  }

  if(pVector->Find(*pTarget) < 0)
  {
    pVector->Add(pTarget);

    // Update the pending targets if we're handling a packet (reentrance)
    if(m_iHandledPid == iPid)
      m_vPendingTargets.Add(pTarget);
  }
}


//------------------------------------------------------------------------------
// PID unselection
//------------------------------------------------------------------------------
void C_TsDemux::UnselectPid(I_TsPacketHandler *pTarget, u16 iPid)
{
  ASSERT(!(iPid & ~0x1fff));
  C_Vector<I_TsPacketHandler> *pVector = m_cTargets.Get(iPid);

  if(pVector)
  {
    int iIndex = pVector->Find(*pTarget);

    if(iIndex >= 0)
    {
      pVector->Remove(iIndex);
      // Free the vector if it's empty
      if(!pVector->Size())
      {
        m_cTargets.Delete(iPid);

        // Hook
        OnUnselectPid(iPid);
      }
    }
  }
  // Update the pending targets if we're handling a packet (reentrance)
  if(m_iHandledPid == iPid)
  {
    int iIndex = m_vPendingTargets.Find(*pTarget);
    if(iIndex >= 0)
      m_vPendingTargets.Remove(iIndex);
  }
}


//------------------------------------------------------------------------------
// Demultiplexing lock
//------------------------------------------------------------------------------
void C_TsDemux::Lock()
{
  m_cLock.Lock();
}


//------------------------------------------------------------------------------
// Demultiplexing unlock
//------------------------------------------------------------------------------
void C_TsDemux::UnLock()
{
  m_cLock.UnLock();
}


//------------------------------------------------------------------------------
// Input of the demux
//------------------------------------------------------------------------------
bool C_TsDemux::HandlePacket(C_TsPacket* pPacket)
{
  m_cLock.Lock();

  ASSERT(m_iHandledPid == 0xffff);
  m_iHandledPid = pPacket->GetPid();
  C_Vector<I_TsPacketHandler> *pTargets = m_cTargets.Get(m_iHandledPid);

  if(pTargets)
  {
    // Protect the access to the vector which may be deleted
    for(unsigned int i = 0; i < pTargets->Size(); i++)
      m_vPendingTargets.Add(&(*pTargets)[i]);

    while(m_vPendingTargets.Size() != 0)
    {
      unsigned int iEnd = m_vPendingTargets.Size() - 1;
      I_TsPacketHandler *pHandler = &m_vPendingTargets[iEnd];

      m_pTsProvider->RefPacket(pPacket);
      pHandler->HandlePacket(pPacket);
      m_vPendingTargets.Remove(iEnd);
    }
  }

  m_pTsProvider->ReleasePacket(pPacket);

  m_iHandledPid = 0xffff;

  m_cLock.UnLock();

  return true;
}


