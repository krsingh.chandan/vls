/*******************************************************************************
* tsmux.h: TS multiplexer
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* The C_TsMux is a class which can select one or more programs of a TS stream.
*
* First it has to be attached to a TS demultiplexer (C_TsDemux) by calling
* the "Attach" method. The "Detach" method is called to reset the multiplexer.
*
* The "AttachProgram" method selects the whole program identified by its
* program_number and its PMT_PID.
* The "DetachProgram" method unselects the whole program identified by its
* PMT_PID.
*
* The multiplexer sends the stream to an object which should implement the
* I_TsPacketHandler interface. The PAT of the whole stream is generated
* so that the stream is valid on the MPEG2 view point.
*
* The C_TsMuxPmtDecoder class is used internally by a C_TsMux object.
*
*******************************************************************************/


#ifndef _TSMUX_H_
#define _TSMUX_H_


class C_TsMux;


//------------------------------------------------------------------------------
// C_TsMuxPmtDecoder
//------------------------------------------------------------------------------
class C_TsMuxPmtDecoder : public C_DvbPsiPmtDecoder, public I_DvbPsiPmtHandler
{
public:
  C_TsMuxPmtDecoder(C_NetList *pTsProvider, C_TsMux *pMux, u16 iProgramNumber);
  virtual ~C_TsMuxPmtDecoder();

  void Detach();
  uint16_t GetPnr() {return uiPnr;}

protected:
  virtual void OnDvbPsiPmtEvent(int iEvent);

private:
  // Internal data
  C_TsMux *m_pMux;
  uint16_t uiPnr;
};


//------------------------------------------------------------------------------
// C_TsMux
//------------------------------------------------------------------------------
class C_TsMux : public I_TsPacketHandler
{
  friend class C_TsMuxPmtDecoder;
public:
  C_TsMux(C_NetList *pTsProvider, C_TsDemux *pDemux,
          I_TsPacketHandler *pHandler);
  virtual ~C_TsMux();

  void Attach();
  void Detach();

  void AttachProgram(u16 iProgramNumber, u16 iPmtPid);
  void DetachProgram(u16 iPmtPid);

  virtual bool HandlePacket(C_TsPacket* pPacket);
  virtual bool HandlePrefillPacket(C_TsPacket*) { return false; }
  virtual void Shutdown() {}
  virtual void PrefillStart() {}
  virtual void PrefillComplete() {}

protected:
  // PMT PID -> PMT decoder
  C_HashTable<u16, C_TsMuxPmtDecoder> m_cPmtDecoders;

private:
  // Internal data
  C_NetList *m_pTsProvider;
  C_TsDemux *m_pDemux;
  I_TsPacketHandler *m_pHandler;

  C_DvbPsiPat m_cPat;
  bool m_bSendPat;
};


#else
#error "Multiple inclusions of tsmux.h"
#endif

