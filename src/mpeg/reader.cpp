/*******************************************************************************
* reader.cpp: Stream readers
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: reader.cpp,v 1.5 2003/10/27 10:58:11 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"

#include "mpeg.h"
#include "ts.h"

#include "../server/buffer.h"
#include "../server/broadcast.h"
#include "../server/request.h"

#include "reader.h"
#include "converter.h"

/*******************************************************************************
* C_MpegReader
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Construuctor
//------------------------------------------------------------------------------
C_MpegReader::C_MpegReader(C_Module* pModule, C_Broadcast* pBroadcast)
{
  ASSERT(pModule);
  ASSERT(pBroadcast);

  m_pModule = pModule;
  m_pBroadcast = pBroadcast;

  m_hLog = pModule->GetLogger();

  m_bDiscontinuity = false;
  m_bEndOfStream = false;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_MpegReader::~C_MpegReader()
{
  m_pModule->Unref();
}


