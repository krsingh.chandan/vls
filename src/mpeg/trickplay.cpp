/*******************************************************************************
* trickplay.cpp: base class to control trickplay status
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: trickplay.cpp,v 1.5 2003/10/27 10:58:11 sam Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"

#include "mpeg.h"
#include "ts.h"

#include "../server/buffer.h"
#include "../server/program.h"
#include "../server/broadcast.h"
#include "../server/request.h"
#include "../server/input.h"

#include "reader.h"
#include "trickplay.h"
#include "converter.h"

/*******************************************************************************
* C_TrickPlay : Interface to trickplay between converter and  SyncFifo
********************************************************************************
*
*******************************************************************************/
C_TrickPlay::C_TrickPlay(C_Module* pModule, C_TrickPlayConfig& cConfig)
{
  ASSERT(pModule);
  ASSERT(cConfig.m_hLog);
  ASSERT(cConfig.m_pBroadcast);
  ASSERT(cConfig.m_pHandler);
  ASSERT(cConfig.m_pEventHandler);
  ASSERT(cConfig.m_pTsProvider);
  ASSERT(cConfig.m_pConverter);

  m_pModule             = pModule;
  m_hLog                = cConfig.m_hLog;
  m_pBroadcast          = cConfig.m_pBroadcast;
  m_pHandler            = cConfig.m_pHandler;
  m_iInitFill           = cConfig.m_iInitFill;
  m_pEventHandler       = cConfig.m_pEventHandler;
  m_pTsProvider         = cConfig.m_pTsProvider;
  m_pConverter          = cConfig.m_pConverter;
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_TrickPlay::~C_TrickPlay()
{
  if (m_pModule) m_pModule->Unref();
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_TrickPlay::InitWork()
{
  m_iRequestTrickPlayStatus = m_iTrickPlayStatus = TRICKPLAY_PLAY;
  m_bStop = false;
  m_pConverter->InitWork();
}


//------------------------------------------------------------------------------
// Stop request
//------------------------------------------------------------------------------
void C_TrickPlay::StopWork()
{
  m_iRequestTrickPlayStatus = TRICKPLAY_STOP;
  m_cResumeCond.Protect();
  m_cResumeCond.Signal();
  m_bStop = true;
  Interrupt();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TrickPlay::CleanWork()
{
  m_pConverter->CleanWork();
}

//------------------------------------------------------------------------------
// Resume streaming
//------------------------------------------------------------------------------
void C_TrickPlay::Resume()
{
  m_iRequestTrickPlayStatus = TRICKPLAY_PLAY;
}


//------------------------------------------------------------------------------
// Suspend streaming
//------------------------------------------------------------------------------
void C_TrickPlay::Suspend()
{
  m_iRequestTrickPlayStatus = TRICKPLAY_PAUSE;
}

//------------------------------------------------------------------------------
// Forward Trickplay streaming
//------------------------------------------------------------------------------
void C_TrickPlay::Forward(int speed)
{
  m_iRequestTrickPlayStatus = TRICKPLAY_FORWARD;
  m_iRequestedSpeed = speed;
}

//------------------------------------------------------------------------------
// Rewind Trickplay streaming
//------------------------------------------------------------------------------
void C_TrickPlay::Rewind(int speed)
{
  m_iRequestTrickPlayStatus = TRICKPLAY_REWIND;
  m_iRequestedSpeed = speed;
}


//------------------------------------------------------------------------------
// Return current active trickplay status
//------------------------------------------------------------------------------
trickplay_status_t C_TrickPlay::CurrentStatus()
{
  return m_iTrickPlayStatus;
}

trickplay_status_t C_TrickPlay::RequestedStatus()
{
  return m_iRequestTrickPlayStatus; 
}
