/*******************************************************************************
* dvbinput.h: DVB Input class definition
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: dvbinput.h,v 1.16 2003/09/27 21:10:05 nitrox Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef _DVB_INPUT_H_
#define _DVB_INPUT_H_


//------------------------------------------------------------------------------
// C_LocalInput class
//------------------------------------------------------------------------------
class C_DvbInput : public C_Input, public C_EventHandler,
                   public C_TsDemux, public I_DvbPsiPatHandler
{
public:
  C_DvbInput(C_Module* pModule, const C_String& strName);
  ~C_DvbInput();

  virtual C_String GetType() {return C_String("dvb");}
protected:
  // Init/termination methods
  virtual void OnInit();
  virtual void OnDestroy();

  // Program management
  virtual void OnStartStreaming(C_Broadcast* pBroadcast);
  virtual void OnResumeStreaming(C_Broadcast* pBroadcast);
  virtual void OnSuspendStreaming(C_Broadcast* pBroadcast);
  virtual void OnForwardStreaming(C_Broadcast* pBroadcast,int speed);
  virtual void OnRewindStreaming(C_Broadcast* pBroadcast, int speed);
  virtual void OnStopStreaming(C_Broadcast* pBroadcast);
  virtual void OnUpdateProgram(C_String strProgram,
                                     C_String strFileName, C_String strType);
  virtual void OnDeleteProgram(C_String strProgram);
  virtual C_List<C_Program> OnGetAvailablePgrms();

  // Hardware specific PID selection (demux)
  virtual void OnSelectPid(u16 iPid, u8 iType);
  virtual void OnUnselectPid(u16 iPid);

  // The input is the event handler of its converter
  virtual void HandleEvent(const C_Event& cEvent);

  // PAT event handler
  virtual void OnDvbPsiPatEvent(int iEvent);

private:
  //main object
  DVB* dvb;

  // devices
  C_String m_strDVR;
  C_String m_strDEMUX;
  C_String m_strVIDEO;

  bool m_bHasDecoder;
  bool m_bIgnoreTimeout;
  bool m_bIgnoreMissing;
   
  C_Vector<C_String>  m_vProgramNames;
  int m_iFrequency;
  int m_iPolarization;
  int m_iSymbolRate;

  int m_iDiSEqC;
  int m_iLnbLof1;
  int m_iLnbLof2;
  int m_iLnbSLof;

  int m_iSendMethod;
  
  // Kludge: signal the first PAT arrival.
  int m_iDemuxUsageCount;
  int m_iGotTpid;
  C_Mutex m_cDemuxUsageM;
  C_Condition m_cEndInit;

  // Demuxes' file descriptors
  int m_iDemuxes[512];

  // Only one netlist for all the programs
  C_NetList m_cTsProvider;

  // Input stream (demux)
  C_Program m_cInputProgram;
  C_Broadcast m_cInputBroadcast;
  C_MpegReader *m_pReader;
  C_MpegConverter *m_pConverter;

  // TrickPlay Module
  C_TrickPlay *m_pTrickPlay;
  C_String m_strTrickPlayType;
  
  // PAT decoder
  C_DvbPsiPatDecoder m_cPatDecoder;

  // PAT (access protected)
  C_Mutex m_cLock;
  C_DvbPsiPat m_cCurrentPat;

  // Muxes and streamers
  C_HashTable<u16, C_TsMux> m_cMuxes;
  C_HashTable<u16, C_TsStreamer> m_cStreamers;
};


// Declaration and implementation of C_LocalInputModule
DECLARE_MODULE(Dvb, Input, "dvb", const C_String&);


#else
#error "Multiple inclusions of dvbinput.h"
#endif

