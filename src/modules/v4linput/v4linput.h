/*******************************************************************************
* v4linput.h: eading from a v4l device (WebCam, tv tuner)
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: v4linput.h,v 1.4 2003/09/10 12:57:52 nitrox Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/

#ifndef _V4L_INPUT_H_
#define _V4L_INPUT_H_

//------------------------------------------------------------------------------
// C_v4lInput class
//------------------------------------------------------------------------------
class C_v4lInput : public C_Input
{
public:
  C_v4lInput(C_Module* pModule, const C_String& strName);
  ~C_v4lInput();

  virtual C_String GetType() {return C_String("v4l");}
protected:
  // Init/termination methods
  virtual void OnInit();
  virtual void OnDestroy();

  // Selection of the pgrms to broadcast
  virtual void OnStartStreaming(C_Broadcast* pBroadcast);
  virtual void OnResumeStreaming(C_Broadcast* pBroadcast);
  virtual void OnSuspendStreaming(C_Broadcast* pBroadcast);
  virtual void OnForwardStreaming(C_Broadcast* pBroadcast, int speed) {};
  virtual void OnRewindStreaming(C_Broadcast* pBroadcast, int speed) {};
  virtual void OnStopStreaming(C_Broadcast* pBroadcast);
  virtual void OnUpdateProgram(C_String strProgram,C_String strFileName, C_String strType);
  virtual void OnDeleteProgram(C_String strProgram);
  virtual C_List<C_Program> OnGetAvailablePgrms();

private:
  // Directory where to look for the config file
  C_String m_strConfigPath;
  // Directory where to look for files in
  C_String m_strFilesPath;

  // Programs names
  // the program ID is m_vProgramNames.Find(strName) + 1
  C_Vector<C_String> m_vProgramNames;

  // List of currently running readers and streamers
  C_HashTable<handle, C_TrickPlay> m_cTrickPlay;
  C_HashTable<handle, C_TsStreamer> m_cStreamers;
};


// Declaration and implementation of C_v4lInputModule
DECLARE_MODULE(v4l, Input, "v4l", const C_String&);


#else
#error "Multiple inclusions of v4linput.h"
#endif

