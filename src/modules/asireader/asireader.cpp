/*******************************************************************************
* asireader.cpp: ASI reader
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id: asireader.cpp,v 1.2 2003/10/27 10:58:10 sam Exp $
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This reader enables VLS to read from an ASI board.
* It reads from the device and listens for events.
* It should be used the driver included in Linear Systems Ltd.'s DVB Master
* Linux Software Development Kit
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <asi.h>

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "asireader.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_AsiMpegReaderModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_asireader(handle hLog)
{
  return new C_AsiMpegReaderModule(hLog);
}
#endif


/*******************************************************************************
* C_AsiMpegReader
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_AsiMpegReader::C_AsiMpegReader(C_Module* pModule,
                                   C_Broadcast* pBroadcast) :
                        C_MpegReader(pModule, pBroadcast)
{
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_AsiMpegReader::Init()
{
    m_hFd = open("/dev/asirx0", O_RDONLY | O_LARGEFILE, 0); //XXX
    if ( m_hFd < 0 )
    {
        throw E_Exception(GEN_ERR, "Unable to open device file");
    }
    m_uiBufSize = 188;//XXX
    return;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AsiMpegReader::Close()
{
    close(m_hFd);
    return;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_AsiMpegReader::Read(byte* pBuff, s64 iSize)
{
    struct pollfd pfd;

    pfd.fd = m_hFd;
    pfd.events = POLLIN | POLLPRI;

    if (poll (&pfd, 1, 1000) < 0)
    {
        Log(m_hLog, LOG_ERROR, "Error polling device file");
        return MPEG_STREAMERROR;
    }

    if (pfd.revents & POLLIN)
    {
        return LocalRead(pBuff, 188);
    }
    else
    {
        return LocalGetEvent();
    }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AsiMpegReader::ResetEndOfStream()
{
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_AsiMpegReader::Seek(s64 iOffset, s64 bStartPos)
{
    return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_AsiMpegReader::Size()
{
    return 0;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_AsiMpegReader::GetPos()
{
    return 0;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
s64 C_AsiMpegReader::LocalRead(byte* pBuff, s64 iSize)
{
    int iRc = read ( m_hFd, pBuff, 188 );

    if ( iRc < 0 )
    {
        Log(m_hLog, LOG_ERROR, "Error reading device file");
        return MPEG_STREAMERROR;
    }
    return iRc;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_AsiMpegReader::LocalGetEvent(void)
{
    int val;

    if (ioctl (m_hFd, ASI_IOC_RXGETEVENTS, &val) < 0)
    {
        Log(m_hLog, LOG_ERROR, "Unable to get receiver event flag");
        return MPEG_STREAMERROR;
    }

    if (val & ASI_EVENT_RX_BUFFER)
            Log(m_hLog, LOG_WARN, "Buffer queue overrun detected");
    if (val & ASI_EVENT_RX_FIFO)
            Log(m_hLog, LOG_WARN, "FIFO overrun detected");
    if (val & ASI_EVENT_RX_CARRIER)
            Log(m_hLog, LOG_NOTE, "Carrier status change detected");
    if (val & ASI_EVENT_RX_LOS)
            Log(m_hLog, LOG_WARN, "Loss of packet synchro detected");
    if (val & ASI_EVENT_RX_AOS)
            Log(m_hLog, LOG_NOTE, "Acquisition of packet synchro");
    if (val & ASI_EVENT_RX_DATA)
            Log(m_hLog, LOG_NOTE, "Receive data status change detected");
    return 0;
}

