/*******************************************************************************
* asieader.h: ASI reader
*-------------------------------------------------------------------------------
* (c) 2003 Anevia
* $Id: asireader.h,v 1.1 2003/08/31 11:46:17 nitrox Exp $
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*******************************************************************************/


#ifndef _ASI_READER_H_
#define _ASI_READER_H_


//------------------------------------------------------------------------------
// C_MpegAsiReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_AsiMpegReader : public C_MpegReader 
{
public:
  C_AsiMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual int GetFrame(byte ** pBuff ,int iSize) { return 0;};
  virtual int GetAudioFD() { return 0;};
  
  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual s64 Size();
  virtual s64 GetPos();
  virtual void ResetEndOfStream();

protected:
    int LocalGetEvent(void);
    s64 LocalRead(byte* pBuff, s64 iSize);
  
    int m_hFd;
    unsigned int m_uiBufSize;
    bool m_bEnd;
};


// Declaration and implementation of C_TcpMpegReaderModule
DECLARE_MODULE(Asi, MpegReader, "asi", C_Broadcast*);


#else
#error "Multiple inclusions of asireader.h"
#endif

