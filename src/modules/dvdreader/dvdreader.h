/*******************************************************************************
* dvdreader.h: DVD reader
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: dvdreader.h,v 1.6 2003/06/02 19:50:35 jpsaman Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _DVD_READER_H_
#define _DVD_READER_H_


//------------------------------------------------------------------------------
// Dvd Exception
//------------------------------------------------------------------------------
class E_Dvd : public E_Exception
{
public:
  E_Dvd(const C_String& strMsg);
};


//------------------------------------------------------------------------------
// C_DvdReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_DvdMpegReader : public C_MpegReader
{
protected:
  C_ProgramDescriptor* m_pPgrmDescriptor;

public:
  C_DvdMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual int GetFrame(byte ** pBuff ,int iSize) { return 0;};
  virtual int GetAudioFD() { return 0;};

  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual s64 Size();
  virtual s64 GetPos();

  void SetPgrmDescriptor(C_ProgramDescriptor* pPgrmDescriptor)
  {
    m_pPgrmDescriptor = pPgrmDescriptor;
  };

protected:
  s64 ReadBlock(bool bFill = true);

  C_String m_strDevice;
  int m_iTitleId;
  int m_iChapId;
  int m_iAngle;

  dvd_reader_t*         m_pDvd;
  dvd_file_t*           m_pTitle;
  ifo_handle_t*         m_pVmgFile;
  ifo_handle_t*         m_pVtsFile;

  pgc_t* m_pCurPgc;

  bool m_bJumpCell;
  bool m_bReadNav;

  int m_iFirstCell;
  int m_iStartCell;
  int m_iCurCell;
  int m_iNextCell;

  u64 m_uiCurBlock;
  u64 m_uiBlockLeft;

  unsigned int m_uiNextVobu;

  byte m_bBlock[DVD_VIDEO_LB_LEN];
  u64 m_uiByteLeft;

  bool m_bLoop;
};


// Declaration and implementation of C_DvdMpegReaderModule
DECLARE_MODULE(Dvd, MpegReader, "dvd", C_Broadcast*);


#else
#error "Multiple inclusions of dvdreader.h"
#endif

