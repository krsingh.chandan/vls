/*******************************************************************************
* TCPreader.h: TCP reader
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id$
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* reads a stream from the network via a tcpsocket
*******************************************************************************/


#ifndef _TCP_READER_H_
#define _TCP_READER_H_


//------------------------------------------------------------------------------
// C_MpegTcpReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_TcpMpegReader : public C_MpegReader 
{
public:
  C_TcpMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual int GetFrame(byte **,int) { return 0;};
  virtual int GetAudioFD() { return 0;};
  
  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual s64 Size();
  virtual s64 GetPos();
  virtual void ResetEndOfStream();

  byte CS_XOR_Get() {return m_bXOR;};


protected:
  C_Socket m_cSocket;
  bool m_bEnd;

private:
  void CS_XOR(byte*, s64);
  byte m_bXOR;
};



// Declaration and implementation of C_TcpMpegReaderModule
DECLARE_MODULE(Tcp, MpegReader, "tcp", C_Broadcast*);


#else
#error "Multiple inclusions of tcpreader.h"
#endif

