/*******************************************************************************
* tcpinput.cpp: Input for spawning a tcp reader
*-------------------------------------------------------------------------------
* (c)2003 Anevia
* $Id$
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*----------------------------------------------------------------------------
*
*******************************************************************************/
//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------

#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"
#include "../../server/program.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"
#include "../../server/tsstreamer.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/trickplay.h"
#include "../../mpeg/converter.h"

#include "tcpinput.h"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_TcpInputModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_tcpinput(handle hLog)
{
      return new C_TcpInputModule(hLog);
}
#endif


/*******************************************************************************
* C_TcpInput class
********************************************************************************
*
*******************************************************************************/
//------------------------------------------------------------------------------
// Constructor

//------------------------------------------------------------------------------
C_TcpInput::C_TcpInput(C_Module* pModule, const C_String& strName)
                                : C_Input(pModule, strName)
{
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_TcpInput::~C_TcpInput()
{
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_TcpInput::OnInit()
{
    C_String* pStr = new C_String("tcp1");
    m_vProgramNames.Add(pStr);
    Log(m_hLog, LOG_NOTE, "Added program '" + *pStr+"'");
    return;
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_TcpInput::OnDestroy()
{
    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_List<C_Program> C_TcpInput::OnGetAvailablePgrms()
{
    C_List<C_Program> cPgrmList;

    for(unsigned int ui = 0; ui < m_vProgramNames.Size(); ui++)
    {
        C_Program* pProgram = new C_Program(m_vProgramNames[ui]);
        ASSERT(pProgram);
        cPgrmList.PushEnd(pProgram);
    }
    return cPgrmList;
}

//------------------------------------------------------------------------------
// Start the reception of the given program
//------------------------------------------------------------------------------
void C_TcpInput::OnStartStreaming(C_Broadcast* pBroadcast)
{
    C_String strReaderType = "tcp";
    C_String strConverterType;
    C_String strTrickPlayType = "normal";

    // Get the converter required, configure the broadcast
    C_Application* pApp = C_Application::GetApp();
    C_String strType = pApp->GetSetting(GetName() + ".type", "mpeg2-ts");
    if ( strType == "mpeg2-ts" )
    {
        strConverterType = "ts2ts";
        pBroadcast->SetOption("mpegversion", "2");
        pBroadcast->SetOption("preparse", "0");
    }
    else if ( strType == "mpeg2-ps" )
    {
        strConverterType = "ps2ts";
        pBroadcast->SetOption("mpegversion", "2");
        pBroadcast->SetOption("preparse", "0");
    }
    else if ( strType == "mpeg1-ps" )
    {
        strConverterType = "ps2ts";
        pBroadcast->SetOption("mpegversion", "2");
        pBroadcast->SetOption("preparse", "0");
    }

    // Create the netlist and the fifo
    const C_Channel* pChannel = pBroadcast->GetChannel();
    C_NetList* pTsProvider = new C_NetList(3*3*797);
    uint16_t uiSize =  pTsProvider->Capacity() -pChannel->GetBuffCapacity()-2;
    C_SyncFifo* pBuffer =  new C_SyncFifo(uiSize);

    // Create the reader
    C_MpegReader* pReader;
    C_MpegReaderModule* pReaderModule = (C_MpegReaderModule*)
                                        C_Application::GetModuleManager()
                                            ->GetModule("mpegreader",
                                                           strReaderType);
    if(pReaderModule)
    {
        pReader = pReaderModule->NewMpegReader(pBroadcast);
    }
    else
    {
        throw E_Exception(GEN_ERR, "Module mpegreader:" + strConverterType +
                                                               " not present");
    }

    // Create the converter
    C_MpegConverter* pConverter;
    C_MpegConverterModule* pConverterModule = (C_MpegConverterModule*)
                            C_Application::GetModuleManager()
                                ->GetModule("mpegconverter",
                                                    strConverterType);
    if(pConverterModule)
    {
        C_MpegConverterConfig cConfig;
        cConfig.m_hLog = m_hLog;
        cConfig.m_pBroadcast = pBroadcast;
        cConfig.m_pReader = pReader;
        cConfig.m_pTsProvider = pTsProvider;
        cConfig.m_pEventHandler = m_pEventHandler;
        pConverter = pConverterModule->NewMpegConverter(cConfig);
        ASSERT(pConverter);
    }
    else
    {
        throw E_Exception(GEN_ERR, "Module mpegconverter:" +
                                           strConverterType + " not present");
    }

    pReader->SetConverter(pConverter);

    // Create the trickplay
    C_TrickPlay* pTrickPlay;
    C_TrickPlayModule* pTrickPlayModule = (C_TrickPlayModule*)
                                            C_Application::GetModuleManager()
                                            ->GetModule("trickplay",
                                                        strTrickPlayType);
    if (pTrickPlayModule)
    {
        C_TrickPlayConfig cTrickPlayConfig;
        cTrickPlayConfig.m_hLog = m_hLog;
        cTrickPlayConfig.m_pBroadcast = pBroadcast;
        cTrickPlayConfig.m_pReader = pReader;
        cTrickPlayConfig.m_pHandler = pBuffer;
        cTrickPlayConfig.m_iInitFill = pBuffer->Capacity();
        cTrickPlayConfig.m_pEventHandler = m_pEventHandler;
        cTrickPlayConfig.m_pTsProvider = pTsProvider;
        cTrickPlayConfig.m_pConverter = pConverter;

        pTrickPlay = pTrickPlayModule->NewTrickPlay(cTrickPlayConfig);
        ASSERT(pTrickPlay);
    }
    else
    {
        throw E_Exception(GEN_ERR,
                          "Module TrickPlay:" + strTrickPlayType +
                          " not present");
    }

    // Create the streamer
    C_TsStreamer* pStreamer = new C_TsStreamer(m_hLog, pBroadcast,
                                               pTsProvider, pBuffer,
                                               m_pEventHandler, true, false);
    ASSERT(pStreamer);
    m_cTrickPlay.Add(pBroadcast, pTrickPlay);
    m_cStreamers.Add(pBroadcast, pStreamer);
    m_cConverters.Add(pBroadcast, pConverter);

    try
    {
        pTrickPlay->Create();
        pStreamer->Create();
        fprintf(stderr, "nitrox -- Right\n");
    }
    catch(E_Exception e)
    {
        pStreamer->Stop();
        pTrickPlay->Stop();

        //Unregister the 2 thread and delete them
        m_cTrickPlay.Delete(pBroadcast);
        m_cStreamers.Delete(pBroadcast);

        throw E_Exception(GEN_ERR, "unable to start streaming of program "+
                          pBroadcast->GetProgram()->GetName(), e);
    }
    return;
}


//------------------------------------------------------------------------------
// Resume the reception of the given program
//------------------------------------------------------------------------------
void C_TcpInput::OnResumeStreaming(C_Broadcast* pBroadcast)
{
    ASSERT(pBroadcast);

    // Find the converter that receive the pgrm
    C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
    ASSERT(pTrickPlay);

    // Resume the thread
    try
    {
        pTrickPlay->Resume();
    }
    catch(E_Exception e)
    {
        throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                          pBroadcast->GetProgram()->GetName(), e);
    }
    return;
}

//------------------------------------------------------------------------------
// Suspend the reception of the given program
//------------------------------------------------------------------------------
void C_TcpInput::OnSuspendStreaming(C_Broadcast* pBroadcast)
{
    ASSERT(pBroadcast);

    // Find the converter that receive the pgrm
    C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
    ASSERT(pTrickPlay);

    C_String strPgrmName = pBroadcast->GetProgram()->GetName();

    // Suspend the thread
    try
    {
        pTrickPlay->Suspend();
    }
    catch (E_Exception e)
    {
        throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                          pBroadcast->GetProgram()->GetName(), e);
    }
    return;
}

//------------------------------------------------------------------------------
// Forward the reception of the given program with specified speed
//------------------------------------------------------------------------------
void C_TcpInput::OnForwardStreaming(C_Broadcast* pBroadcast, int speed)
{
    ASSERT(pBroadcast);

    // Find the reader and the streamer that receive the pgrm
    C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
    ASSERT(pTrickPlay);

    // Call forward to the threads
    try
    {
        pTrickPlay->Forward(speed);
    }
    catch(E_Exception e)
    {
        throw E_Exception(GEN_ERR,
                          "Unable to forward into the streaming of program "+
                          pBroadcast->GetProgram()->GetName(), e);
    }
    return;
}

//------------------------------------------------------------------------------
// Rewind the reception of the given program with specified speed
//------------------------------------------------------------------------------
void C_TcpInput::OnRewindStreaming(C_Broadcast* pBroadcast, int speed)
{
    ASSERT(pBroadcast);

    // Find the reader and the streamer that receive the pgrm
    C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
    ASSERT(pTrickPlay);

    // Call rewind to the threads
    try
    {
        pTrickPlay->Rewind(speed);
    }
    catch(E_Exception e)
    {
        throw E_Exception(GEN_ERR,
                          "Unable to rewind into the streaming of program "+
                          pBroadcast->GetProgram()->GetName(), e);
    }
    return;
}


//------------------------------------------------------------------------------
// Stop the reception of the given program
//------------------------------------------------------------------------------
void C_TcpInput::OnStopStreaming(C_Broadcast* pBroadcast)
{
    ASSERT(pBroadcast);

    // Find the reader and the streamer that receive the pgrm
    C_TrickPlay* pTrickPlay = m_cTrickPlay.Remove(pBroadcast);
    ASSERT(pTrickPlay);
    C_TsStreamer* pStreamer = m_cStreamers.Remove(pBroadcast);
    ASSERT(pStreamer);
    C_MpegConverter* pConverter = m_cConverters.Remove(pBroadcast);
    ASSERT(pConverter);

    // Stop the threads
    try
    {
        pStreamer->Stop();
        pTrickPlay->Stop();
        delete pTrickPlay;
        delete pConverter;
        delete pStreamer; // streamer MUST be deleted last
    }
    catch(E_Exception e)
    {
        throw E_Exception(GEN_ERR, "Unable to stop streaming of program "+
                                       pBroadcast->GetProgram()->GetName(), e);
    }

    return;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TcpInput::OnUpdateProgram(C_String, C_String, C_String)
{
    throw E_Exception(GEN_ERR, "Updating program not implemented");
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TcpInput::OnDeleteProgram(C_String)
{
    throw E_Exception(GEN_ERR, "Deleting program not implemented");
}
