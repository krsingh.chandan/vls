/*******************************************************************************
* fileoutput.h: file output
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: fileoutput.h,v 1.2 2002/08/09 13:42:32 tooney Exp $
*
* Authors: James Courtier-Dutton <James@superbug.demon.co.uk>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _FILEOUTPUT_H_
#define _FILEOUTPUT_H_


//------------------------------------------------------------------------------
// C_FileOutput class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_FileOutput : public C_Output
{
 public:
  C_FileOutput(const C_String& strFileName,
               bool bAppend);
  virtual ~C_FileOutput();

  virtual void OnInit();
  virtual void OnClose();

 protected:
  virtual void WriteToPort(bool RtpEncapsulation, u32 RtpSendTime);

  C_File m_cFile;

  // Output parameters
  C_String m_strFileName;
  bool m_bAppend;
};


#else
#error "Multiple inclusions of fileoutput.h"
#endif

