/*******************************************************************************
* fileoutput.cpp: file output
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: fileoutput.cpp,v 1.5 2003/10/27 10:58:10 sam Exp $
*
* Authors: James Courtier-Dutton <James@superbug.demon.co.uk>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"
#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"

#include "../../server/buffer.h"
#include "../../server/output.h"

#include "fileoutput.h"


//******************************************************************************
// C_FileOutput class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_FileOutput::C_FileOutput(const C_String& strFileName, bool bAppend)
                : C_Output(7),
                  m_cFile(strFileName),
                  m_strFileName(strFileName)
{
  m_bAppend = bAppend;
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_FileOutput::~C_FileOutput()
{
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_FileOutput::OnInit()
{
    // Open the file
    if(m_bAppend)
#ifdef WIN32
  	m_cFile.Open("ab+");
#else
  	m_cFile.Open("a+");
#endif
    	else
#ifdef WIN32
      m_cFile.Open("wb+");
#else
      m_cFile.Open("w+");
#endif
  
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_FileOutput::OnClose()
{
  try
  {
    m_cFile.Close();
  }
  catch(E_Exception e)
  {
    throw E_Output("Output termination failed", e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_FileOutput::WriteToPort(bool RtpEncapsulation, u32 RtpSendTime)
{
  ASSERT(m_pTsProvider);

  unsigned int iPacketNumber = m_cTsBuff.Size();

  int HeaderOffset;
  bool HasDiscontinuity = 0;

  if(iPacketNumber > 0)
  {
    try
    {
      if (RtpEncapsulation)
      {
        HeaderOffset = RTP_HEADER_LEN;
        m_pRtpHeader->BuildHeader(m_iRtpCounter++);

        // Convert to the 90kHzClock : GetDate()*90 000/1 000 000
        // Doing a /10 *9 /10 fo conversion / integer issues.
        m_pRtpHeader->SetRtpTimeStamp( ((RtpSendTime/10)*9)/10 );

        // Check for discontinuity in one of the TS packets
        for(unsigned int iIndex = 0; iIndex < iPacketNumber; iIndex++)
         HasDiscontinuity |= m_cTsBuff[iIndex].IsDiscontinuity();
        m_pRtpHeader->SetRtpDiscontinuity(HasDiscontinuity);

        m_cFile.Write(*m_pRtpHeader, RTP_HEADER_LEN);
      }

      for(unsigned int i = 0; i < iPacketNumber; i++)
      {
        m_cFile.Write(m_cTsBuff[i], TS_PACKET_LEN);
      }
    }
    catch(E_File e)
    {
      throw E_Output("File output error", e);
    }

    // Free the now unused packets
    C_TsPacket* pPacket;
    for(unsigned int i = 0; i < iPacketNumber; i++)
    {
      // Pop the packet from the buffer
      pPacket = m_cTsBuff.Pop();
      ASSERT(pPacket);
      // And release it
      m_pTsProvider->ReleasePacket(pPacket);
    }
  }
}


