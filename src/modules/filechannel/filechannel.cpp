/*******************************************************************************
* filechannel.cpp: file channel
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: filechannel.cpp,v 1.6 2003/10/27 10:58:10 sam Exp $
*
* Authors: James Courtier-Dutton <James@superbug.demon.co.uk>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"
#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"

#include "fileoutput.h"
#include "filechannel.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_FileChannelModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_filechannel(handle hLog)
{
  return new C_FileChannelModule(hLog);
}
#endif


//******************************************************************************
// class C_FileChannel
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_FileChannel::C_FileChannel(C_Module* pModule,
                             const C_ChannelConfig& cConfig) :
                               C_Channel(pModule, cConfig)
{
  m_pOutput = new C_FileOutput(cConfig.m_strFilename, cConfig.m_bAppend);
}


