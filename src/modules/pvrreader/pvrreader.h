/*******************************************************************************
* pvrreader.cpp: Reading from an Hauppauge PVR board
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: pvrreader.h,v 1.3 2003/09/26 12:08:21 alexis Exp $
*
* Authors: Tristan Leteutre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/


#ifndef _PVR_READER_H_
#define _PVR_READER_H_

#include <linux/videodev.h>

class E_Pvr : public E_Exception
{
public:
  E_Pvr(const C_String& strMsg);
};

//------------------------------------------------------------------------------
// C_PvrMpegReader class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_PvrMpegReader : public C_MpegReader
{
public:
  C_PvrMpegReader(C_Module* pModule, C_Broadcast* pBroadcast);

  virtual void Init();
  virtual void Close();

  virtual s64 Read(byte* pBuff, s64 iSize);
  virtual s64 Seek(s64 iOffset, s64 bStartPos);
  virtual s64 Size();
  virtual s64 GetPos();

  virtual int GetFrame(byte ** pBuff ,int iSize);
  virtual int GetAudioFD() { return 0;};
  
protected:
  C_Broadcast* m_pBroadcast;
  C_String m_strDeviceName;
  int m_hFd;

  bool m_bLoop;
  bool m_bEnd;

  // Pvr parameters
 
  
   // parameters of the image
  int m_iNorm;
  int m_iFrequency;
  int m_iFrameRate;
  C_String m_strSize;
  int m_iBitRate;
  int m_iMaxBitRate;
  int m_iBitRateMode;
  int m_iInputNumber;
  int m_iHeight, m_iWidth;
};


// Declaration and implementation of C_PvrMpegReaderModule
DECLARE_MODULE(Pvr, MpegReader, "pvr", C_Broadcast*);


#else
#error "Multiple inclusions of pvrreader.h"
#endif

