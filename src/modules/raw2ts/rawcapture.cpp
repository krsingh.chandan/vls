/*******************************************************************************
* v4lraw.cpp: Capture thread from a v4l device (WebCam, tv tuner)
*-------------------------------------------------------------------------------
* (c) 2003 VideoLAN
* $Id: rawcapture.cpp,v 1.8 2003/11/05 17:03:04 tooney Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.n>
*          Tristan Leteutre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Implementation of a raw capture thread
*******************************************************************************/
//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------

#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"

#include "../../server/buffer.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>

#include "rawcapture.h"

#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/mman.h>
#include <unistd.h>

/********************************************************************************
* Useful helper functions
********************************************************************************/
u64 GetTime(void)
{
  struct timeval tmp;
  gettimeofday(&tmp, NULL);
  return( (u64)tmp.tv_sec * 1000000 + (u64)tmp.tv_usec );
}


/*******************************************************************************
* C_VideoCaptureThread
*******************************************************************************/

C_VideoCaptureThread::C_VideoCaptureThread( handle hLog,
                                            C_MpegReader * pReader,
                                            C_DatedFifo *pFifo,
                                            int iPictureBufSize,
                                            C_Condition * pCondition,
                                            float quality )
{
  m_pReader = pReader;
  m_pFifo = pFifo;
  m_iPictureBufSize = iPictureBufSize;
  m_pCondition = pCondition;
  m_quality = quality ;
  m_hLog = hLog;
}

void C_VideoCaptureThread::InitWork()
{
  m_bStop = false;
}

void C_VideoCaptureThread::DoWork()
{
  C_DatedBuffer * pDatedBuffer;

  while( !m_bStop )
  {
    pDatedBuffer = new C_DatedBuffer();
    if( !pDatedBuffer->Alloc( m_iPictureBufSize ) )
    {
      fprintf( stderr, "Error : cannot Alloc() (%d bytes)\n",
                       m_iPictureBufSize );
      return;
    }

    // read data from the device; this is the blocking stage.
    byte * tmp = NULL;
    pDatedBuffer->m_iSize = m_pReader->GetFrame( &tmp, m_iPictureBufSize );

    // date it
    pDatedBuffer->m_TimeStamp = ( GetTime() * 9 / 100 ) &  0x0effffff;
    
    // the memory returned by the reader does not belong to us, so we
    // have to memcpy it before pushing this into the fifo.
    // TODO: find a way to avoid memcpy-ing
    memcpy( pDatedBuffer->m_pData, tmp, m_iPictureBufSize );

    // add it to fifo
    int iRc = m_pFifo->Push( pDatedBuffer );
    if (iRc)
      fprintf( stderr, "Warning : trashing a video frame (%lld)... "
                       "your computer is too slow !\n",
                        pDatedBuffer->m_TimeStamp );

    // warn the encoder thread we have something
    m_pCondition->Protect();
    m_pCondition->Signal();
  }
}

void C_VideoCaptureThread::StopWork()
{
  m_bStop = true;
}
void C_VideoCaptureThread::CleanWork()
{
}


/*******************************************************************************
* C_AudioCaptureThread
*******************************************************************************/

C_AudioCaptureThread::C_AudioCaptureThread( handle hLog,
                                            C_MpegReader * pReader,
                                            C_DatedFifo *pFifo,
                                            int iAudioSampleBufSize,
                                            C_Condition * pCondition,
                                            int channel,
                                            int audiofreq )
{
  m_pReader = pReader;
  m_pFifo = pFifo;
  m_iAudioSampleBufSize = iAudioSampleBufSize;
  m_pCondition = pCondition;
  m_channel = channel;
  m_audiofreq = audiofreq;
  m_hLog = hLog;
}

void C_AudioCaptureThread::InitWork()
{
  m_bStop = false;
  m_iAudioFD = m_pReader->GetAudioFD();
}

void C_AudioCaptureThread::DoWork()
{
  C_DatedBuffer * pDatedBuffer;
  u64 currentDate;
  struct audio_buf_info buf_info;
  int delay;

  while( !m_bStop )
  {
    pDatedBuffer = new C_DatedBuffer();
    if( !pDatedBuffer->Alloc( m_iAudioSampleBufSize ) )
    {
      fprintf( stderr, "Error : cannot Alloc() (%d bytes)\n",
                       m_iAudioSampleBufSize );
      return;
    }


    // read data from the device; this is the blocking stage
    // do a safe read, to be sure to get  m_iAudioSampleBufSize bytes
    int iRet = 1;
    int iCount = 0;

    while ( (iCount < m_iAudioSampleBufSize))
    {
      if ((iRet = read( m_iAudioFD,
                        pDatedBuffer->m_pData + iCount,
                        m_iAudioSampleBufSize - iCount)) < 0 )
        break;
     
      iCount += iRet;
    }

    if (iRet < 0)
    {
      perror("Error reading audio");
      return;
    }

    ASSERT(iCount == m_iAudioSampleBufSize);

    delay = iCount;
    
    // date it
    currentDate = GetTime();

    // correct the date because of kernel buffering
    if( ioctl( m_iAudioFD, SNDCTL_DSP_GETISPACE, &buf_info ) == 0 )
      delay += buf_info.bytes;

    currentDate -= (u64)1000000 * (u64)delay
                   / 2            /* 16 bit samples */
                   / m_channel    /* channel */
                   / m_audiofreq; /* sample rate */

    pDatedBuffer->m_TimeStamp = ( currentDate * 9 / 100 ) &  0x0effffff;
    pDatedBuffer->m_iSize = iCount;

    // add it to fifo
    int iRc = m_pFifo->Push( pDatedBuffer );
    if (iRc) {
      fprintf( stderr, "Warning : trashing an audio frame (%lld)... "
                       "your computer is too slow !\n",
                        pDatedBuffer->m_TimeStamp );
      delete pDatedBuffer;
    }

    // warn the encoder thread we have something
    m_pCondition->Protect();
    m_pCondition->Signal();
  }
}

void C_AudioCaptureThread::StopWork()
{
  m_bStop = true;
}

void C_AudioCaptureThread::CleanWork()
{
}
