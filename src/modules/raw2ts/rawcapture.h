/*******************************************************************************
* v4lraw.h: Capture thread from a v4l device (WebCam, tv tuner)
*-------------------------------------------------------------------------------
* (c) 2003 VideoLAN
* $Id: rawcapture.h,v 1.1 2003/08/06 08:18:52 tooney Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.n>
*          Tristan Leteutre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Implementation of a raw capture thread
*******************************************************************************/

#ifndef _V4LRAW_CAPTURE_H_
#define _V4LRAW_CAPTURE_H_

//------------------------------------------------------------------------------
// C_AudioCaptureThread class
//------------------------------------------------------------------------------
class C_AudioCaptureThread : public C_Thread
{
public:
  C_AudioCaptureThread( handle hLog, C_MpegReader * pReader, C_DatedFifo * pFifo,
                       int iAudioSampleBufSize, 
                       C_Condition * pCondition, int channel, int audiofreq );

protected:
  virtual void InitWork();
  virtual void DoWork();
  virtual void StopWork();
  virtual void CleanWork();

private:
  C_MpegReader * m_pReader;
  C_DatedFifo * m_pFifo;
  C_Condition * m_pCondition;

  int m_iAudioFD;
  int m_iAudioSampleBufSize;

  bool m_bStop;
  int m_channel;
  int m_audiofreq;

  handle m_hLog;
};


//------------------------------------------------------------------------------
// C_VideoCaptureThread class
//------------------------------------------------------------------------------
class C_VideoCaptureThread : public C_Thread
{
public:
  C_VideoCaptureThread( handle hLog, C_MpegReader * pReader, C_DatedFifo *pFifo,
                       int iPictureBufSize, 
                       C_Condition * pCondition, float quality );

protected:
  virtual void InitWork();
  virtual void DoWork();
  virtual void StopWork();
  virtual void CleanWork();

private:
  C_MpegReader * m_pReader;
  C_DatedFifo * m_pFifo;
  C_Condition * m_pCondition;

  int m_iPictureBufSize;

  bool m_bStop;
  float m_quality;

  handle m_hLog;
};

#else
#error "Multiple inclusions of v4lraw.h"
#endif
