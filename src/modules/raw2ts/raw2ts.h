/*******************************************************************************
* raw2ts.h: raw -> ES ffmpeg encoder -> TS
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: raw2ts.h,v 1.24 2003/10/28 23:48:45 tooney Exp $
*
* Authors: Tristan Leteurtre <tooney@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _RAW2TS_CONVERTER_H_
#define _RAW2TS_CONVERTER_H_

#ifdef __cplusplus
extern "C" {
#include <avcodec.h> //for palette
}
#endif

//------------------------------------------------------------------------------
// C_Raw2TsMpegConverter class
//------------------------------------------------------------------------------
// Mpeg 2 TS -> Mpeg 2 TS (no conversion)
//------------------------------------------------------------------------------
class C_Raw2TsMpegConverter : public C_MpegConverter
{
public:
  C_Raw2TsMpegConverter(C_Module* pModule, C_MpegConverterConfig& cConfig);
  int EStoTS( C_Fifo<C_TsPacket> * pPackets,  byte * EsData,
                   int size, int TimeStamp,
              u8 iPayloadType, int * iCC);

  virtual s64 GetNextTsPackets(C_Fifo<C_TsPacket> * pPackets, int skip = 0);
  virtual s64 GetPrevTsPackets(C_Fifo<C_TsPacket> * pPackets, int skip = 0);

protected:
  virtual void InitWork();
  virtual void StopWork();

  void DeInterlace(AVCodecContext *dec, AVPicture *picture, byte **);

private:
  C_DvbPsiPat *m_cPat;
  C_DvbPsiPmt *m_cPmt;
  C_DatedFifo *m_pVideoFifo;
  C_DatedFifo *m_pAudioFifo;
  C_VideoCaptureThread * m_pVideoThread;
  C_AudioCaptureThread * m_pAudioThread;
  C_Condition * m_pHasDataCondition;

  C_DatedBuffer * m_pDatedBuffer; // Keep state accros multiple function calls

  int m_iPos;
  // ffmpeg variables
  AVCodec * m_VideoCodec;
  AVCodecContext *m_VideoCodecContext;
  AVCodec * m_AudioCodec;
  AVCodecContext *m_AudioCodecContext;

  // mute audio ?
  bool m_bMute;
  
  // compression : mpeg1 or mpeg4
  C_String m_strCompression;
  int m_type ;
  C_String m_audioCompression;

  // buffers
  AVPicture * m_Picture;
  byte *m_VideoOutBuf, *m_PictureBuf;
  int m_iVideoOutBufSize, m_iVideoOutSize, m_iPictureBufSize;

  byte *m_AudioSampleBuf, *m_AudioOutBuf;
  int m_iAudioSampleBufSize, m_iAudioOutBufSize;

  // C_PesPacket member to avoid malloc/free
  C_PesPacket * m_pPesPacket;

  // parameters of the image
  int m_iHeight, m_iWidth, m_iFrameRate, m_iPalette;
  bool m_bDeInterlace;

  // encapsulation variables
  int iVideoContinuityCounter;
  int iAudioContinuityCounter;

  int m_channel;
  int m_audiofreq;
  float m_quality;
  int m_atype;
  int m_iFrameSize;  
};


// Declaration and implementation of C_Raw2TsMpegConverterModule
DECLARE_MODULE(Raw2Ts, MpegConverter, "raw2ts", C_MpegConverterConfig&);

#else
#error "Multiple inclusions of raw2ts.h"
#endif

