/*******************************************************************************
* ts2ts.cpp: TS to TS converter
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: ts2ts.cpp,v 1.10 2003/10/27 10:58:11 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../server/buffer.h"
#include "../../server/program.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "ts2ts.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_Ts2TsMpegConverterModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_ts2ts(handle hLog)
{
  return new C_Ts2TsMpegConverterModule(hLog);
}
#endif


/*******************************************************************************
* C_Ts2TsConverter
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Ts2TsMpegConverter::C_Ts2TsMpegConverter(C_Module* pModule,
                                           C_MpegConverterConfig& cConfig) :
                            C_MpegConverter(pModule, cConfig),
                            m_iCorrectPercentage(10)
{
  m_iLoop = cConfig.m_pBroadcast->GetOption("loop").ToInt();
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_Ts2TsMpegConverter::InitWork()
{
  // Inherited method
  C_MpegConverter::InitWork();
}

//------------------------------------------------------------------------------
// Functions to correct an estimated seek for the n-th Packet
//------------------------------------------------------------------------------
s64 inline C_Ts2TsMpegConverter::CorrectForward(s64 size)
{
  s64 iOffset;
  s64 iMissAligned;

  iOffset = ((100 - m_iCorrectPercentage) * size) / 100;
  iMissAligned = iOffset % TS_PACKET_LEN;

  return (iOffset - iMissAligned);
}

s64 inline C_Ts2TsMpegConverter::CorrectRewind(s64 size)
{
  s64 iOffset;
  s64 iMissAligned;

  iOffset = ((100 + m_iCorrectPercentage) * size) / 100;
  iMissAligned = iOffset % TS_PACKET_LEN;

  return (iOffset - iMissAligned);
}

//------------------------------------------------------------------------------
// Get Next Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_Ts2TsMpegConverter::GetNextTsPackets(C_Fifo<C_TsPacket>* pPackets
                                                                    , int skip)
{
  s64 iRc = 0;
  bool bDiscontinuity = false;

  // do any skipping required
  if (skip>0)
  {
    iRc = m_pReader->Seek(CorrectForward(skip*TS_PACKET_LEN), FILE_SEEK_CURRENT);
    if(iRc == MPEG_ENDOFSTREAM)
    {
      C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
      LogDbg(m_hLog, "End of program \"" + strPgrmName + "\" reached");
      return iRc;
    }
    else if(iRc != NO_ERR)
    {
      C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
      Log(m_hLog, LOG_ERROR, "Read error for program \"" + strPgrmName + "\"");
      return iRc;
    }
  }

  // grab a new packet
  C_TsPacket * pPacket = m_pTsProvider->GetPacket();

  // packet fill loop.. keeps going until a valid packet is found
  while(true) {
    // get data for it
    iRc = m_pReader->Read((byte*) (*pPacket), TS_PACKET_LEN);

    // first, check that the read went OK
    if (iRc == TS_PACKET_LEN) {
      // read was fine
      iRc = NO_ERR;
    } else if ((iRc == FILE_EOF) || (m_pReader->EndOfStream())) {
      // we hit the EOF
      if (m_iLoop) {
        // try and reset the stream
        m_pReader->ResetEndOfStream();

        // if that was successful, do loop setup stuff
        if (!m_pReader->EndOfStream()) {
          if ((iRc = Synch()) != 0) {
            return iRc;
          }
          m_iLoop--;
        }
      } else {
        // not looping.
        iRc = MPEG_ENDOFSTREAM;
      }

      // we always add an error packet at the end. We
      // don't analyse the data in the stream to know for sure
      // it completed any ES packets etc, so for the benefit of
      // the doubt, we add one in.
      ZeroPacket(pPacket);
      pPacket->SetErrorFlag();
    } else if (iRc >= 0) {
      // truncated read
      ZeroPacket(pPacket);
      pPacket->SetErrorFlag();
      iRc = NO_ERR;
    } else {
      // we had some other error.
      m_pTsProvider->ReleasePacket(pPacket);
      return MPEG_STREAMERROR;
    }

    // Now, was it a valid packet?
    if ((*pPacket)[0] == 0x47) {
      // if the discontinuity flag is set, the packet
      // HAS to contain the PCR
      if (bDiscontinuity) {
        if (pPacket->HasPCR()) {
          break;
        }
      } else {
        // normal mode => any packet will do
        break;
      }
    } else {
      // invalid packet => attempt to resynchronise with the stream,
      // and set discontinuity flag so the next one we output has the PCR
      bDiscontinuity = true;
      if ((iRc = Synch()) != 0) {
        m_pTsProvider->ReleasePacket(pPacket);
        return iRc;
      }
    }
  }

  // remember to warn of discontinuity if necessary
  if (bDiscontinuity) {
    pPacket->SetDiscontinuityFlag();
  }

  // OK, add the packet to the list
  pPackets->Push(pPacket);

  // return status
  return iRc;
}

//------------------------------------------------------------------------------
// Get Previous Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_Ts2TsMpegConverter::GetPrevTsPackets(C_Fifo<C_TsPacket> * pPackets,
                                                                      int skip)
{
  s64 iRc = 0;

  // perform any seeking necessary
  if (skip>0)
  {
    iRc = m_pReader->Seek(- CorrectRewind(skip*TS_PACKET_LEN), FILE_SEEK_CURRENT);
    if (iRc == MPEG_BEGINOFSTREAM)
    {
      C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
      LogDbg(m_hLog, "Begin of program \"" + strPgrmName + "\" reached");

      // Jump to start of stream
      iRc = m_pReader->Seek(0, FILE_SEEK_BEGIN);
      ASSERT(iRc == NO_ERR);
      return MPEG_BEGINOFSTREAM;
    }
    else if (iRc != NO_ERR)
    {
      C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
      Log(m_hLog, LOG_ERROR, "Read error for program \"" + strPgrmName + "\"");
      return iRc;
    }
  }

  // read the data.
  return GetNextTsPackets(pPackets, 0);
}


//------------------------------------------------------------------------------
// Synchronize in the TS stream
//------------------------------------------------------------------------------
int C_Ts2TsMpegConverter::Synch()
{
	unsigned char c;
	int iRc;

	// loop until we find a sync byte or an error
	c = 0;
	iRc = 0;
  while((c != 0x47) && (!iRc)) {
		iRc = m_pReader->Read(&c, 1);
		if ((iRc == FILE_EOF) || (m_pReader->EndOfStream())) {
      if (m_iLoop) {
        // try and reset the stream
        m_pReader->ResetEndOfStream();

        // if that was successful, do loop setup stuff
        if (!m_pReader->EndOfStream()) {
          m_iLoop--;
					iRc = 0;
        }
      } else {
        // not looping.
        iRc = MPEG_ENDOFSTREAM;
      }			
		} else if (iRc == 1) {
			// successful read (but not necessarily a sync byte)
			iRc = 0;
		}
	}
	
	return iRc;
}

void C_Ts2TsMpegConverter::ZeroPacket(C_TsPacket* pPacket) {
  (*pPacket)[0] = 0x47;
  for(int i = 1; i < TS_PACKET_LEN; i++)
    (*pPacket)[i] = 0;
}
