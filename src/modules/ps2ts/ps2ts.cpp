/*******************************************************************************
* ps2ts.cpp: threaded PS to TS converter
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#if defined(HAVE_INTTYPES_H)
#   include <inttypes.h>
#elif defined(HAVE_STDINT_H)
#   include <stdint.h>
#endif

#ifdef HAVE_DVBPSI_DVBPSI_H
#   include <dvbpsi/dvbpsi.h>
#   include <dvbpsi/descriptor.h>
#   include <dvbpsi/pat.h>
#   include <dvbpsi/pmt.h>
#   include <dvbpsi/dr.h>
#else
#   include "src/dvbpsi.h"
#   include "src/descriptor.h"
#   include "src/tables/pat.h"
#   include "src/tables/pmt.h"
#   include "src/descriptors/dr.h"
#endif

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/streamdescr.h"

#include "../../server/buffer.h"
#include "../../server/program.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"

#include "../../mpeg/dvbpsi.h"
#include "../../mpeg/ps2ts.h"
#include "../../mpeg/reader.h"
#include "../../mpeg/converter.h"

#include "ps2ts.h"

#include "../../mpeg/ps2ts.cpp"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_Ps2TsMpegConverterModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_ps2ts(handle hLog)
{
  return new C_Ps2TsMpegConverterModule(hLog);
}
#endif


/*******************************************************************************
* C_Ps2TsMpegConverter
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Ps2TsMpegConverter::C_Ps2TsMpegConverter(C_Module* pModule,
                                        C_MpegConverterConfig& cConfig) :
        C_MpegConverter(pModule, cConfig),
        m_cConverter(cConfig.m_pReader,
                     cConfig.m_pTsProvider, 2,
                     cConfig.m_pBroadcast->GetOption("mpegversion").ToInt(),
                     cConfig.m_pBroadcast->GetOption("loop").ToInt(),
                     m_hLog)
{
  m_bPreParse = (cConfig.m_pBroadcast->GetOption("preparse") == "1");

  LogDbg(m_hLog,
         C_String("Mpeg version : ") +
         cConfig.m_pBroadcast->GetOption("mpegversion").ToInt());
  LogDbg(m_hLog, C_String("Preparsing : ") + (m_bPreParse ? "yes" : "no"));
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_ProgramDescriptor* C_Ps2TsMpegConverter::GetPgrmDescriptor()
{
  return m_cConverter.GetPgrmDescriptor();
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_Ps2TsMpegConverter::InitWork()
{
  C_MpegConverter::InitWork();

  m_cConverter.Synch();

  if(m_bPreParse)
  {
    for(unsigned int i = 0; i < m_pReader->Size() / 188 / 256; i++)
    {
      // preparsing to make the PAT and the PMT
      C_TsPacket* pPacket = m_cConverter.GetPacket(true);
      if (!pPacket) break;
      m_pTsProvider->ReleasePacket(pPacket);
    }

    // Back to the beginning
    m_pReader->Seek(0, FILE_SEEK_BEGIN);
    m_cConverter.Synch();
  }
}

//------------------------------------------------------------------------------
// Get Next Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_Ps2TsMpegConverter::GetNextTsPackets(C_Fifo<C_TsPacket>* pPackets, int)
{
  s64 iRc = 0;
  bool bDiscontinuity = false;

  // Temporary
  C_TsPacket* pPacket = m_cConverter.GetPacket();
  if (pPacket) pPackets->Push(pPacket);

  switch(m_cConverter.GetStatus())
  {
  case 0:
    if(!pPacket) {
      iRc = MPEG_STREAMERROR;
    }
    break;
  case -99:
    iRc = MPEG_ENDOFSTREAM;
    break;
  default:
    iRc = MPEG_STREAMERROR;
    break;
  }

  // Check stream discontinuity
  if( !iRc
      && (m_pReader->HasDiscontinuity() || bDiscontinuity)
      && pPacket->HasPCR())
  {
    ASSERT(pPacket);
    ASSERT(pPacket->SetDiscontinuityFlag());
    pPacket->SetDiscontinuityFlag();
    m_pReader->ResetDiscontinuity();
    bDiscontinuity = false;
  }

  if(iRc == MPEG_ENDOFSTREAM)
  {
    C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
    LogDbg(m_hLog, "End of program \"" + strPgrmName + "\" reached");
    // Don't need to release here, trickplay module should do it
    // m_pTsProvider->ReleasePacket(pPacket);
  }
  else if (iRc != NO_ERR)
  {
    C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
    Log(m_hLog, LOG_ERROR, "Read error for program \"" + strPgrmName + "\"");
    // Don't need to release here, trickplay module should do it
    // m_pTsProvider->ReleasePacket(pPacket);
  }

  return iRc;
}

//------------------------------------------------------------------------------
// Get Previous Ts Packet from the Reader
//------------------------------------------------------------------------------
s64 C_Ps2TsMpegConverter::GetPrevTsPackets(C_Fifo<C_TsPacket>*, int)
{
  return MPEG_BEGINOFSTREAM;
}

