/*******************************************************************************
* telnetinterface.h: Telnet class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: telnetinterface.h,v 1.3 2003/08/04 08:27:23 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _TELNETINTERFACE_H_
#define _TELNETINTERFACE_H_

//------------------------------------------------------------------------------
// E_Telnet class
//------------------------------------------------------------------------------
class E_TelnetInterface : public E_Exception
{
 public:
  E_TelnetInterface(const C_String& strMsg);
  E_TelnetInterface(const C_String& strMsg, const E_Exception& e);
};


//------------------------------------------------------------------------------
// C_TelnetInterfaceOptions class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_TelnetInterfaceOptions
{
 public:
  C_Buffer<byte> Request(byte bAction, byte bOption);
  C_Buffer<byte> Answer(const C_Buffer<byte>& cRequest);
  int GetOptionStatus();

 private:
  // Local NVT settings
  bool m_bLocalEcho;
  bool m_bLocalSGA;
  bool m_bLocalLineMode;

  // Remote NVT settings
  bool m_bRemoteEcho;
  bool m_bRemoteSGA;
  bool m_bRemoteLineMode;

  // Pending negociations
  C_List< C_Buffer<byte> > m_cPendingRequests;
};


/*//------------------------------------------------------------------------------
// C_TelnetInterfaceLogin class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_TelnetInterfaceLogin
{
 public:
  C_TelnetInterfaceLogin();

  const C_String& GetLogin() const;
  void SetLogin(const C_String& strLogin);
  void ResetLogin();

  bool IsLoginSet() const;

 private:
  C_String m_strLogin;
  bool m_bLoginSet;
};*/



//------------------------------------------------------------------------------
// C_TelnetInterfaceCmdLine class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_TelnetInterfaceCmdLine
{
 public:
  // Constructor
  C_TelnetInterfaceCmdLine();

  // Local and remote command line construction
  C_Buffer<byte> Edit(const C_Buffer<byte>& cCmd);
  C_String Append(byte bByte);

  // Local command line storage
  void StartNewLine();
  void DropLine();

  // Cmd line retrieval
  C_String GetCmdLine(C_String);

 private:
  // Current command line
  C_String m_strLine;
  unsigned int m_iPosInLine;

  // Command line history
  C_List<C_String> m_cHistory;
  C_ListIterator<C_String> m_cCurrent;
};


//------------------------------------------------------------------------------
// C_TelnetInterfaceSession class
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_TelnetInterfaceSession : public C_InterfaceSession
{
 public:
  C_TelnetInterfaceSession(C_Socket* pConnection, void* pAdmin);

  // Data processing
  virtual void ProcessData();

  C_String m_strPeerName;

 protected:
  virtual void OnInit();
  virtual void OnClose();

  void HandleByte(byte bChar);
  void HandleLine();

  void ExecLogin();
  void ExecCommand();

  void SendAnswer(const C_Answer& cAnswer, const C_String& strPrefix = "");

 private:

  // Session status
  int m_iMode;

  // Internal data
  C_String m_strPath;
  C_String m_strPrompt;
  C_String m_strLogin;
  C_TelnetInterfaceCmdLine m_cCmdLine;
  C_TelnetInterfaceOptions m_cOptions;
  C_Buffer<byte> m_cBuffer;
};



//------------------------------------------------------------------------------
// C_TelnetInterface class
//------------------------------------------------------------------------------
class C_TelnetInterface : public C_Interface,
                          protected C_ConnectionsHandler<C_TelnetInterfaceSession>
{
 public:
  C_TelnetInterface(C_Module* pModule, C_InterfaceConfig& cConfig);

  int Init();
  int Run();
  int Stop();
  int Destroy();

 protected:

  virtual void InitSession();
  virtual void StopSession();
  virtual void RunSession();
  virtual void DestroySession();

  
// private:
  // Helpers
//  handle m_hLog;
//  C_Admin* m_pAdmin;

};


DECLARE_MODULE(Telnet, Interface, "telnet", C_InterfaceConfig&);

#else
#error "Multiple inclusions of telnetadmin.h"
#endif

