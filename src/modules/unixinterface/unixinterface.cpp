/*******************************************************************************
* unixinterface.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: unixinterface.cpp,v 1.1 2003/10/30 20:53:53 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Tristan Leteurtre <tristan.leteurtre@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*******************************************************************************/



//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <ctype.h>

//#include "../../server/config.h"
#include "../../core/core.h"
#include "../../core/network.h"
#include "../../server/request.h"
#include "../../server/repository.h"
#include "../../server/admin.h"
#include "../../server/interface.h"
#include "unixinterface.h"

#include "../../core/network.cpp"

#include "../../server/repository.cpp"

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_UnixInterfaceModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC	
C_Module* NewBuiltin_unixinterface(handle hLog)
{
  return new C_UnixInterfaceModule(hLog);
}
#endif



//------------------------------------------------------------------------------
// Unix interface
//------------------------------------------------------------------------------
#define WELCOME         "Videolan Server Administration System"
#define LOGIN           "Login: "
#define PASSWD          "Password: "
#define PROMPT          "vls:"
#define PROMPTEND       "# "

/*******************************************************************************
* E_UnixInterface exception
********************************************************************************
*
*******************************************************************************/
E_UnixInterface::E_UnixInterface(const C_String& strMsg) : E_Exception(GEN_ERR, strMsg)
{
}

E_UnixInterface::E_UnixInterface(const C_String& strMsg, const E_Exception& e) :
             E_Exception(GEN_ERR, strMsg, e)
{
}


/*******************************************************************************
* C_UnixInterfaceSession class
********************************************************************************
*
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_UnixInterfaceSession::C_UnixInterfaceSession(C_Socket* pConnection, void* pAdmin) :
                      C_InterfaceSession(pConnection,pAdmin),
                      m_strPeerName("[Unknown]"),
                      m_cBuffer(3)
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_UnixInterfaceSession::OnInit()
{
  // Actual initialisation is done internally in small pieces cause it could
  // block the entire server if done in one single step. For now just send
  // the welcome message
  m_cStream << "\r\n" << WELCOME << "\r\n\r\n";
  m_strLogin = "admin";

  // We assume that login and access management is done using an external
  // program, so deliver full right access to commands.
  m_pAdmin->SetSuperUser();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_UnixInterfaceSession::OnClose()
{
  try
  {
    // Send a goodby message if possible
    if(m_cStream.GetState() == STREAM_READY)
      m_cStream << "\r\nClosing connection" << "\r\n";

    // Close the connection
    m_pConnection->Close();
  }
  catch(E_Exception e)
  {
    throw E_UnixInterface("Unable to close connection with "+m_strPeerName, e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_UnixInterfaceSession::ProcessData()
{
  try
  {
    // First get the data from the network
    C_Buffer<byte> cData(256);
    m_cStream >> cData;
    if(cData.GetSize() <= 0)
      throw E_UnixInterface("Connection reset by peer");

    unsigned int iPos = 0;
    unsigned int iRef = 0;
    unsigned int iSize = cData.GetSize();

    while(iPos < iSize)
    {
      if(cData[iPos] == '\r' || cData[iPos] == '\n')
      {
        char msg[iPos-iRef+1];
        memcpy(msg, &cData[iRef], iPos-iRef);
        msg[iPos-iRef] = '\0';
//        m_strMessage += msg;
        m_strCmd = C_String(msg);
        //printf("Command : %s\n", msg);
        HandleLine();
        iRef = iPos + 1;
      }

      // Go to next char in input
      iPos++;
    }
    char msg[iPos-iRef+1];
    memcpy(msg, &cData[iRef], iPos-iRef);
    msg[iPos-iRef] = '\0';
//    m_strMessage += msg;
  }
  catch(E_Stream<C_Socket> e)
  {
    throw E_UnixInterface("Connection error", e);
  }  
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_UnixInterfaceSession::HandleByte(byte bByte)
{
  printf("Handle byte char: '%c' (%x)\n", bByte, bByte);

//      if(iStatus == YES)
      {
        // Remote echo
//        C_String cRemoteCmdLine = m_cCmdLine.Append(bByte);
//        if(m_iPhase != PASSWD_PHASE)
//          m_cStream << cRemoteCmdLine;
      }
//      else if(iStatus == NO)
//        throw E_UnixInterface("Client not supported");

}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_UnixInterfaceSession::HandleLine()
{
//  ASSERT(m_iPhase == COMMAND_PHASE);

  //C_String strCmd = m_cCmdLine.GetCmdLine(m_strPath);

  C_Request cRequest("");
  C_Answer cAdminAnswer = m_pAdmin->ParseCmdLine(this, m_strCmd, cRequest);

  switch(cAdminAnswer.GetStatus())
  {
  case ADMIN_WELLFORMED_COMMAND:
    if(cRequest.GetCmd() == "logout")
    {
      throw E_UnixInterface("logout requested");
    }
    else if (cRequest.GetCmd() == "cd")
    {
        // TODO find something a bit more generic ;-)
        // nitrox
        if(m_strPath == "" && cRequest.GetArg("path") == "broadcast")
        {
            m_strPath = "broadcast";
        }
        else if( cRequest.GetArg("path") == "..")
        {
            m_strPath = "";
        }
        else if(  m_strPath == "" && cRequest.GetArg("path") == "channel")
        {
            m_strPath = "channel";
        }
        else if(  m_strPath == "" && cRequest.GetArg("path") == "input")
        {
            m_strPath = "input";
        }
        else if(  m_strPath == "" && cRequest.GetArg("path") == "program")
        {
            m_strPath = "program";
        }
        m_strPrompt = m_strLogin + "@" + PROMPT + m_strPath + PROMPTEND;
    }
    else
    {
      C_Answer cAnswer = m_pAdmin->HandleRequest(cRequest);
      SendAnswer(cAnswer);
    }
    break;
  case ADMIN_COMMAND_NOT_VALID:
  case ADMIN_UNKNOWN_COMMAND:
  case ADMIN_COMMAND_DENIED:
    SendAnswer(cAdminAnswer);
    break;
  case ADMIN_EMPTY_COMMAND:
    // Nothing to do
    break;
  default:
    ASSERT(false);
    break;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Send a C_Answer in human readable format
//------------------------------------------------------------------------------
void C_UnixInterfaceSession::SendAnswer(const C_Answer& cAnswer,
                                 const C_String& strPrefix)
{
  if(cAnswer.GetProvider() != "")
  {
    m_cStream << "\r\n";
#ifdef DEBUG
    m_cStream << strPrefix + "Provider: " + cAnswer.GetProvider() + "\r\n";

    int iStatus = cAnswer.GetStatus();
    if(iStatus >= 0)
      m_cStream << strPrefix + "Status: " + iStatus + "\r\n";
    else
      m_cStream << strPrefix + "Error: " + iStatus + "\r\n";
#endif
    C_List<C_String> cMessageList = cAnswer.GetMessages();
    unsigned int iSize = cMessageList.Size();
    for(unsigned int i = 0; i < iSize; i++)
    {
#ifdef DEBUG
      m_cStream << strPrefix + "Info: " + cMessageList[i] + "\r\n";
#else
      m_cStream << strPrefix + cMessageList[i] + "\r\n";
#endif
    }

    C_List<C_Answer> cAnswerList = cAnswer.GetSubAnswers();
    iSize = cAnswerList.Size();
    for (unsigned int j = 0; j < iSize; j++)
    {
      SendAnswer(cAnswerList[j], strPrefix+"  ");
    }
  }
}





/*******************************************************************************
* C_UnixInterface class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_UnixInterface::C_UnixInterface(C_Module* pModule, C_InterfaceConfig& cConfig) :
              C_Interface(pModule, cConfig),
              C_ConnectionsHandler<C_UnixInterfaceSession>(cConfig.hLog,cConfig.pAdmin)
{
}

//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_UnixInterface::Init()
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  int iRc = NO_ERR;

  // Get config
  C_String strSocketPath = pApp->GetSetting(m_sInterfaceName+".socketpath",
                                        "/tmp/vls-unix");

  int iDomain = AF_UNIX;

    // Init the socket
    try
    {
      C_ConnectionsHandler<C_UnixInterfaceSession>::Init(iDomain, strSocketPath, 0);
    }
    catch(E_Exception e)
    {
      Log(C_Interface::m_hLog, LOG_ERROR,
          m_sInterfaceName+" server initialisation failed:\n" + e.Dump());
      iRc = GEN_ERR;
    }

    if(!iRc)
    {
     Log(C_Interface::m_hLog, LOG_NOTE, m_sInterfaceName+" server initialised");
    }
  return iRc;
}

void C_UnixInterface::InitSession()
{
}

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
void C_UnixInterface::RunSession()
{
    C_ConnectionsHandler<C_UnixInterfaceSession>::Run();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_UnixInterface::StopSession()
{
    C_ConnectionsHandler<C_UnixInterfaceSession>::Stop();
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_UnixInterface::DestroySession()
{
    C_ConnectionsHandler<C_UnixInterfaceSession>::Destroy();
}

