/*******************************************************************************
* netoutput.h: network output
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: netoutput.h,v 1.12 2004/02/16 17:07:48 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _NETOUTPUT_H_
#define _NETOUTPUT_H_


//------------------------------------------------------------------------------
// C_NetOutput: output streaming over networks
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_NetOutput : public C_Output
{
 public:
  C_NetOutput(const C_ChannelConfig& cConfig);
  virtual ~C_NetOutput();

  virtual void OnInit();
  virtual void OnClose();

 protected:
  virtual void WriteToPort(bool RtpEncapsulation, u32 RtpSendTime);

  C_Socket m_cSocket;
  C_SocketBuff m_cSocketBuff;

  // Output parameters
  C_String m_strSrcHost;
  C_String m_strSrcPort;
  C_String m_strDstHost;
  C_String m_strDstPort;
  C_String m_strType;
  C_String m_strInterface;
  int m_iTTL;

#ifdef BUGGY_VLC
  virtual int PrivateWriteTo(int iBuffLen) = 0;

  byte m_ByteBuff[RTP_HEADER_LEN + TS_PACKET_LEN * TS_IN_ETHER];
#endif

};


//------------------------------------------------------------------------------
// C_Net4Output: output streaming over IPv4 networks
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_Net4Output : public C_NetOutput
{
 public:
  C_Net4Output(const C_ChannelConfig& cConfig);
  virtual ~C_Net4Output();

  virtual void OnInit();
  virtual void OnClose();

 protected:
#ifdef BUGGY_VLC
  virtual int PrivateWriteTo(int iBuffLen);

  C_Inet4Addr m_cOutputInetAddr;
#endif
};


//------------------------------------------------------------------------------
// C_Net6Output: output streaming over IPv6 networks
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef HAVE_IPV6
class C_Net6Output : public C_NetOutput
{
 public:
  C_Net6Output(const C_ChannelConfig& cConfig);
  virtual ~C_Net6Output();

  virtual void OnInit();
  virtual void OnClose();

 protected:
#ifdef BUGGY_VLC
  virtual int PrivateWriteTo(int iBuffLen);

  C_Inet6Addr m_cOutputInetAddr;
#endif
};
#endif

//------------------------------------------------------------------------------
// C_UnixOutput: output streaming over unix sockets
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
class C_UnixOutput : public C_NetOutput
{
 public:
  C_UnixOutput(const C_ChannelConfig& cConfig);
  virtual ~C_UnixOutput();

  virtual void OnInit();
  virtual void OnClose();

 protected:
#ifdef BUGGY_VLC
  virtual int PrivateWriteTo(int iBuffLen);

  C_UnixAddr m_cOutputInetAddr;
#endif
};
#endif


#else
#error "Multiple inclusions of netoutput.h"
#endif

