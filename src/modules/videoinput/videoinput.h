/*******************************************************************************
* videoinput.h: Video Input class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: videoinput.h,v 1.7 2003/09/10 12:57:52 nitrox Exp $
*
* Authors: Cyril Deguet <asmax@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This input has been designed to handle Video4linux devices
*
*-------------------------------------------------------------------------------
* Current status:
*
*******************************************************************************/

#ifndef _VIDEO_INPUT_H_
#define _VIDEO_INPUT_H_


//------------------------------------------------------------------------------
// C_VideoInput class
//------------------------------------------------------------------------------
class C_VideoInput : public C_Input
{
public:
  C_VideoInput(C_Module* pModule, const C_String& strName);
  ~C_VideoInput();

  virtual C_String GetType() {return C_String("video");}
protected:
  // Init/termination methods
  virtual void OnInit();
  virtual void OnDestroy();

  // Selection of the pgrms to broadcast
  virtual void OnStartStreaming(C_Broadcast* pBroadcast);
  virtual void OnResumeStreaming(C_Broadcast* pBroadcast);
  virtual void OnSuspendStreaming(C_Broadcast* pBroadcast);
  virtual void OnForwardStreaming(C_Broadcast* pBroadcast,int speed);
  virtual void OnRewindStreaming(C_Broadcast* pBroadcast, int speed);
  virtual void OnStopStreaming(C_Broadcast* pBroadcast);
  virtual void OnUpdateProgram(C_String strProgram,
                                     C_String strFileName, C_String strType);
  virtual void OnDeleteProgram(C_String strProgram);

  virtual C_List<C_Program> OnGetAvailablePgrms();

private:
  // Input configuration
  C_String m_strDevice;
  C_String m_strType;
  C_String m_strTrickPlayType;
  
  // Programs names
  // the program ID is m_vProgramNames.Find(strName) + 1
  C_Vector<C_String> m_vProgramNames;

  // List of currently running readers and streamers
  C_HashTable<handle, C_TsStreamer> m_cStreamers;
  C_HashTable<handle, C_TrickPlay> m_cTrickPlay;
};


// Declaration and implementation of C_VideoInputModule
DECLARE_MODULE(Video, Input, "video", const C_String&);

#else
#error "Multiple inclusions of videoinput.h"
#endif

