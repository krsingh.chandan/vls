/*******************************************************************************
* videoinput.cpp: Video4linux streams
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Cyril Deguet <asmax@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../../core/core.h"

#include "../../mpeg/mpeg.h"
#include "../../mpeg/ts.h"
#include "../../mpeg/rtp.h"

#include "../../server/program.h"
#include "../../server/buffer.h"
#include "../../server/output.h"
#include "../../server/channel.h"
#include "../../server/broadcast.h"
#include "../../server/request.h"
#include "../../server/input.h"
#include "../../server/tsstreamer.h"

#include "../../mpeg/reader.h"
#include "../../mpeg/trickplay.h"
#include "../../mpeg/converter.h"

#include "videoinput.h"


//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_VideoInputModule, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_videoinput(handle hLog)
{
  return new C_VideoInputModule(hLog);
}
#endif


/*******************************************************************************
* C_VideoInput class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_VideoInput::C_VideoInput(C_Module* pModule,
                           const C_String& strName) : C_Input(pModule,
                                                              strName)
{
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_VideoInput::~C_VideoInput()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
void C_VideoInput::OnInit()
{
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  // Retrieve the configuration from vls.cfg
  m_strDevice = pApp->GetSetting(GetName() + ".device", "/dev/video");
  m_strType = pApp->GetSetting(GetName() + ".type", "mpeg2-ps").ToLower();
  
 // Jpsaman stuff :P
   m_strTrickPlayType = pApp->GetSetting(GetName() + ".trickplay",
                      "normal").ToLower();
 
  // Build the program list : there is only one program named "video"
  C_String* pStr = new C_String("video");
  m_vProgramNames.Add(pStr);
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
void C_VideoInput::OnDestroy()
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_List<C_Program> C_VideoInput::OnGetAvailablePgrms()
{
  C_List<C_Program> cPgrmList;

  C_Program* pProgram = new C_Program(/*1,*/ m_vProgramNames[0]);
  ASSERT(pProgram);
  cPgrmList.PushEnd(pProgram);

  return cPgrmList;
}


//------------------------------------------------------------------------------
// Start the reception of the given program
//------------------------------------------------------------------------------
void C_VideoInput::OnStartStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);
  C_Application * pApp = C_Application::GetApp();

  // We choose a TS packet buffer able to store up to 3s of stream at 8 Mbits/s
  C_NetList* pTsProvider = new C_NetList(3*3*797);

  // The netlist must be at least of the size of the Reader buffer +
  // the size of the output buffer + 2 to be sure that there are always free
  // packets in it
  const C_Channel* pChannel = pBroadcast->GetChannel();
  ASSERT(pTsProvider->Capacity() - pChannel->GetBuffCapacity() - 2 > 0);
  unsigned int uiSize = pTsProvider->Capacity() -pChannel->GetBuffCapacity()-2;
  C_SyncFifo* pBuffer;

  // PVR Parameters sent to the reader module.
  C_String strNorm = pApp->GetSetting(GetName() + ".norm", "pal").ToLower();
  pBroadcast->SetOption("pvr_norm", strNorm);

  C_String strFrequency = pApp->GetSetting(GetName() + ".frequency", "567250").ToLower();
  pBroadcast->SetOption("pvr_frequency", strFrequency);

  C_String strFrameRate = pApp->GetSetting(GetName() + ".framerate", "25").ToLower();
  pBroadcast->SetOption("pvr_framerate", strFrameRate);

  C_String strSize = pApp->GetSetting(GetName() + ".size", "").ToLower();
  pBroadcast->SetOption("pvr_size", strSize);

  C_String strBitRate = pApp->GetSetting(GetName() + ".bitrate", "3000").ToLower();
  pBroadcast->SetOption("pvr_bitrate", strBitRate);

  C_String strMaxBitRate = pApp->GetSetting(GetName() + ".maxbitrate", "4000").ToLower();
  pBroadcast->SetOption("pvr_paxbitrate", strMaxBitRate);

  C_String strBitRateMode = pApp->GetSetting(GetName() + ".bitratemode", "cbr").ToLower();
  pBroadcast->SetOption("pvr_bitratemode", strBitRateMode);

  C_String strInputNumber = pApp->GetSetting(GetName() + ".inputnumber"
                                                        , "0").ToLower();
  pBroadcast->SetOption("pvr_inputnumber", strInputNumber);


  
  C_String strReaderType;
  C_String strConverterType;
  
  // Specific behaviour depends on the type
  if(m_strType == "mpeg2-ps")        // Mpeg 2 - Program Stream
  {
    // Update the size of the buffer and create it
    uiSize -= 2;
    pBuffer = new C_SyncFifo(uiSize);

    // Converter configuration
    strConverterType = "ps2ts";
    pBroadcast->SetOption("mpegversion", "2");
    pBroadcast->SetOption("preparse", "0");
  }
  else if(m_strType == "mpeg2-ts")        // Mpeg 2 - Transport Stream
  {
    // Update the size of the buffer and create it
    pBuffer = new C_SyncFifo(uiSize);

    // Converter configuration
    strConverterType = "ts2ts";
  }
  else
  {
    delete pTsProvider;
    throw E_Exception(GEN_ERR, "Unsupported or unknown type : " + m_strType);
  }

  C_String strCardType = pApp->GetSetting(GetName() + ".cardtype", "kfir").ToLower();

  if (strCardType=="kfir")
  {
    pBroadcast->SetOption("filename", m_strDevice);
    strReaderType = "file";
  }
  else
  {
    pBroadcast->SetOption("pvr_device", m_strDevice);
    strReaderType = "pvr";
  }
  
  C_MpegReader* pReader;

  // Create the reader
  C_MpegReaderModule* pReaderModule = (C_MpegReaderModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegreader",
                                                    strReaderType);

  if(pReaderModule)
  {
    pReader = pReaderModule->NewMpegReader(pBroadcast);
    ASSERT(pReader);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module mpegreader:" + strReaderType +
                      " not present");
  }

  C_MpegConverter* pConverter;

  // Create the converter
  C_MpegConverterModule* pConverterModule = (C_MpegConverterModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("mpegconverter",
                                                    strConverterType);

  if(pConverterModule)
  {
    C_MpegConverterConfig cConfig;
    cConfig.m_hLog = m_hLog;
    cConfig.m_pBroadcast = pBroadcast;
    cConfig.m_pReader = pReader;
    cConfig.m_pTsProvider = pTsProvider;
    cConfig.m_pEventHandler = m_pEventHandler;
    pConverter = pConverterModule->NewMpegConverter(cConfig);
    ASSERT(pConverter);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module mpegconverter:" + strConverterType +
                      " not present");
  }

  pReader->SetConverter(pConverter);

  // Create the TrickPlay
  C_TrickPlay* pTrickPlay;

  C_TrickPlayModule* pTrickPlayModule = (C_TrickPlayModule*)
                                C_Application::GetModuleManager()
                                        ->GetModule("trickplay",
                                                    m_strTrickPlayType);
  if (pTrickPlayModule)
  {
    C_TrickPlayConfig cTrickPlayConfig;
    cTrickPlayConfig.m_hLog = m_hLog;
    cTrickPlayConfig.m_pBroadcast = pBroadcast;
    cTrickPlayConfig.m_pReader = pReader;
    cTrickPlayConfig.m_pHandler = pBuffer;
    cTrickPlayConfig.m_iInitFill = pBuffer->Capacity();
    cTrickPlayConfig.m_pEventHandler = m_pEventHandler;
    cTrickPlayConfig.m_pTsProvider = pTsProvider;
    cTrickPlayConfig.m_pConverter = pConverter;

    pTrickPlay = pTrickPlayModule->NewTrickPlay(cTrickPlayConfig);
    ASSERT(pTrickPlay);
  }
  else
  {
    throw E_Exception(GEN_ERR,
                      "Module TrickPlay:" + m_strTrickPlayType +
                      " not present");
  }

  // Create the streamer
  C_TsStreamer* pStreamer = new C_TsStreamer(m_hLog, pBroadcast,
                                             pTsProvider, pBuffer,
                                             m_pEventHandler, true, false);

  ASSERT(pReader);
  ASSERT(pConverter);
  ASSERT(pTrickPlay);
  ASSERT(pStreamer);

  m_cTrickPlay.Add(pBroadcast, pTrickPlay);
  m_cStreamers.Add(pBroadcast, pStreamer);

  try
  {
    pTrickPlay->Create();
    pStreamer->Create();
  }
  catch(E_Exception e)
  {
    pStreamer->Stop();
    pTrickPlay->Stop();

    // Unregister the 2 thread and delete them
    m_cTrickPlay.Delete(pBroadcast);
    m_cStreamers.Delete(pBroadcast);

    throw E_Exception(GEN_ERR, "unable to start streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Resume the reception of the given program
//------------------------------------------------------------------------------
void C_VideoInput::OnResumeStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the trickplay module that handles this broadcast
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  // Resume the thread
  try
  {
    pTrickPlay->Resume();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}


//------------------------------------------------------------------------------
// Suspend the reception of the given program
//------------------------------------------------------------------------------
void C_VideoInput::OnSuspendStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the trickplay module that handles this broadcast
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Get(pBroadcast);
  ASSERT(pTrickPlay);

  C_String strPgrmName = pBroadcast->GetProgram()->GetName();

  // Suspend the thread
  try
  {
    pTrickPlay->Suspend();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to resume streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
// Forward the reception of the given program
//------------------------------------------------------------------------------
void C_VideoInput::OnForwardStreaming(C_Broadcast*, int)
{
}

//------------------------------------------------------------------------------
// Rewind the reception of the given program
//------------------------------------------------------------------------------
void C_VideoInput::OnRewindStreaming(C_Broadcast*, int)
{  
}

//------------------------------------------------------------------------------
// Stop the reception of the given program
//------------------------------------------------------------------------------
void C_VideoInput::OnStopStreaming(C_Broadcast* pBroadcast)
{
  ASSERT(pBroadcast);

  // Find the trickplay module that handles this broadcast
  C_TrickPlay* pTrickPlay = m_cTrickPlay.Remove(pBroadcast);
  ASSERT(pTrickPlay);
  C_TsStreamer* pStreamer = m_cStreamers.Remove(pBroadcast);
  ASSERT(pStreamer);

  // Stop the threads
  try
  {
    pStreamer->Stop();
    pTrickPlay->Stop();
    delete pTrickPlay;
    delete pStreamer; // streamer MUST be deleted last
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to stop streaming of program "+
                      pBroadcast->GetProgram()->GetName(), e);
  }
}

//------------------------------------------------------------------------------
// Update configuration dynamically
//------------------------------------------------------------------------------
void C_VideoInput::OnUpdateProgram(C_String, C_String, C_String)
{
}

void C_VideoInput::OnDeleteProgram(C_String)
{
}
