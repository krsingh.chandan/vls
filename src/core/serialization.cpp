/*******************************************************************************
* serialization.cpp: Serialization support
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* The following classes provide an interface and some mechanisms that any
* class can use to become serializable
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <stdio.h>

#include "common.h"
#include "debug.h"
#include "reflect.h"
#include "serialization.h"




/*******************************************************************************
* C_Serializer
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Serializer::C_Serializer(const byte* pBaseAddress,
                           const C_ClassDescription& cDescription) :
                  m_cDescription(cDescription)
{
  ASSERT(pBaseAddress);
  m_pBaseAddress = pBaseAddress;

  m_iCurrentField = 0;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
u32 C_Serializer::NextBytesCount() const
{
  u32 iResult = 0;

  C_ClassField cField = m_cDescription.GetField(m_iCurrentField);

  byte bType = cField.GetFieldType();
  ASSERT(bType != UNKNOWN);

  switch(bType)
  {
    case SCALAR:
    {
      //iResult = size(cField.GetScalarType);.
      break;
    }
    case ARRAY:
    {
      //iResult = sizeof(u32) + cField.GetSize()*sizeof(cField.GetScalarType);
      break;
    }
    case CLASS:
    {
      //iResult = 
      break;
    }
    default:
      ASSERT(false);
  }

  return iResult;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const byte* C_Serializer::GetNextBytes()
{
  ASSERT(false);

  m_iCurrentField++;

  return NULL;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Serializer::SetNextBytes(const byte*)
{
  
  m_iCurrentField++;
}

