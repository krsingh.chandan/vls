/*******************************************************************************
* buffers.cpp: Buffers management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: buffers.cpp,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at list.h for further explanations



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Buffer<T>::C_Buffer(unsigned int iCapacity)
{
  m_iCapacity = iCapacity;
  m_iSize = 0;
  m_aData = new T[iCapacity];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Buffer<T>::C_Buffer(const C_Buffer<T>& cBuffer)
{
  m_iCapacity = cBuffer.m_iCapacity;
  m_iSize = cBuffer.m_iSize;
  m_aData = new T[m_iCapacity];
  for(unsigned int i = 0; i < m_iSize; i++)
  {
    m_aData[i] = cBuffer.m_aData[i];
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Buffer<T>::~C_Buffer()
{
  ASSERT(m_aData);
  delete[] m_aData;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T>
 C_Buffer<T>& C_Buffer<T>::operator = (const C_Buffer<T>& cBuffer)
{
  ASSERT(this != &cBuffer);
  ASSERT(m_aData);
  delete[] m_aData;
  
  m_iCapacity = cBuffer.m_iCapacity;
  m_iSize = cBuffer.m_iSize;
  m_aData = new T[m_iCapacity];
  for(unsigned int i = 0; i < m_iSize; i++)
  {
    m_aData[i] = cBuffer.m_aData[i];
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> unsigned int C_Buffer<T>::GetCapacity() const
{
  return m_iCapacity;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> unsigned int C_Buffer<T>::GetSize() const
{
  return m_iSize;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> T& C_Buffer<T>::operator[] (unsigned int iPosition) const
{
  ASSERT(iPosition < m_iCapacity);
  return m_aData[iPosition];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_Buffer<T>::SetSize(unsigned int iSize)
{
  ASSERT(iSize <= m_iCapacity)
  m_iSize = iSize;
}

