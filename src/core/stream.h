/*******************************************************************************
* stream.h: Stream class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: stream.h,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This class provides high level I/O methods to efficiently and quickly stream
* the base types of the core API
* It is based upon the base I/O classes (File, Socket, ...) that still can be
* used directly to achieve better performances
*
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/


#ifndef _STREAM_H_
#define _STREAM_H_


//------------------------------------------------------------------------------
// Stream state
//------------------------------------------------------------------------------
#define STREAM_READY    0
#define STREAM_ERROR    -1
#define STREAM_EOF      -2


//------------------------------------------------------------------------------
// Stream Exception
//------------------------------------------------------------------------------
template <class IOStream> class E_Stream : public E_Exception
{
 public:
  E_Stream(int iCode, const C_String& strMsg);

  E_Stream(int iCode, const C_String& strMsg, const E_Exception& eException);
};



//------------------------------------------------------------------------------
// C_Stream class
//------------------------------------------------------------------------------
// The C_Stream takes as argument an opened low level I/O stream, and builds
// upon it high level streaming methods. 
//------------------------------------------------------------------------------
template <class IOStream> class C_Stream
{
 public:
  C_Stream(IOStream* pIOStream);
  ~C_Stream();
  
  // The marker is a byte used to mark the dataflow in order to locate
  // the end of variable size datas (such as strings). Default marker is '\0'
  void SetMarker(byte bMarker)
  { m_bMarker = bMarker; }
  byte GetMarker() const
  { return m_bMarker; }

  // State of the stream (ready, error, eof)
  byte GetState() const
  { return m_bState; }

  // Get some info on the stream
  C_String GetName() const;
  C_String GetInfo() const;

  // Close the stream
  void Close();

  // Byte streaming
  C_Stream& operator << (const C_Buffer<byte>& cBuff);
  C_Stream& operator >> (C_Buffer<byte>& cBuff);

  // Text streaming
  C_Stream& operator << (const C_String& strBuff);
  C_Stream& operator >> (C_String& strBuff);

  // Streaming of serializable objects
  // Avoid to use those methods to store data, just use them to transmit them
  C_Stream& operator << (const C_Serializable& cObject);
  C_Stream& operator >> (C_Serializable& cObject);

 private:
  IOStream* m_pIOStream;
  byte m_bMarker;
  byte m_bState;
};



#else
#error "Multiple inclusions of stream.h"
#endif

