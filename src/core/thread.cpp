/*******************************************************************************
* thread.cpp: thread management
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id: thread.cpp,v 1.11 2003/10/27 10:58:10 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*------------------------------------------------------------------------------*
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#ifdef PTHREAD_COND_T_IN_PTHREAD_H
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#elif defined WIN32
#include <windows.h>
#include <process.h>
#endif

#include <stdio.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include "common.h"
#include "string.h"
#include "system.h"
#include "exception.h"
#include "list.h"
#include "thread.h"

#include "debug.h"
#include "list.cpp"



#define INT_TEST_COUNT 15      // Test count before calling interruption
                               // process
#define INT_TEST_DELAY 1000000 // Delay in microseconds between each test
                               // before calling interruption process


//******************************************************************************
// Class C_Thread
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Instanciation of the static list of running threads and of the thread killer
//------------------------------------------------------------------------------
C_List<C_Thread> C_Thread::s_cThreadList;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Thread::C_Thread(unsigned int iProperties)
{
  m_iFlags = iProperties;

  m_iStatus = THREAD_STATUS_NOTSTARTED;

#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  ZERO(tId);
#elif defined WIN32
  ZERO(hThread);
  ZERO(dwId);
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Thread::~C_Thread()
{
  // Verify that the thread has been stopped before
  ASSERT(s_cThreadList.Find(*this) < 0);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Thread::Create()
{ 
  try
  {
    // Do some initialisations before starting the real job
    InitWork();

    // Now launch the thread
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
    // Set the thread attributes. It is important for the thread to be created
    // joinable unless the instance of the class could be destroyed before the
    // end of the execution of DoWork 
    pthread_attr_t sAttr;
    pthread_attr_init(&sAttr);
    pthread_attr_setdetachstate(&sAttr, PTHREAD_CREATE_JOINABLE);

    int iRc = pthread_create(&tId, NULL, C_Thread::StartRoutine, this);
    
    pthread_attr_destroy(&sAttr);
    
#elif defined _WIN32
    unsigned long threadID;
    hThread = (HANDLE)CreateThread(NULL, 0, 
            (LPTHREAD_START_ROUTINE)C_Thread::StartRoutine, this, 0, &threadID);
    int iRc = ( hThread ? 0 : 1 );
#endif

    if(iRc)
    {
      throw E_Exception(GEN_ERR, "Unable to start thread: " + GetErrorMsg());
    }
  

    // Reference the thread in the list of running threads
    s_cThreadList.PushEnd(this);
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to create thread", e);
  }
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Thread::Cancel()
{
  try
  {
    // Ask for thread cancellation
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
    int iRc = pthread_cancel(tId);
#elif defined WIN32
    int iRc = 0;
#endif
    if(iRc)
      throw E_Exception(iRc, "Thread cancellation failed: " + GetErrorMsg()); 

    // Now Stop the cancelled thread
    Stop();
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Error when cancelling thread", e);
  }
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Thread::Stop()
{
  try
  {
    // As all our threads are created joinable, we must ensure that the calling
    // thread is a different thread to avoid a deadlock. So if a thread try to stop
    // itself, we ask another thread (the killer) to do the job.
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
    if(pthread_self() == tId)
#else
    if (GetCurrentThread() == hThread)
#endif
    {
        ASSERT(false);
        //s_cThreadKiller.Kill(this);
    }

    // Don't join if not started
    // It avoids exception's generation because pthread_join returns an error
    // when the thread was not started.
    else if(m_iStatus != THREAD_STATUS_NOTSTARTED)
    {
      // Ask the user work routine to stop its normal processing
      StopWork();
  
      // Try to join the thread in order to clean the resources it used and
      // to synchronise the calling thread with the destruction of this thread
      // Seems that there is a bug here in the libpthread: when the thread
      // is cancelled, pRc is set to any value so that there is no way to get
      // the return value of the DoWork method
      // Ok, let's try again to see if the pb still occurs
      E_Exception* p_eError = NULL;
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
      int iRc = pthread_join(tId, (void**)&p_eError);
#elif defined _WIN32
      int iRc = 0;
      WaitForSingleObject (hThread, INFINITE);
#endif
      if(iRc)
      {
        // Look at thre return code of the joined thread
        ASSERT(p_eError);
        E_Exception e(*p_eError);
        delete p_eError;
        throw e;
      }

      // Cleaning
      CleanWork();

      // Remove the thread from the list of running thread, but only if it has been
      // successfully stopped. Do not delete it, this is the responsability of the
      // user
      int iThreadPos = s_cThreadList.Find(*this);
      ASSERT(iThreadPos >= 0);
      s_cThreadList.Remove(iThreadPos);
    }
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR, "Unable to stop thread", e);
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Thread::Interrupt()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_cancel(tId);
#elif defined _WIN32
  int iRc = 0;
#endif
  if(iRc)
    throw E_Exception(GEN_ERR, "Unable to interrupt thread: " + GetErrorMsg());
  m_iStatus = THREAD_STATUS_INTERRUPTED;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Thread::DelayedInterruption()
{
  if(m_iStatus == THREAD_STATUS_STARTED)
  {
    for(unsigned int ui = 0; (ui < INT_TEST_COUNT) &&
                             (m_iStatus == THREAD_STATUS_STARTED); ui++)
    {
#ifdef WIN32
      Sleep(INT_TEST_DELAY / 1000000);
#else
      usleep(INT_TEST_DELAY);
#endif

    }
  }

  if(m_iStatus == THREAD_STATUS_STARTED)
  {
    printf("Thread not stopped after %u s,"
                " calling interruption process\n",
           INT_TEST_COUNT * INT_TEST_DELAY / 1000000);
    Interrupt();
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
bool C_Thread::operator == (const C_Thread& cThread)
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  return (this->tId == cThread.tId);
#elif defined WIN32
  return (this->hThread == cThread.hThread);
#endif
};


//------------------------------------------------------------------------------
// Return the identifier of the calling thread
//-----------------------------------------------------------------------------
// The result can be NULL if the calling thread has not been created through
// a C_Thread::Create (eg: the main thread, launched with the process)
// TO DO: create an iterator on lists an use it, it would be really faster
//-----------------------------------------------------------------------------
C_Thread* C_Thread::Self()
{
  C_Thread* pResult = NULL;

#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  pthread_t tId = pthread_self();
  for(unsigned int i = 0; i < s_cThreadList.Size(); i++)
  {
    C_Thread& cCurrent = s_cThreadList[i];
    if(cCurrent.tId == tId)
    {
      pResult = &cCurrent;
      break;
    }
  }
#elif defined _WIN32
  HANDLE hThread = GetCurrentThread();
  for(unsigned int i = 0; i < s_cThreadList.Size(); i++)
  {
    C_Thread& cCurrent = s_cThreadList[i];
    if(cCurrent.hThread == hThread)
    {
      pResult = &cCurrent;
      break;
    }
  }
#endif

  return pResult;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_Thread::IsRunning() const
{
  return (m_iStatus == THREAD_STATUS_STARTED);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// The purpose of this routine is only to allow the "extern C" pthread_create
// functions to call the DoWork() C++ method
//------------------------------------------------------------------------------
void* C_Thread::StartRoutine(void* pThread)
{
  C_Thread* pThis = (C_Thread*)pThread;
  E_Exception* p_eError = NULL;

  pThis->m_iStatus = THREAD_STATUS_STARTED;

  try
  {
    pThis->DoWork();
  }
  catch(E_Exception e)
  {
    p_eError = new E_Exception(e);
  }

  pThis->m_iStatus = THREAD_STATUS_STOPPED;

  return p_eError;
}





//******************************************************************************
// Class C_Mutex
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Mutex::C_Mutex()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_init(&m_sMutex, NULL);
  ASSERT(!iRc);         // The function shouldn't fail
  USE(iRc);

#elif defined _WIN32
  InitializeCriticalSection( &m_sMutex );
#endif  
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Mutex::~C_Mutex()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_destroy(&m_sMutex);
  ASSERT(!iRc);         // The mutex should not be locked when being destroyed
  USE(iRc);

#elif defined _WIN32
  DeleteCriticalSection( &m_sMutex );
#endif
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Mutex::Lock()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_lock(&m_sMutex);

  ASSERT(!iRc);         // Errors only happen with bad uses of the mutex
  USE(iRc);

#elif defined _WIN32
  EnterCriticalSection( &m_sMutex );
#endif  
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Mutex::UnLock()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_unlock(&m_sMutex);
  ASSERT(!iRc);         // Errors only happen with bad uses of the mutex
  USE(iRc);

#elif defined _WIN32
  LeaveCriticalSection( &m_sMutex );
#endif  
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// If the mutex is not locked, lock it and return NO_ERR, otherwise returns
// immediatly MUTEX_LOCKED
//------------------------------------------------------------------------------
int C_Mutex::TryLock()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_trylock(&m_sMutex);
  if(iRc == EBUSY)
    return MUTEX_LOCKED;
  else
  {
    ASSERT(iRc == 0);
    return NO_ERR;
  }

#elif defined _WIN32
  ASSERT(false);
  return GEN_ERR;
#endif
}


//******************************************************************************
// Class C_Semaphore
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Semaphore::C_Semaphore(unsigned int iInitialValue)
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
# ifdef USE_SEM_T
  int iRc = sem_init(&sSemaphore, 0, iInitialValue);
  ASSERT(!iRc)          // Should never fail
  USE(iRc);
# else
  pthread_mutex_init(&m_sMutex, NULL);
  pthread_cond_init(&m_sCondition, NULL);
  sSemaphore = iInitialValue;
# endif

#elif defined _WIN32
  sSemaphore = CreateSemaphore(NULL, iInitialValue, 32767, NULL);
#endif  
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Semaphore::~C_Semaphore()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
# ifdef USE_SEM_T
  int iRc = sem_destroy(&sSemaphore);
  ASSERT(!iRc);         // No thread should be waiting on it at that time
  USE(iRc);
# else
  pthread_mutex_destroy(&m_sMutex);
  pthread_cond_destroy(&m_sCondition);
# endif

#elif defined _WIN32
  CloseHandle(sSemaphore);
#endif
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int C_Semaphore::Post()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
# ifdef USE_SEM_T
  return sem_post(&sSemaphore);
# else
  pthread_mutex_lock(&m_sMutex);
  sSemaphore++;
  pthread_cond_signal(&m_sCondition);
  pthread_mutex_unlock(&m_sMutex);
  return 0;
# endif

#elif defined _WIN32
  return ReleaseSemaphore(sSemaphore, 1, NULL);
#endif  
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
int C_Semaphore::Wait()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
# ifdef USE_SEM_T
  return sem_wait(&sSemaphore);
# else
  pthread_mutex_lock(&m_sMutex);
  while (!sSemaphore)
  {
    pthread_cond_wait(&m_sCondition, &m_sMutex);
  }
  sSemaphore--;
  pthread_mutex_unlock(&m_sMutex);
  return 0;
# endif

#elif defined _WIN32
  return WaitForSingleObject(sSemaphore, INFINITE);
#endif
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Semaphore::TryWait()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
# ifdef USE_SEM_T
  int result = sem_trywait(&sSemaphore);
  if (result != 0) // Not only EAGAIN (thks Andrew Baumann)
    return -1;
  else
    return 0;
# else
  int result = pthread_mutex_trylock(&m_sMutex);
  if (result == EBUSY)
    return -1; // Something that says EBUSY
  while (!sSemaphore)
  {
    pthread_cond_wait(&m_sCondition, &m_sMutex);
  }
  sSemaphore--;
  pthread_mutex_unlock(&m_sMutex);
  return 0;
# endif

#elif defined _WIN32
  return TWaitForSingleObject(sSemaphore, 0); // Check the return value for WAIT_TIMEOUT
#endif
}


//******************************************************************************
// Class C_Condition
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Condition::C_Condition()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_init(&m_sMutex, NULL);
  ASSERT(!iRc);         // The function shouldn't fail

  iRc = pthread_cond_init(&m_sCondition, NULL);
  ASSERT(!iRc);

#elif defined _WIN32
  InitializeCriticalSection( &m_sMutex ); 
  
  /* initialise counter */
  m_iWaitingThreads = 0;
  /* Create an auto-reset event. */
  m_sSignal = CreateEvent(NULL,  /* no security */
                          FALSE, /* auto-reset event */
                          FALSE, /* non-signaled initially */
                          NULL); /* unnamed */
  //ASSERT (!m_sSignal);
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Condition::~C_Condition()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_destroy(&m_sMutex);
  ASSERT(!iRc);         // The mutex should not be locked when being destroyed

  iRc = pthread_cond_destroy(&m_sCondition);
  ASSERT(!iRc);         // No thread should wait on the condition
  
#elif defined _WIN32
  DeleteCriticalSection( &m_sMutex );
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Condition::Protect()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_lock(&m_sMutex);
  ASSERT(!iRc);
  USE(iRc);

#elif defined _WIN32
  EnterCriticalSection( &m_sMutex );
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Condition::Release()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_mutex_unlock(&m_sMutex);
  ASSERT(!iRc);
  USE(iRc);

#elif defined _WIN32
  LeaveCriticalSection( &m_sMutex );
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Condition::Signal()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_cond_signal(&m_sCondition);
  ASSERT(!iRc);
  iRc = pthread_mutex_unlock(&m_sMutex);
  ASSERT(!iRc);

#elif defined _WIN32
  int iWaitingThreads = m_iWaitingThreads;
  while (m_iWaitingThreads
         && m_iWaitingThreads == iWaitingThreads)
  {
    PulseEvent (m_sSignal);
    Sleep (0); // deschedule the current thread 
  }
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Condition::Broadcast()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_cond_broadcast(&m_sCondition);
  ASSERT(!iRc);
  iRc = pthread_mutex_unlock(&m_sMutex);
  ASSERT(!iRc);

#elif defined _WIN32
  while (m_iWaitingThreads)
  {
    PulseEvent( m_sSignal );
    Sleep( 0 ); // deschedule the current thread
  }
#endif
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Condition::Wait()
{
#ifdef PTHREAD_COND_T_IN_PTHREAD_H
  int iRc = pthread_cond_wait(&m_sCondition, &m_sMutex);
  ASSERT(!iRc);
  USE(iRc);

#elif defined _WIN32
  m_iWaitingThreads ++;

  // Release the mutex
  LeaveCriticalSection( &m_sMutex );

  WaitForSingleObject( m_sSignal, INFINITE);

  // maybe we should protect this with a mutex ?
  m_iWaitingThreads --;

  // Reacquire the mutex before returning.
  EnterCriticalSection( &m_sMutex );
#endif
}

