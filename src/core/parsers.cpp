/*******************************************************************************
* parsers.cpp: Command line and configuration files parsers
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Cyril Deguet <asmax@via.ecp.fr>
*          Damien Lucas <nitrox@videolan.org>
*          Loïc Minier <lool@videolan.org>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"


#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#ifdef HAVE_OPENDIR
#include <dirent.h>
#endif
#include <sys/types.h>
#include <string.h>

#include "common.h"
#include "debug.h"
#include "reflect.h"
#include "serialization.h"
#include "string.h"
#include "vector.h"
#include "hashtable.h"
#include "stack.h"
#include "buffers.h"
#include "exception.h"
#include "file.h"
#include "log.h"
#include "stream.h"

#include "parsers.h"
#include "lexer.h"

#include "stack.cpp"
#include "stream.cpp"


//******************************************************************************
// class C_ParserException
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
E_Parser::E_Parser(int iCode, const C_String& strMsg) :
                        E_Exception(iCode, strMsg)
{
  // Nothing to do
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
E_Parser::E_Parser(int iCode, const C_String& strMsg, E_Exception e) :
                        E_Exception(iCode, strMsg, e)
{
  // Nothing to do
}





//******************************************************************************
// class C_CfgFileParser
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_CfgFileParser::C_CfgFileParser(C_ParserHandler* pCallBack) : 
    m_cContextStack(10)
{
  ASSERT(pCallBack);
  m_pCallBack = pCallBack;

  // Init the file position indicators
  m_iLineNumber = 0;

}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Vector<C_String> C_CfgFileParser::Parse(const C_String& strFileName,
                                          bool,
                                          const C_String& strRoot)
{
  C_Vector<C_String> vInclude;
  
  ASSERT(m_pCallBack);

  m_pCallBack->SetRoot(strRoot);
#ifdef DEBUG
  if(strRoot!="")
    fprintf(stderr, "Reading file %s for module '%s'\n",
                                  strFileName.GetString(), strRoot.GetString());
  else
    fprintf(stderr, "Reading file %s for base configuration\n", 
                                                       strFileName.GetString());
#endif
  try
  {
    // Open the cfg file
    C_File* pCfgFile = new C_File(strFileName);
    pCfgFile->Open("r");
    yyin = pCfgFile->GetHandle();

    // Parse the file
    int iTokenType = 0;
    while ((iTokenType = yylex()) != 0)
    {
      C_String strFirstToken(yytext);
      if (iTokenType == TOK_END)
      {
        // Unstack the section
        if(m_cContextStack.Size() == 0)
        {
          throw E_Parser(GEN_ERR, strFirstToken + "tag at line " + 
                         m_iLineNumber + "in excess");
        }
        C_String* pstrSection = m_cContextStack.Pop();
        m_pCallBack->OnEndSection(*pstrSection);
        delete pstrSection;
        if (yylex() == TOK_NEWLINE)
        {
          m_iLineNumber++;
        }
        else {
          throw E_Parser(GEN_ERR,
                         C_String("Error in the configuration file at line ") +
                         m_iLineNumber);
        }
      }
      else if (iTokenType == TOK_BEGIN)
      { 
        if (yylex() != TOK_VALUE)
        {
          throw E_Parser(GEN_ERR,
                         C_String("Error in the configuration file at line ") +
                         m_iLineNumber);
        }
        else if(m_cContextStack.Size() == m_cContextStack.Capacity())
        {
          throw E_Parser(GEN_ERR, C_String("Error line ") + m_iLineNumber +
                         ": only " + m_cContextStack.Capacity() +
                         " section levels allowed");
        }
        C_String strValue(yytext);
        // Remove the quotes. FIXME: should be done by the lexer
        C_String strSection(strValue.SubString(1, strValue.Length()-1));
        m_cContextStack.Push(new C_String(strSection));
        m_pCallBack->OnStartSection(strSection);
        if (yylex() == TOK_NEWLINE)
        {
          m_iLineNumber++;
        }
        else {
          throw E_Parser(GEN_ERR,
                         C_String("Error in the configuration file at line ") +
                         m_iLineNumber);
        }
      }
      else if (iTokenType == TOK_VAR)
      {
        if (yylex() != TOK_EQUAL) 
        {
          throw E_Parser(GEN_ERR,
                         C_String("Error in the configuration file at line ") +
                         m_iLineNumber);
        }
        else if (yylex() != TOK_VALUE)
        {
          throw E_Parser(GEN_ERR,
                         C_String("Error in the configuration file at line ") +
                         m_iLineNumber);
        }
        C_String strValue(yytext);
        // Remove the quotes. FIXME: should be done by the lexer
        C_String strStrippedValue(strValue.SubString(1, strValue.Length()-1));
        if(strFirstToken=="configpath")
        {
      C_String* strInc=new C_String(strStrippedValue+"/input.cfg");
      C_String* strIncRoot=new C_String(m_pCallBack->GetSection());
      vInclude.Add(strInc);
      vInclude.Add(strIncRoot);
    }
    else
    {
      m_pCallBack->OnProperty(strFirstToken, strStrippedValue);
    }
    if (yylex() == TOK_NEWLINE)
        {
          m_iLineNumber++;
        }
        else
        {
          throw E_Parser(GEN_ERR,
                         C_String("Error in the configuration file at line ") +
                         m_iLineNumber);
        }
      }
      else if (iTokenType == TOK_NEWLINE)
      {
        m_iLineNumber++;
      }
      else
      {
        throw E_Parser(GEN_ERR,
                       C_String("Error in the configuration file at line ") +
                       m_iLineNumber);
      }
    }
    pCfgFile->Close();
    delete pCfgFile;
  }
  catch(E_Exception e)
  {
    throw E_Parser(GEN_ERR, "Parsing of file '" + strFileName + "' failed", e);
  }

  if(strRoot=="")
  {
    m_pCallBack->OnStartSection("local1");
    m_pCallBack->OnProperty("programcount", m_pCallBack->GetLocalCount());
    m_pCallBack->OnEndSection("local1");
  }
  else
  {
    m_pCallBack->OnProperty("programcount", m_pCallBack->GetLocalCount());
  }
  return vInclude;
}

// Needed for flex functions
extern "C" int yywrap(void)
{
  return 1;
}


void C_CfgFileParser::AddCmdLineSetting(unsigned char sOption, C_String sValue)
{
  C_String sMeth;
  C_String sDest1;
  C_String sDest2;

  switch(sOption)
  {
    case 'd':  /* Destination spec */
      /* Format string should be methode:/dest:port */
     if(sValue=="") throw E_Parser(GEN_ERR, "-d option needs argument"); 
      /* On récupère le port de destination s'ilest précisé */
      if(sValue.Find(':', 1)!=GEN_ERR)
      {
        sMeth=sValue.SubString(0,sValue.Find(':', 1));
        sValue=sValue.SubString(sValue.Find(':', 1)+1,
                                                  strlen(sValue.GetString()));
        if(sValue.Find(':', 1)!=GEN_ERR)
        {
          sDest1=sValue.SubString(0,sValue.Find(':', 1));
          sDest2=sValue.SubString(sValue.Find(':', 1)+1,
                                                  strlen(sValue.GetString()));
        }
        else
        {
          sDest1=sValue;
          sDest2="";
        }
      }
      else
      {
        sMeth="udp";
        sDest1=sValue;
        sDest2="";
      }
      if(sMeth=="udp" || sMeth=="rtp")
      {
        if(sDest1=="") throw E_Parser(GEN_ERR, "No IP destination specified");
        if(sDest2.ToInt()<=0 && sDest2!="")
          throw E_Parser(GEN_ERR, "Invalid port specification");
        m_pCallBack->OnStartSection("channels");
        m_pCallBack->OnProperty("output1", "network");
        m_pCallBack->OnEndSection("channels");
        m_pCallBack->OnStartSection("output1");
        m_pCallBack->OnProperty("dsthost", sDest1);
        if(sDest2!="") m_pCallBack->OnProperty("dstport", sDest2);
        if(sDest1.ToInt()<=239 && sDest1.ToInt()>=224)
          m_pCallBack->OnProperty("streamtype", "multicast");
        else
          m_pCallBack->OnProperty("streamtype", "unicast");
        if(sMeth=="udp") m_pCallBack->OnProperty("protocol", "udp");
        else m_pCallBack->OnProperty("protocol", "rtp");
        m_pCallBack->OnEndSection("output1");
      }
      else if (sMeth=="file")
      {
        if(sDest1=="") throw E_Parser(GEN_ERR, "No Filename specified");
        m_pCallBack->OnStartSection("channels");
          m_pCallBack->OnProperty("output1", "file");
        m_pCallBack->OnEndSection("channels");
        m_pCallBack->OnStartSection("output1");
          m_pCallBack->OnProperty("filename", sDest1);
          m_pCallBack->OnProperty("append", "no");
        m_pCallBack->OnEndSection("output1");
      }
      break;

    case 't':
      if(sValue.ToInt()>0)
      {
        m_pCallBack->OnStartSection("output1");
        m_pCallBack->OnProperty("ttl", sValue.ToInt());
        m_pCallBack->OnEndSection("output1");
      }
      else
      {
        printf("Invalid value for TTL option\n");
        exit(0);
      }
      break;
      
    case 'l':
      /* Store looping property in the program specified */
      m_pCallBack->OnStartSection("program1");
      m_pCallBack->OnProperty("loop", "true");
      m_pCallBack->OnEndSection("program1");
      break;

    case 130:
      /* Log to file */
      m_pCallBack->OnStartSection("global");
      m_pCallBack->OnProperty("logfile", sValue);
      m_pCallBack->OnEndSection("global");
      break;
  }
}

void C_CfgFileParser::AddCmdLineTarget(C_String sTarget, C_String sTags)
{

  C_String sProgramLaunch;   /* Name of the program to be launch at startup */
  /*
   * Target Parsing
   * XXX - it is done in a really non generic way
   */

  /*
   * Gets the Method
   */
  if(sTarget.Find(':', 1)==GEN_ERR)
  {
    throw E_Parser(GEN_ERR, "Syntax error in target definition");
  }

  C_String sMeth=sTarget.SubString(0,sTarget.Find(':', 1));

  try
  {
    if(sMeth=="dvb") sProgramLaunch = ParseDVBTarget(sTarget);
    else if(sMeth=="dvd") sProgramLaunch = ParseDVDTarget(sTarget);
    else if(sMeth=="file") sProgramLaunch = ParseFILETarget(sTarget);
    else if(sMeth=="v4l") sProgramLaunch = ParseV4LTarget(sTarget);
    else if(sMeth=="video") sProgramLaunch = ParseVIDEOTarget(sTarget);
  }
  catch(E_Exception e)
  {
      throw E_Parser(GEN_ERR, e.Dump());
  }

  /* Automatic start section */
  C_String sNewTags="";
  if (sTags.Find("--loop", 0) != GEN_ERR) sNewTags += " -1";
  // TODO nitrox
  // handle sTags (rtp, loop, end, ...)

  m_pCallBack->OnStartSection("launchonstartup");
  m_pCallBack->OnProperty("command1", "broadcast_start broadcast1");
  m_pCallBack->OnProperty("command2", "broadcast_add broadcast1 output1 "
                                        + sProgramLaunch +" input1"+sNewTags);

  m_pCallBack->OnEndSection("launchonstartup");
}


C_String C_CfgFileParser::ParseDVBTarget(C_String sT)
{
    /*
     *      dvb:program[:devicenumber]
     */

    C_String sP;
    unsigned int iN = 0;

    sP = sT.SubString(sT.Find(':', 1)+1, sT.Length());

    if(sP.Find(':', 1)!=GEN_ERR)
    {
       iN=sP.SubString(sP.Find(':', 1)+1, sP.Length()).ToInt();
       sP=sP.SubString(0,sP.Find(':', 1));
    }

    m_pCallBack->OnStartSection("inputs");
    m_pCallBack->OnProperty("input1", "dvb");
    m_pCallBack->OnEndSection("inputs");

    m_pCallBack->OnStartSection("input1");
    m_pCallBack->OnProperty("devicenumber", iN);
    m_pCallBack->OnProperty("sendmethod", 0);
    m_pCallBack->OnEndSection("input1");

    return sP.Replace(' ', '_');
}

C_String C_CfgFileParser::ParseDVDTarget(C_String sT)
{
    /*
     *      dvd:[device][@chapter,title]
     */

    C_String sD="/dev/dvd";
    unsigned int uiC=1;
    unsigned int uiT=1;

    sD = sT.SubString(sT.Find(':', 1)+1, sT.Length());
    if(sD.Find('@', 0)!=GEN_ERR)
    {
        sscanf(sD.SubString(sD.Find('@', 0)+1, sD.Length()).GetString(),
                                                          "%u,%u", &uiC, &uiT);
        sD=sD.SubString(0,sD.Find('@', 0));
    }

    if(sD=="")
    {
        sD = "/dev/dvd"; // TODO - very linux specific
    }

    m_pCallBack->OnStartSection("inputs");
    m_pCallBack->OnProperty("input1", "local");
    m_pCallBack->OnEndSection("inputs");

    m_pCallBack->OnStartSection("input1");
    m_pCallBack->OnProperty("programcount", "1");
    m_pCallBack->OnEndSection("input1");

    m_pCallBack->OnStartSection("input1.1");
    m_pCallBack->OnProperty("name", "program1");
    m_pCallBack->OnProperty("device", sD);
    m_pCallBack->OnProperty("type", "dvd");
    m_pCallBack->OnProperty("dvdtitle", uiT);
    m_pCallBack->OnProperty("dvdchapter", uiC);
    m_pCallBack->OnEndSection("input1.1");

    return "program1";
}

C_String C_CfgFileParser::ParseFILETarget(C_String sT)
{
    /*
     *      file:file[:format]
     */

    C_String sFile;
    C_String sFmt;

    sFile = sT.SubString(sT.Find(':', 1)+1, sT.Length());

    if(sFile.Find(':', 1)!=GEN_ERR)
    {
        sFmt = sFile.SubString(sFile.Find(':', 1)+1, sFile.Length());
        sFile = sFile.SubString(0,sFile.Find(':', 1));
    }
    else
    {
        /* Format is not specified - let's try to guess it */
        C_File fFile = C_File(sFile);
        fFile.Open("r");
        byte bFormat[4];
        while (true) {
            fFile.Read(bFormat,(s64) 4);
            // now trying to apply ISO-13818... -- lool
            // TS?
            if( bFormat[0] == 0x47 ) {
#ifdef DEBUG
                        fprintf( stderr, "DEBUG: detected MPEG2-TS\n" );
#endif
                sFmt = "mpeg2-ts";
                break;
            } else if( (bFormat[0] == 0x00) && (bFormat[1] == 0x00)
                       && (bFormat[2] == 0x01) ) {
                // beginning of a PS system or pack header?
                if (bFormat[3] == 0xbb) {       // system header
                    fFile.Read(bFormat, (s64)2);
                    int iheaderSize = (bFormat[0] << 8) + bFormat[1];
                    fFile.Seek((s64)(iheaderSize), FILE_SEEK_CURRENT);
                    continue;
                } else if (bFormat[3] == 0xba) {        // pack header
                    fFile.Read(bFormat,(s64) 1);
                    if (((bFormat[0] >> 6) & 0x03) == 0x01) {
#ifdef DEBUG
                        fprintf( stderr, "DEBUG: detected MPEG2-PS\n" );
#endif
                        sFmt = "mpeg2-ps";
                        break;
                    } else if ((bFormat[0] & 0xf0) == 0x20) {
#ifdef DEBUG
                        fprintf( stderr, "DEBUG: detected MPEG1-PS\n" );
#endif
                        sFmt = "mpeg1-ps";
                        break;
                    } else {
                        fFile.Close();
                        throw E_Parser( GEN_ERR, "Automatic format detection "
                                        "of MPEG failed (pack header found)" );
                    }
                } else {        // PES?, can not guess right now
#ifdef DEBUG
                        fprintf( stderr, "DEBUG: detected PES?\n" );
#endif
                    fFile.Close();
                    throw E_Parser( GEN_ERR, "Automatic format detection of "
                                            "MPEG failed (PES found)" );
                }
            } else {
                fFile.Close();
                throw E_Parser( GEN_ERR, "Automatic format detection failed" );
            }
        }
        fFile.Close();
    }
    m_pCallBack->OnStartSection("inputs");
    m_pCallBack->OnProperty("input1", "local");
    m_pCallBack->OnEndSection("inputs");

    m_pCallBack->OnStartSection("input1");
    m_pCallBack->OnProperty("programcount", "1");
    m_pCallBack->OnEndSection("input1");

    m_pCallBack->OnStartSection("input1.1");
    m_pCallBack->OnProperty("name", "program1");
    m_pCallBack->OnProperty("type", sFmt.ToLower());
    m_pCallBack->OnProperty("filename", sFile);
    m_pCallBack->OnEndSection("input1.1");

    return "program1";
}


C_String C_CfgFileParser::ParseVIDEOTarget(C_String sT)
{
    /*
     *  video:[device:format]
     */

    C_String sD="";
    C_String sF="";

    sD = sT.SubString(sT.Find(':', 1)+1, sT.Length());
    if(sD.Find(':', 1)!=GEN_ERR)
    {
        sF = sD.SubString(sD.Find(':', 1)+1, sD.Length());
        sD = sD.SubString(0,sD.Find(':', 1));
    }

    m_pCallBack->OnStartSection("inputs");
    m_pCallBack->OnProperty("input1", "video");
    m_pCallBack->OnEndSection("inputs");

    m_pCallBack->OnStartSection("input1");
    if ( sD != "" ) m_pCallBack->OnProperty("device", sD);
    if ( sF != "" ) m_pCallBack->OnProperty("type", sF);
    m_pCallBack->OnEndSection("input1");

    return "video";
}

C_String C_CfgFileParser::ParseV4LTarget(C_String sT)
{
    /*
     *   v4l:[videodev[@codec,bitrate]][:audiodev[@codec,bitrate]]
     *       [{extraparam1=value1,extraparam2=value2}]
     */

    /*
     * Grmbl, that command line is quite a nightmare !
     * Tooney, if you ever write another input with that many
     * options, I won't write any command line for it !
     * -- nitrox
     */

    C_String sVideo="";
    C_String sAudio="";
    C_String sExtra="";

    /* Let's get the 3 main parts */
    sVideo = sT.SubString(sT.Find(':', 1)+1, sT.Length());
    if ( sVideo.Find(':', 1) != GEN_ERR)
    {
        sAudio = sVideo.SubString(sVideo.Find(':', 1)+1, sVideo.Length());
        sVideo = sVideo.SubString(0,sVideo.Find(':', 1));
        if ( sAudio.Find('{', 1) != GEN_ERR )
        {
            sExtra = sAudio.SubString(sAudio.Find('{', 1)+1, sAudio.Length()-1);
            sAudio = sAudio.SubString(0, sAudio.Find('{', 1));
        }
    }
    else if ( sVideo.Find('{', 1) != GEN_ERR )
    {
        sExtra = sVideo.SubString(sVideo.Find('{', 1)+1, sVideo.Length()-1);
        sVideo = sVideo.SubString(0,sVideo.Find(':', 1));
    }

    /* Let's analyze video part */
    if(sVideo != "")
    {
        //TODO !!!
    }

    throw E_Parser(GEN_ERR, "V4L command line not implemented !");
    return "";
}
