/*******************************************************************************
* string.h: String class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: string.h,v 1.4 2003/08/28 18:03:26 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef __STRING_H_
#define __STRING_H_


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_String
{
 public:
  // Constructors
  C_String();
  C_String(const C_String& strSrc);
  C_String(const char* pszSrc);
  C_String(char cSrc);
  C_String(s32 iSrc);
  C_String(u32 iSrc);

   // Destructor
  ~C_String();

  // Copy operators
  C_String& operator = (const char *pszSrc);
  C_String& operator = (const C_String& strSrc);
  C_String& operator = (const int iSrc);
  
  C_String* Clone();

  // String access (read only)
  unsigned int Length() const;
  const char& operator [] (unsigned int iIndex) const;
  const char* GetString() const;
  
  // String comparisons
  bool operator == (const char* pszArg) const;
  bool operator == (const C_String& strArg) const;
  bool operator != (const char* pszArg) const;
  bool operator != (const C_String& strArg) const;
  bool operator < (const C_String& strArg) const;
  bool operator > (const C_String& strArg) const;

  // String concatenations
  C_String operator + (const C_String& strToken) const;
  C_String operator + (const char *pszToken) const;
  C_String operator + (char cChar) const;
  C_String operator + (s32 iNumber) const;
  C_String operator + (u32 iNumber) const;

  C_String& operator += (const C_String& strToken);
  C_String& operator += (const char *pszToken);
  C_String& operator += (char cChar);
  C_String& operator += (s32 iNumber);
  C_String& operator += (u32 iNumber);

  // Creation of new strings based on the current one
  C_String ToLower() const;
  C_String ToUpper() const;
  C_String SubString(unsigned int iBeginning, unsigned int iEnd) const;
  C_String Strip(const C_String& strInvalidChars) const;

  // String conversion
  int ToInt() const;
  
  // Substring lookup
  int Find(char cChar, unsigned int iStart = 0) const;
  int Replace(char cChar, char cChar2, unsigned int iStart = 0);
  int Find(const C_String& strItem, unsigned int iStart = 0) const;
  bool StartsWith(const C_String& strItem) const;
  bool EndsWith(const C_String& strItem) const;

  // For hashtables
  u32 GetHashCode() const;
  
 protected:
  // Internal constructor, used for copy optimisations
  C_String(unsigned int iSize, bool bDummy);
  
 private:
  // String length
  unsigned int m_iLength;
  // Null terminated traditional string
  char* m_pszBuff;
};



//------------------------------------------------------------------------------
// Additional operators to deal with traditonal strings (char*)
//------------------------------------------------------------------------------
// Solve various problems such as this one: "my traditional string" + C_String
// To be extended 
//------------------------------------------------------------------------------
C_String operator + (const char* pszLeft, const C_String& strRight);
C_String operator + (const int iLeft, const C_String& strRight);



//------------------------------------------------------------------------------
// class C_StringTokenizer
//------------------------------------------------------------------------------
class C_StringTokenizer
{
 public:
  C_StringTokenizer(const C_String& strSource, char cSeparator = ' ');

  void SetSeparator(char cSeparator);

  bool HasMoreToken();
  C_String NextToken();
  void Reset();

  unsigned int CountTokens() const;

 private:
  C_String m_strSource;
  unsigned int m_iPosition;
  char m_cSeparator;
};



#else
#error "Multiple inclusions of string.h"
#endif

