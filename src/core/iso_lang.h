/*******************************************************************************
* iso-lang.h: ISO Languages tools
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: iso_lang.h,v 1.1 2002/05/14 22:10:08 bozo Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef __ISO_LANG_H_
#define __ISO_LANG_H_

typedef struct iso639_lang_s
{
    char * psz_eng_name;        /* Description in English */
    char * psz_native_name;     /* Description in native language */
    char * psz_iso639_1;        /* ISO-639-1 (2 characters) code */
    char * psz_iso639_2T;       /* ISO-639-2/T (3 characters) English code */
    char * psz_iso639_2B;       /* ISO-639-2/B (3 characters) native code */
} iso639_lang_t;

iso639_lang_t * GetLang_1(const char * psz_iso639_1);

iso639_lang_t * GetLang_2T(const char * psz_iso639_2T);

iso639_lang_t * GetLang_2B(const char * psz_iso639_2B);

#else
#error "Multiple inclusions of iso-lang.h"
#endif

