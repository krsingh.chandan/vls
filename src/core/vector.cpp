/*******************************************************************************
* vector.cpp: Vectors manipulation
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Notice: This file must be included in the source file with its header
* TO DO:
* Warning: This class is not thread safe
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at vector.h for further explanations




//******************************************************************************
// class C_Vector
//******************************************************************************
// The simplest of the container classes, and in many cases the most efficient.
// It is often used where a variable size array is needed.
// Supports random access to elements, insertion and removal of elements
// anywhere in the container. The number of elements in a vector may vary
// dynamically; memory management is automatic.
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Vector<T>::C_Vector(unsigned int uiGrowthFactor,
                                         byte bAutoClean)
{
  ASSERT(uiGrowthFactor > 0);

  m_uiSize = 0;
  m_uiCapacity = uiGrowthFactor;
  m_uiGrowthFactor = uiGrowthFactor;
  m_apElems = new T* [uiGrowthFactor];  
  ASSERT(m_apElems);

  m_bAutoClean = bAutoClean;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Vector<T>::~C_Vector()
{
  ASSERT(m_apElems);

  // Remove all the elements
  Empty();
    
  // Delete the container itself
  delete[] m_apElems;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Vector<T>::C_Vector(const C_Vector<T>& cSrc)
{
  // Copy the status of the source vector
  m_uiSize = cSrc.m_uiSize;
  m_uiCapacity = cSrc.m_uiCapacity;
  m_uiGrowthFactor = cSrc.m_uiGrowthFactor;
  m_bAutoClean = cSrc.m_bAutoClean;

  // Allow a new buffer
  m_apElems = new T* [m_uiCapacity];
  ASSERT(m_apElems);

  // Now copy the elements themselves: we cannot call the copy constructor for
  // it wouldn't work with virtual classes, so we use the Clone() method
  for(unsigned int i = 0; i < m_uiSize; i++)
  {
    T* pNewElem = cSrc.m_apElems[i]->Clone();
    m_apElems[i] = pNewElem;
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T>
C_Vector<T>& C_Vector<T>::operator = (const C_Vector<T>& cSrc)
{
  // Destroy previous data
  Empty();
  delete[] m_apElems;

  // Copy the status of the source vector
  m_uiSize = cSrc.m_uiSize;
  m_uiCapacity = cSrc.m_uiCapacity;
  m_uiGrowthFactor = cSrc.m_uiGrowthFactor;
  m_bAutoClean = cSrc.m_bAutoClean;

  // Allow a new buffer
  m_apElems = new T* [m_uiCapacity];
  ASSERT(m_apElems);

  // Now copy the elements themselves: we cannot call the copy constructor for
  // it wouldn't work with virtual classes, so we use the Clone() method
  for(unsigned int i = 0; i < m_uiSize; i++)
  {
    T* pNewElem = cSrc.m_apElems[i]->Clone();
    m_apElems[i] = pNewElem;
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_Vector<T>::Add(T* pElem)
{
  ASSERT(pElem);
  if(m_uiSize >= m_uiCapacity)
    Reserve(m_uiCapacity + m_uiGrowthFactor);
  
  m_apElems[m_uiSize] = pElem;
  m_uiSize++;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> T* C_Vector<T>::Remove(unsigned int uiIndex)
{
  ASSERT(uiIndex < m_uiSize);

  m_uiSize--;
  T* pItem = m_apElems[uiIndex];
  ASSERT(pItem);

  for(unsigned int i = uiIndex; i < m_uiSize; i++)
  {
    m_apElems[i] = m_apElems[i+1];
  }

  return pItem;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_Vector<T>::Delete(unsigned int uiIndex)
{
  ASSERT(uiIndex < m_uiSize);

  m_uiSize--;
  delete m_apElems[uiIndex];

  for(unsigned int i = uiIndex; i < m_uiSize; i++)
  {
    m_apElems[i] = m_apElems[i+1];
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> T& C_Vector<T>::operator [] (unsigned int uiIndex) const
{
  ASSERT(uiIndex < m_uiSize);
  T* pElem = m_apElems[uiIndex];
  
  return *pElem;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Returns the index of the T object is the list or GEN_ERR if the object is not
// in the list
// Use the == operator of the T class as the predicate of equality
//------------------------------------------------------------------------------
template <class T> int C_Vector<T>::Find(const T& cElem) const
{
  for(unsigned int iIndex = 0; iIndex < m_uiSize; iIndex++)
  {
    if(*m_apElems[iIndex] == cElem)
      return iIndex;
  };
  
  // Node was not found
  return GEN_ERR;

}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_Vector<T>::Reserve(unsigned int uiCapacity)
{
  if(uiCapacity > m_uiCapacity)
  {
    // Save the elems of the list
    T** apOldElemArray = m_apElems;

    // Create a new empty list
    m_apElems = new T* [uiCapacity];
    ASSERT(m_apElems);
    m_uiCapacity = uiCapacity;

    // Transfer the elems in the new list
    for(unsigned int i = 0; i < m_uiSize; i++)
    {
      m_apElems[i] = apOldElemArray[i];
    }

    // Delete the old list
    delete[] apOldElemArray;
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> u32 C_Vector<T>::Capacity() const
{
  return m_uiCapacity;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Removal of elements is done according to the bAutoClean flag
//------------------------------------------------------------------------------
template <class T> void C_Vector<T>::Empty()
{
  ASSERT(m_apElems);

  switch(m_bAutoClean)
  {
    case YES:
    {
      // Delete all the stored items
      for(unsigned int i = 0; i < m_uiSize; i++)
        delete m_apElems[i];
      break;
    }
    case NO:
    {
      // Nothing to do
      break;
    }
    case SMART:
    {
      // Delete the items as for YES, but also detect if an item is
      // stored twice to avoid to delete it more than once
      unsigned int j = 0;
      while(j < m_uiSize)
      {
        // Skip already handled items
        if(m_apElems[j] == NULL)
          continue;

        // Find all the copies of the current item
        for(unsigned int k = j+1; k < m_uiSize; k++)
        {
          if(m_apElems[k] == m_apElems[j])
            m_apElems[k] = NULL;
        }

        // Delete the first occurence of the current item
        delete m_apElems[j];
      }
      break;
    }
    default:
    {
      ASSERT(false);
      break;
    }
  }

  m_uiSize = 0;
}

template <class T> void C_Vector<T>::Sort()
{
  return QuickSort(0, Size()-1);
}

/* We assume that T has a method T <= T2 */
template <class T> void C_Vector<T>::QuickSort(int iLeft, int iRight)
{
  int iPrevious, iCurrent;

  if (iLeft >= iRight)
    return; // We reached the middle... it is sorted !

  iPrevious = iLeft;

  for (iCurrent = iLeft + 1; iCurrent <= iRight; iCurrent++)
    if ( *m_apElems[iCurrent] < *m_apElems[iLeft] )
      Swap(++iPrevious, iCurrent);

  Swap(iLeft, iPrevious);

  QuickSort(iLeft, iPrevious-1);
  QuickSort(iPrevious+1, iRight);    

}

template <class T> void C_Vector<T>::Swap(unsigned int i, unsigned int j)
{
  if ((i < Size()) && (j < Size()))
  {
    T* Ttmp = m_apElems[i];
    m_apElems[i] = m_apElems[j];
    m_apElems[j] = Ttmp;
  }
}


//******************************************************************************
// class C_VectorBase
//******************************************************************************
// The simplest of the container classes, and in many cases the most efficient.
// It is often used where a variable size array is needed.
// Supports random access to elements, insertion and removal of elements
// anywhere in the container. The number of elements in a VectorBase may vary
// dynamically; memory management is automatic.
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> C_VectorBase<T>::C_VectorBase(unsigned int uiGrowthFactor)
{
  ASSERT(uiGrowthFactor > 0);

  m_uiSize = 0;
  m_uiCapacity = uiGrowthFactor;
  m_uiGrowthFactor = uiGrowthFactor;
  m_apElems = new T[uiGrowthFactor];
  ASSERT(m_apElems);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> C_VectorBase<T>::~C_VectorBase()
{
  ASSERT(m_apElems);

  // Delete the container itself
  delete[] m_apElems;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> C_VectorBase<T>::C_VectorBase(const C_VectorBase<T>& cSrc)
{
  *this = cSrc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T>
C_VectorBase<T>& C_VectorBase<T>::operator = (const C_VectorBase<T>& cSrc)
{
  if (&cSrc == this)
    return *this;

  // Destroy previous data
  delete[] m_apElems;

  // Copy the status of the source VectorBase
  m_uiSize = cSrc.m_uiSize;
  m_uiCapacity = cSrc.m_uiCapacity;
  m_uiGrowthFactor = cSrc.m_uiGrowthFactor;
  this->m_bAutoClean = cSrc.m_bAutoClean;

  // Allocate a new buffer
  m_apElems = new T[m_uiCapacity];
  ASSERT(m_apElems);

  // Now copy the elements themselves: we cannot call the copy constructor for
  // it wouldn't work with virtual classes, so we use the Clone() method
  for(unsigned int i = 0; i < m_uiSize; i++)
  {
    m_apElems[i] = new T(cSrc.m_apElems[i]);
  }

  return *this;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> void C_VectorBase<T>::Add(const T& cElem)
{
  if(m_uiSize >= m_uiCapacity)
    Reserve(m_uiCapacity + m_uiGrowthFactor);

  m_apElems[m_uiSize] = cElem;
  m_uiSize++;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> T C_VectorBase<T>::Remove(unsigned int uiIndex)
{
  ASSERT(uiIndex < m_uiSize);

  m_uiSize--;
  T cItem = m_apElems[uiIndex];

  //TODO very expensive!!! why not copying last object into hole?
  for(unsigned int i = uiIndex; i < m_uiSize; i++)
  {
    m_apElems[i] = m_apElems[i+1];
  }

  return cItem;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> T& C_VectorBase<T>::operator [] (unsigned int uiIndex) const
{
  ASSERT(uiIndex < m_uiSize);
  return m_apElems[uiIndex];
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Returns the index of the T object is the list or GEN_ERR if the object is not
// in the list
// Use the == operator of the T class as the predicate of equality
//------------------------------------------------------------------------------
template <class T> int C_VectorBase<T>::Find(const T& cElem) const
{
  for(unsigned int iIndex = 0; iIndex < m_uiSize; iIndex++)
  {
    if(m_apElems[iIndex] == cElem)
      return iIndex;
  };

  // Node was not found
  return GEN_ERR;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> void C_VectorBase<T>::Reserve(unsigned int uiCapacity)
{
  if(uiCapacity > m_uiCapacity)
  {
    // Save the elems of the list
    T* apOldElemArray = m_apElems;

    // Create a new empty list
    m_apElems = new T[uiCapacity];
    ASSERT(m_apElems);
    m_uiCapacity = uiCapacity;

    // Transfer the elems in the new list
    for(unsigned int i = 0; i < m_uiSize; i++)
    {
      m_apElems[i] = apOldElemArray[i];
    }

    // Delete the old list
    delete[] apOldElemArray;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> u32 C_VectorBase<T>::Size() const
{
  return m_uiSize;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class T> u32 C_VectorBase<T>::Capacity() const
{
  return m_uiCapacity;
}


