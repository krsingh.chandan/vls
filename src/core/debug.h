/*******************************************************************************
* debug.h: debugging macros
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: debug.h,v 1.1 2001/10/06 21:23:36 bozo Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Be sure to include this file after all other system includes because of the
* various redefinitions that must overwrite standard ones
*
*******************************************************************************/


#ifndef _DEBUG_H_
#define _DEBUG_H_


//------------------------------------------------------------------------------
// ASSERT
//------------------------------------------------------------------------------
// This macro is used to test that a variable is not nul. It insert the needed
// code when the program is compiled with the debug option, but does nothing
// in release program.
//------------------------------------------------------------------------------
#ifdef DEBUG
#define ASSERT(Var)                                                            \
if (!(Var))                                                                    \
    printf("ASSERT ERROR in %s line %d (code checked: %s)\n",                  \
           __FILE__, __LINE__, #Var);

#else
#define ASSERT(Var)

#endif
    

//------------------------------------------------------------------------------
// USE
//------------------------------------------------------------------------------
// This macro is used to let the compiler think that the variable is used. This
// is mainly usefull in release mode for the ASSERT are removed from the code
//------------------------------------------------------------------------------
#ifndef DEBUG
#define USE(Var)                                                               \
Var = Var

#else
#define USE(Var)

#endif


//------------------------------------------------------------------------------
// ZERO
//------------------------------------------------------------------------------
// This macro is used to initialise a pointer or a variable to 0. It is mainly
// useful for pointers and when used with the ASSERT macro
//------------------------------------------------------------------------------
#ifdef DEBUG
#define ZERO(Var)                                                              \
memset(&(Var), 0, sizeof((Var)));

#else
#define ZERO(Var)

#endif


//------------------------------------------------------------------------------
// P_ZERO
//------------------------------------------------------------------------------
// This macro is used to initiase the memory pointed out by a pointer to 0.
// It is very useful when used with the ASSERT macro. It also only insert the
// needed code when the program is compiled with the debug option.
//------------------------------------------------------------------------------
#ifdef DEBUG
#define P_ZERO(p_Mem)                                                          \
if((p_Mem) == NULL)                                                            \
    printf("PZERO ERROR in %s line %d: %s is null)\n",                         \
           __FILE__, __LINE__, #p_Mem);                                        \
else                                                                           \
    memset((p_Mem), 0, sizeof(*(p_Mem)));

#else
#define P_ZERO(p_Mem)

#endif


//------------------------------------------------------------------------------
// A_ZERO
//------------------------------------------------------------------------------
// This macro is used to initiase an array of variables to 0.
// It has the same purpose than R_ZERO or P_ZERO, but for array
//------------------------------------------------------------------------------
#ifdef DEBUG
#define A_ZERO(p_Array, i_Size)                                                \
if((p_Array) == NULL)                                                          \
    printf("AZERO ERROR in %s line %d: %s is null)\n",                         \
           __FILE__, __LINE__, #p_Array);                                      \
else                                                                           \
    memset((p_Array), 0, (i_Size)*sizeof(*(p_Array)));

#else
#define A_ZERO(p_Array, i_Size)

#endif


#else
#error "Multiple inclusions of debug.h"
#endif

