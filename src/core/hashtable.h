/*******************************************************************************
* hashtable.h: Hashtable class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
* This implementation uses the vector template
*
*******************************************************************************/


#ifndef _HASHTABLE_H_
#define _HASHTABLE_H_


//------------------------------------------------------------------------------
// Declaration forward
//------------------------------------------------------------------------------
template <class Key, class Value> class C_HashTable;
template <class Key, class Value> class C_HashTableIterator;




//******************************************************************************
// class C_HashMethod
//******************************************************************************
// This class has a specialisation for u16 and u32 to be able to use integers
// as key. Negative integers are not supported yet, they shouldn't be usefull
// There is also a specialisation for handles
//******************************************************************************
template <class T> class C_HashMethod
{
 public:
  inline C_HashMethod(u32 uiMaxHash);

  inline u32 Hash(const T& cKey) const;
  inline u32 GetMaxHash() const;

 private:
  u32 m_uiMaxHash;
};


template <> class C_HashMethod<u32>
{
 public:
  inline C_HashMethod(u32 uiMaxHash);

  inline u32 Hash(u32 iKey) const;
  inline u32 GetMaxHash() const;

 private:
  u32 m_uiMaxHash;
};


template <> class C_HashMethod<u16>
{
 public:
  inline C_HashMethod(u32 uiMaxHash);

  inline u32 Hash(u16 iKey) const;
  inline u32 GetMaxHash() const;

 private:
  u32 m_uiMaxHash;
};


template <> class C_HashMethod<handle>
{
 public:
  inline C_HashMethod(u32 uiMaxHash);

  inline u32 Hash(handle hKey) const;
  inline u32 GetMaxHash() const;

 private:
  u32 m_uiMaxHash;
};




//******************************************************************************
// class C_Predicate
//******************************************************************************
// 
//******************************************************************************
template <class T> class C_Predicate
{
 public:
   C_Predicate();
  inline bool Compare(const T& cArg1, const T& cArg2) const;
};




//******************************************************************************
// class C_HashTableNode
//******************************************************************************
//
//******************************************************************************
template <class Key, class Value> class C_HashTableNode
{
  friend class C_HashTable<Key, Value>;
  
 public:
  C_HashTableNode(const Key& cKey, Value* pValue);
  C_HashTableNode(const C_HashTableNode<Key, Value>& cNode);
  ~C_HashTableNode();

  inline const Key& GetKey() const;
  inline Value* GetValue() const;

 protected:
  inline void Empty();
  
 private:
  const Key m_cKey;
  Value* m_pValue;
};




//******************************************************************************
// class C_HashTable
//******************************************************************************
//
//******************************************************************************
template <class Key, class Value> class C_HashTable
{
 friend class C_HashTableIterator<Key, Value>;

 public:
  C_HashTable(u32 uiBuckets = 17);
  //C_HashTable(C_HashMethod<Key>* pHashMethod);
  //C_HashTable(C_Predicate<Key>* pPredicate);
  //C_HashTable(C_HashMethod<Key>* pHashMethod, C_Predicate<Key>* pPredicate);
  C_HashTable(const C_HashTable<Key, Value>& cHashTable);
  ~C_HashTable();

  C_HashTable<Key, Value>& operator =
                                 (const C_HashTable<Key, Value>& cHashTable);

  void Add(const Key& cKey, Value* pValue);
  Value* Remove(const Key& cKey);
  void Delete(const Key& cKey);
  void Update(const Key& cKey, Value* pNewValue);
  Value* Modify(const Key& cKey, Value* pNewValue);

  Value* Get(const Key& cKey) const;
  //Value& operator[] (const Key& cKey);
  
  unsigned int Size() const;
  C_HashTableIterator<Key, Value> CreateIterator() const;

 private:
  C_HashMethod<Key> m_cHashMethod;
  C_Predicate<Key> m_cPredicate;

  u32 m_uiArraySize;
  C_Vector< C_HashTableNode<Key, Value> >* m_avData;
};



//******************************************************************************
// class C_HashTableIterator
//******************************************************************************
//
//******************************************************************************
template <class Key, class Value> class C_HashTableIterator
{
 friend class C_HashTable<Key, Value>;

 public:
  inline bool HasNext();
  inline C_HashTableNode<Key, Value>* GetNext();
  inline void Reset();

 private:
  // The iterator can only be constructed by the HashTable
  C_HashTableIterator(const C_HashTable<Key, Value>& cHashTable);
  //C_HashTableIterator(const C_HashTableIterator<Key, Value>& cIterator);

  // HashTable to browse
  const C_HashTable<Key, Value>& m_cHashTable;

  // Position in the data of the hashtable
  unsigned int m_iCurrentVector;
  unsigned int m_iCurrentVectorItem;
};



#else
#error "Multiple inclusions of hashtable.h"
#endif

