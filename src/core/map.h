/*******************************************************************************
* map.h: map container class definition
*-------------------------------------------------------------------------------
* (c)2003 VideoLAN
* $Id: map.h,v 1.2 2003/06/09 13:38:48 jpsaman Exp $
*
* Authors: Jean-Paul Saman <jpsaman@wxs.nl>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Note: since the compiler also need the generic implementation code of the
* template to build the type specific byte code, the .cpp file must have to be
* also included in the source file
*
*******************************************************************************/

#ifndef _MAP_H_
#define _MAP_H_

//------------------------------------------------------------------------------
// Declaration forward
//------------------------------------------------------------------------------
template <class Key,class Value> class MapIterator;

//******************************************************************************
// class C_Map
//******************************************************************************
// A simple Map class
//******************************************************************************
template <class Key, class Value> class C_Map
{
  friend class MapIterator<Key,Value>;

  public:
    C_Map() { }

    void Add(const Key& cKey, const Value& cValue);
    void Delete(const Key& cKey);
    void Update(const Key& cKey, Value& cNewValue);
    bool Find(const Key& cKey) const;
    Value Get(const Key& cKey) const;

  private:
    C_VectorBase<Key>   m_vKeys;
    C_VectorBase<Value> m_vValues;
};


template <class Key,class Value> class C_MapIterator
{
  public:
    C_MapIterator(const C_Map<Key,Value>& cMap);

    inline void Init();
    inline bool HasNext();
    inline void GetNext();
    inline Key Current();

  private:
    const C_Map<Key,Value>& m_mMap;
    int                   m_iIndex;
};

#else
#error "Multiple inclusions of map.h"
#endif
