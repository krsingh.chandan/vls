/*******************************************************************************
* settings.cpp: Application settings management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <stdio.h>
#ifdef HAVE_OPENDIR
#include <dirent.h>
#endif
#include <sys/types.h>

#include "common.h"
#include "debug.h"
#include "reflect.h"
#include "serialization.h"
#include "string.h"
#include "stack.h"
#include "vector.h"
#include "hashtable.h"
#include "buffers.h"
#include "exception.h"
#include "file.h"
#include "stream.h"
#include "parsers.h"
#include "settings.h"

#include "vector.cpp"
#include "hashtable.cpp"
#include "stack.cpp"



//******************************************************************************
// class C_SettingsHandler
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Class definition
//------------------------------------------------------------------------------
// Not in the header for this class is private to the File module
//------------------------------------------------------------------------------
class C_SettingsHandler : public C_ParserHandler
{
public:
  C_SettingsHandler(C_Settings* pSettings);
  virtual ~C_SettingsHandler();
  
  virtual void OnStartSection(const C_String& strName);
  virtual void OnEndSection(const C_String& strName);
  virtual void OnProperty(const C_String& strName, const C_String& strVal);

  C_String GetSection(); 
  int GetLocalCount(); //KLUDGE
  void ResetLocalCount(); //KLUDGE
  void SetRoot(const C_String& strR);

private:
  int m_iLocalCount; //KLUDGE
  C_Settings* m_pSettings;
  C_String m_strPrefix;
  C_String m_strRoot;
};


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_SettingsHandler::C_SettingsHandler(C_Settings* pSettings)
{
  ASSERT(pSettings);
  m_pSettings = pSettings;
  m_iLocalCount = 0;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_SettingsHandler::~C_SettingsHandler()
{
  // Just here to avoid a warning at compile time
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_SettingsHandler::OnStartSection(const C_String& strName)
{
  if(strName.ToInt()!=0 && (m_strPrefix.Find('.', 0)==GEN_ERR))
  {
    // Guess we have a file program badly declared
#ifdef DEBUG
    printf("Implicit declaration of input 'local1'\n");
#endif
    OnStartSection("inputs");
    OnProperty("local1", "local");
    OnEndSection("inputs");
    m_strPrefix += "local1.";
    m_iLocalCount++;
    const C_String strN = m_iLocalCount;
    m_strPrefix += strN + ".";
  }
  else if(strName.ToInt()!=0)
  {
      if(m_iLocalCount < strName.ToInt())
        m_iLocalCount = strName.ToInt();
      m_strPrefix += strName + ".";
  }
  else
  {
    m_strPrefix += strName + ".";
  }
}

C_String C_SettingsHandler::GetSection(void)
{
  return m_strPrefix;
}

void C_SettingsHandler::SetRoot(const C_String& strR)
{
  m_strRoot=strR;
  m_strPrefix=m_strRoot;
  return;
}

int C_SettingsHandler::GetLocalCount(void)
{
  return m_iLocalCount;
}

void C_SettingsHandler::ResetLocalCount(void)
{
  m_iLocalCount=0;
  return;
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_SettingsHandler::OnEndSection(const C_String& strName)
{
  if(strName.ToInt()!=0)
  {
    const C_String strN = m_iLocalCount;
    ASSERT(m_strPrefix.Length() - strN.Length() >= 0);
    unsigned int iEnd = m_strPrefix.Length() - strN.Length() - 1;
    m_strPrefix = m_strPrefix.SubString(0, iEnd);
  }
  else
  {
    ASSERT(m_strPrefix.Length() - strName.Length() >= 0);
    unsigned int iEnd = m_strPrefix.Length() - strName.Length() - 1;
    m_strPrefix = m_strPrefix.SubString(0, iEnd);
  }
  if(m_strPrefix.Find('.')!=GEN_ERR)
  {
    m_strPrefix=m_strRoot;
  }
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_SettingsHandler::OnProperty(const C_String& strName,
                                   const C_String& strValue)
{
  C_String strKey = m_strPrefix + strName;
  m_pSettings->Update(strKey, strValue);
}



//******************************************************************************
// class C_Settings
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Settings::C_Settings() : m_cSettings(2311)
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Settings::Load(const C_String& strDefaultCfgFile, bool)
{
  C_Vector<C_String> vInclude;
  C_SettingsHandler cHandler(this);
  C_CfgFileParser cParser(&cHandler);
  vInclude=cParser.Parse(strDefaultCfgFile, true, "");
  for(unsigned int i=0; i<vInclude.Size(); i+=2)
  {
    // Only one inclusion level is allowed
    // fileName is in vInclude[i] and root is in vInclude[i+1]
    cHandler.ResetLocalCount();
    cParser.Parse(vInclude[i], true, vInclude[i+1]);
  }
}

void C_Settings::AddCmdLineTarget(C_String sTarget, C_String sTags)
{
  C_SettingsHandler cHandler(this);
  C_CfgFileParser cParser(&cHandler);
  cParser.AddCmdLineTarget(sTarget, sTags);
}

void C_Settings::AddCmdLineSetting(char sOption, C_String sValue)
{
  C_SettingsHandler cHandler(this);
  C_CfgFileParser cParser(&cHandler);
  cParser.AddCmdLineSetting(sOption, sValue);
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Settings::Update(const C_String& strName, const C_String& strValue)
{
  m_cSettings.Update(strName, new C_String(strValue));
}

void C_Settings::Delete(const C_String& strName)
{
  m_cSettings.Delete(strName);
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Settings::GetSetting(const C_String& strName,
                                const C_String& strDfltValue) const
{
  C_String* pstrValue = m_cSettings.Get(strName);
  //C_String* pstrValue = m_cSettings.Get(strName.ToLower());
  if(pstrValue)
  {
    return *pstrValue;
  }
  else
  {
    return strDfltValue;
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Vector<C_Setting> C_Settings::GetSettings(const C_String& strSection) const
{
  C_HashTableIterator<C_String, C_String> cIterator =
                                                   m_cSettings.CreateIterator();
  C_Vector<C_Setting> vValues;

  while(cIterator.HasNext())
  {
    C_HashTableNode<C_String, C_String>* pNode = cIterator.GetNext();
    C_String strName = pNode->GetKey();
    C_String* pstrValue = pNode->GetValue();

    if(strName.StartsWith(strSection + "."))
    {
      // Strip the initial section name
      unsigned int iOffset = strSection.Length() + 1;
      unsigned int iLength = strName.Length();
      C_String strSetting = strName.SubString(iOffset, iLength);

      // Add the setting to the result
      C_Setting* pSetting = new C_Setting(strSetting, *pstrValue);
      vValues.Add(pSetting);
    }
  }

  return vValues;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Vector<C_Setting> C_Settings::GetAllSettings() const
{
  C_HashTableIterator<C_String, C_String> cIterator =
                                                   m_cSettings.CreateIterator();
  C_Vector<C_Setting> vValues;

  while(cIterator.HasNext())
  {
    C_HashTableNode<C_String, C_String>* pNode = cIterator.GetNext();
    C_String strName = pNode->GetKey();
    C_String* pstrValue = pNode->GetValue();

    // Add the setting to the result
    C_Setting* pSetting = new C_Setting(strName, *pstrValue);
    vValues.Add(pSetting);
  }

  return vValues;
}
