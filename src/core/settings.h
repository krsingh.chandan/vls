/*******************************************************************************
* settings.h: Settings class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: settings.h,v 1.7 2003/08/29 10:20:52 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _SETTINGS_H_
#define _SETTINGS_H_


//------------------------------------------------------------------------------
// C_Setting
//------------------------------------------------------------------------------
// Represents a single setting, ie a name and the associated value
//------------------------------------------------------------------------------
class C_Setting
{
 public:
  C_Setting(const C_String& strName, const C_String& strValue) : 
                                        m_strName(strName), m_strValue(strValue)
  {}

  C_Setting* Clone() const
  { return new C_Setting(*this); }

  const C_String& GetName() const
  { return m_strName; }
  const C_String& GetValue() const
  { return m_strValue; }

  bool operator < (const C_Setting& sArg) const
  { return (m_strName < sArg.GetName()); }
  bool operator > (const C_Setting& sArg) const
  { return (m_strName > sArg.GetName()); }

private:
  C_String m_strName;
  C_String m_strValue;
};



//------------------------------------------------------------------------------
// C_Settings
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
class C_Settings
{
 public:
  // Default constructor
  C_Settings();

  // Retrieve the config from both command line and config file
  void Load(const C_String& strDefaultCfgFile, bool bReadCmdLine = true);
  void AddCmdLineTarget(C_String sTarget, C_String sTags);
  void AddCmdLineSetting(char sOption, C_String sValue);
  // Store it in an updated version of the cfg file
  void Store();

  // Update the value for a given property, or add it if it is not already
  // defined
  void Update(const C_String& strName, const C_String& strValue);
  void Delete(const C_String& strName);

  // Access to the properties 
  C_String GetSetting(const C_String& strName, const C_String& strDflt) const;
  C_Vector<C_Setting> GetSettings(const C_String& strSection) const;
  C_Vector<C_Setting> GetAllSettings() const;

 private:
  C_HashTable<C_String, C_String> m_cSettings;  
};



#else
#error "Multiple inclusions of settings.h"
#endif

