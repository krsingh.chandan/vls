/*******************************************************************************
* module.cpp: Module management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: module.cpp,v 1.7 2003/10/27 13:52:53 sam Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#include "config.h"


#include <stdio.h>
#include <string.h>
#include <sys/types.h>

#ifdef HAVE_DLFCN_H
#include <dlfcn.h>                                              // For library.h
#endif

#ifdef HAVE_OPENDIR
#include <sys/stat.h>                                              // For file.h
#include <dirent.h>                                                // For file.h
#endif


#include "common.h"
#include "debug.h"
#include "string.h"
#include "vector.h"
#include "hashtable.h"
#include "exception.h"
#include "file.h"
#include "log.h"
#include "library.h"
#include "settings.h"

#include "module.h"
#include "application.h"


#include "vector.cpp"
#include "hashtable.cpp"
#include "library.cpp"


//******************************************************************************
// class C_Module
//******************************************************************************
//
//******************************************************************************


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Module::C_Module(handle hLog,
                   const C_String& strType,
                   const C_String& strName) : m_strType(strType),
                                              m_strName(strName)
{
  ASSERT(hLog);

  m_hLog = hLog;

  m_iRefCounter = 0;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_Module::~C_Module()
{
  ASSERT(!m_iRefCounter);
}


//******************************************************************************
// class C_ModuleManager
//******************************************************************************
//
//******************************************************************************


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_ModuleManager::C_ModuleManager(handle hLog)
{
  m_hLog = hLog;

  m_pModuleTypes = new C_HashTable<C_String, C_HashTable<C_String, C_Module> >;
  ASSERT(m_pModuleTypes);
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_ModuleManager::~C_ModuleManager()
{
  delete m_pModuleTypes;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_ModuleManager::BrowseDirectory(const C_String& strDir)
{
  C_Directory cDirectory(strDir);

  Log(m_hLog, LOG_NOTE, "Browsing modules in directory \"" + strDir + "\"");

  C_Vector<C_String> vFiles;
  C_Vector<C_String> vSubDirs;

  try
  {
    cDirectory.Open();
    vFiles = cDirectory.GetFiles();
    vSubDirs = cDirectory.GetSubDirs();
    cDirectory.Close();
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_WARN, "Unable to browse directory \"" + strDir +
                          "\" : " + e.Dump());
  }

  for(unsigned int ui = 0; ui < vSubDirs.Size(); ui++)
  {
    BrowseDirectory(strDir + "/" + vSubDirs[ui]);
  }

  for(unsigned int ui = 0; ui < vFiles.Size(); ui++)
  {
    if(vFiles[ui].EndsWith(".so"))
    {
      C_String strFullName = strDir + "/" + vFiles[ui];
      LogDbg(m_hLog, "trying file \"" + strFullName + "\"");

      C_Library<C_Module, handle>* pLib =
                      new C_Library<C_Module, handle>(strFullName);
      C_Module* pModule = NULL;

      try
      {
        pLib->Load();
        pModule = pLib->CreateInstance(m_hLog);
        RegisterModule(pModule);
        m_cLibraries.Add(pModule->GetType()+":"+pModule->GetName(), pLib);
      }
      catch(E_Exception e)
      {
        if(pModule)
        {
          delete pModule;
        }
        else
        {
          Log(m_hLog, LOG_ERROR, "No module in file \"" + strFullName +
                                 "\" : " + e.Dump());
        }
        delete pLib;
      }
    }
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_ModuleManager::RegisterModule(C_Module* pModule)
{
  ASSERT(pModule);

  const C_String& strType = pModule->GetType();
  const C_String& strName = pModule->GetName();

  C_HashTable<C_String, C_Module>* pTable = m_pModuleTypes->Get(strType);

  if(!pTable)
  {
    pTable = new C_HashTable<C_String, C_Module>;
    m_pModuleTypes->Add(strType, pTable);
  }

  C_Module* pMod = pTable->Get(strName);

  if(pMod)
  {
    Log(m_hLog, LOG_ERROR, "Module \"" + strType + ":" + strName +
                           "\" already registered");
    throw E_Exception(GEN_ERR, "Module \"" + strType + ":" + strName +
                               "\" already registered");
  }

  Log(m_hLog, LOG_NOTE, "Module \"" + strType + ":" + strName +
                        "\" registered");
  pTable->Add(strName, pModule);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Module* C_ModuleManager::GetModule(const C_String& strType,
                                     const C_String& strName)
{
  C_HashTable<C_String, C_Module>* pTable = m_pModuleTypes->Get(strType);
  if(pTable)
    return pTable->Get(strName);
  else
    return NULL;
}


