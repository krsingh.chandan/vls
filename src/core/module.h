/*******************************************************************************
* module.h: Module management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: module.h,v 1.4 2003/06/08 18:32:24 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _MODULE_H_
#define _MODULE_H_


//******************************************************************************
// class C_Module
//******************************************************************************
//
//******************************************************************************
class C_Module
{
public:
  C_Module(handle hLog,
           const C_String& strType, const C_String& strName);
  virtual ~C_Module();

  const C_String& GetType() const { return m_strType; };
  const C_String& GetName() const { return m_strName; };
  const handle GetLogger()  const { return m_hLog; };

  void Ref() { m_iRefCounter++; };
  void Unref() { m_iRefCounter--; ASSERT(m_iRefCounter >= 0); };
protected:
  C_String m_strType;
  C_String m_strName;

  int m_iRefCounter;

  handle m_hLog;
};


//******************************************************************************
// class C_ModuleManager
//******************************************************************************
//
//******************************************************************************
class C_ModuleManager
{
public:
  C_ModuleManager(handle hLog);
  ~C_ModuleManager();

  void RegisterModule(C_Module* pModule);
  void BrowseDirectory(const C_String& strDir);

  C_Module* GetModule(const C_String& strType, const C_String& strName);

protected:

private:
  C_HashTable<C_String, C_HashTable<C_String, C_Module> >* m_pModuleTypes;

  // "<type>:<name>" -> C_Library */
  C_HashTable<C_String, C_Library<C_Module, handle> > m_cLibraries;

  handle m_hLog;
};


//******************************************************************************
// DECLARE_VIRTUAL_MODULE
//******************************************************************************
// Declares and implements a module type.
//******************************************************************************
#define DECLARE_VIRTUAL_MODULE(ClassType, strType, arg)                       \
class C_##ClassType##Module : public C_Module                                 \
{                                                                             \
public:                                                                       \
  C_##ClassType##Module(handle hLog,                                          \
                        const C_String& strName) : C_Module(hLog,             \
                                                            strType,          \
                                                            strName)          \
  {                                                                           \
  };                                                                          \
                                                                              \
  virtual C_##ClassType * New##ClassType (arg val) = 0;                       \
};


//******************************************************************************
// DECLARE_MODULE
//******************************************************************************
// Declares and implements a module.
//******************************************************************************
#define DECLARE_MODULE(ClassName, ClassType, strName, arg)                    \
class C_##ClassName##ClassType##Module : public C_##ClassType##Module         \
{                                                                             \
public:                                                                       \
  C_##ClassName##ClassType##Module(handle hLog) :                             \
                                C_##ClassType##Module(hLog, strName)          \
  {                                                                           \
  };                                                                          \
                                                                              \
  virtual C_##ClassType * New##ClassType (arg val)                           \
  {                                                                           \
    LogDbg(m_hLog, "New instance of module \"" + m_strType +                  \
                   ":" + m_strName + "\"");                                   \
    Ref();                                                                    \
    return new C_##ClassName##ClassType (this, val);                          \
  };                                                                          \
}


#else
#error "Multiple inclusions of module.h"
#endif

