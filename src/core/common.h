/*******************************************************************************
* common.h: common definitions
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: common.h,v 1.6 2003/06/02 19:50:35 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Collection of error codes, usefull types and byte manipulations macros
*
*******************************************************************************/


#ifndef _COMMON_H_
#define _COMMON_H_


//------------------------------------------------------------------------------
// Return codes
//------------------------------------------------------------------------------

// OK return code
#define NO_ERR          0

// Error codes: always < 0
#define GEN_ERR         -1
#define FILE_ERR        -2
#define FILE_EOF        -3
#define FILE_BOF        -4
#define NET_ERR         -5
#define MUTEX_ERR       -6
#define SYNC_ERR        -7

// Status code: always > 0
#define BUFFER_FULL     1
#define BUFFER_EMPTY    2
#define MUTEX_LOCKED    3

#define NO_CHANGE       4
#define UPDATED         5


//------------------------------------------------------------------------------
// Shared constants
//------------------------------------------------------------------------------
#define YES             1
#define NO              0
#define SMART           2


//------------------------------------------------------------------------------
// Types definitions
//------------------------------------------------------------------------------

// Booleans types for solaris are defined in the stl
#ifdef SOLARIS
#include <stl_config.h>
#endif

// Basic types definitions
typedef signed char         s8;
typedef signed short        s16;
typedef signed int          s32;
#ifdef _WIN32
typedef __int64             s64;
#else
typedef signed long long    s64;
#endif

typedef unsigned char       u8;
typedef unsigned short      u16;
typedef unsigned int        u32;
#ifdef _WIN32
typedef unsigned __int64    u64;
#else
typedef unsigned long long  u64;
#endif

// Byte type
typedef u8                  byte;
 
// Handles
typedef void*               handle;

// Counter for statistics and profiling
typedef unsigned long       count;



//------------------------------------------------------------------------------
// Byte manipulation macros
//------------------------------------------------------------------------------

// CEIL: division with round to nearest greater integer
#define CEIL(n, d)  ( ((n) / (d)) + ( ((n) % (d)) ? 1 : 0) )

// PAD: PAD(n, d) = CEIL(n ,d) * d
#define PAD(n, d)   ( ((n) % (d)) ? ((((n) / (d)) + 1) * (d)) : (n) )

// MSB/LSB (ie big endian/little endian) convertions - network order is always
// MSB, and should be used for both network communications and files
//#define hton16      htons
//#define hton32      htonl
//#define ntoh16      ntohs                              
//#define ntoh32      ntohl

// Macros used to read a TS packet
// Disabled because ntoh* isn't alignment-safe --Meuuh
//#define U16_AT(p)   ( ntoh16(*((u16 *)&(p))) )
//#define U32_AT(p)   ( ntoh32(*((u32 *)&(p))) )
#define U16_AT(p)       (   (((u16)((u8*)&(p))[0]) << 8)                \
                          | (((u16)((u8*)&(p))[1])) )
#define U32_AT(p)       (   (((u32)((u8*)&(p))[0]) << 24)               \
                          | (((u32)((u8*)&(p))[1]) << 16)               \
                          | (((u32)((u8*)&(p))[2]) << 8)                \
                          | (((u32)((u8*)&(p))[3])) )

// Macros used to write a TS packet
//#define SET_U16_TO(p, n)        ( *((u16*)&p) = hton16(n) )
//#define SET_U32_TO(p, n)        ( *((u32*)&p) = hton32(n) )
#define SET_U16_TO(p, n)        ((u8*)&(p))[0] = (n) >> 8;              \
                                ((u8*)&(p))[1] = (n);
#define SET_U32_TO(p, n)        ((u8*)&(p))[0] = (n) >> 24;             \
                                ((u8*)&(p))[1] = (n) >> 16;             \
                                ((u8*)&(p))[2] = (n) >> 8;              \
                                ((u8*)&(p))[3] = (n);


#else
#error "Multiple inclusions of common.h"
#endif

