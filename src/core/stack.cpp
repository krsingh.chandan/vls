/*******************************************************************************
* stack.cpp: Stack management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: stack.cpp,v 1.5 2003/03/09 15:18:05 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at list.h for further explanations



//******************************************************************************
// C_Fifo class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Fifo<T>::C_Fifo(unsigned int iCapacity, byte bAutoClean,
                                     bool bOverride)
{
  m_iCapacity = iCapacity;
  m_apBuff = new T* [iCapacity + 1];
  ASSERT(m_apBuff);

  m_iWhereToPush = 0;
  m_iWhereToPop = 0;

  m_bOverride = bOverride;
  m_bAutoClean = bAutoClean;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Fifo<T>::~C_Fifo()
{
  Empty();

  ASSERT(m_apBuff);
  delete[] m_apBuff;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> C_Fifo<T>::C_Fifo(const C_Fifo<T>& cSrc)
{
  ASSERT(false);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Return BUFFER_FULL if the FIFO is full
//------------------------------------------------------------------------------
template <class T> int C_Fifo<T>::Push(T* pData)
{
  ASSERT(pData);
  int iRc = NO_ERR;

  // Only _one_ _read_ access to m_iWhereToPop to calculate the current size.
  if (   (m_iCapacity + 1 + (m_iWhereToPush - m_iWhereToPop))
       % (m_iCapacity + 1) < m_iCapacity)
  {
    // Add the element
    m_apBuff[m_iWhereToPush] = pData;
    // Update the WhereToPush index
    if (++m_iWhereToPush >= m_iCapacity + 1)
    {
      // We must roll to the 1st element of the array
      m_iWhereToPush = 0;
    }
  }
  else
  {
    // There is no space to store the data: decide what to do
    //according to the settings

    if(m_bOverride)
    {
      // Override the older element
      T* pOldData = Pop();
      ASSERT(pOldData);
      Push(pData);
      switch(m_bAutoClean)
      {
        case YES:
        {
          delete pOldData;
          break;
        }
        case NO:
        {
          // Nothing to do
          break;
        }
        case SMART:
        {
          ASSERT(false);        // To write
          break;
        }
        default:
        {
          ASSERT(false);
          break;
        }
      }
    }
    else
    {
      // Buffer is full, return the corresponding error
      iRc = BUFFER_FULL;
    }
  }

  ASSERT(m_iWhereToPush != m_iWhereToPop);

  return iRc;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Return the first pushed data, or NULL if the FIFO is empty
//------------------------------------------------------------------------------
template <class T> T* C_Fifo<T>::Pop()
{
  T* pData = NULL;
  
  // Only _one_ _read_ access to m_iWhereToPush.
  if (m_iWhereToPush != m_iWhereToPop)
  {
    // Read the element
    pData = m_apBuff[m_iWhereToPop];

    // Update the WhereToPop pointer
    if (++m_iWhereToPop >= m_iCapacity + 1)
    {
      // We must roll to the 1st element of the array */
      m_iWhereToPop = 0;
    }
  }

  return pData;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Take a peek at the data on position given.
//------------------------------------------------------------------------------
template <class T> T* C_Fifo<T>::Peek(unsigned int uiIndex)
{
  T* pData = NULL;

  // Check that there is valid data stored a the given index.
  ASSERT(uiIndex < Size());
  
  // Only _one_ _read_ access to m_iWhereToPush.
  if (uiIndex < Size())
  {
    unsigned int iElemPos = m_iWhereToPop + uiIndex;

    if(iElemPos > m_iCapacity)
      iElemPos = iElemPos - m_iCapacity - 1;
 
    pData = m_apBuff[iElemPos];
  }
  
  return pData;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> T& C_Fifo<T>::operator [] (unsigned int iIndex) const
{
  // Check that there is a valid data stored at the given index
  ASSERT(iIndex < Size());

  // Return the address of the packet
  unsigned int iElemPos = m_iWhereToPop + iIndex;

  if(iElemPos >= m_iCapacity + 1)
    iElemPos = iElemPos - m_iCapacity - 1;
  
  return *m_apBuff[iElemPos];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class T> void C_Fifo<T>::Empty()
{
  switch(m_bAutoClean)
  {
    case YES:
    {
      unsigned int iCount = Size();
      for(unsigned int i = 0; i < iCount; i++)
      {
        T* pData = Pop();
        ASSERT(pData);
        delete pData;
      }
      ASSERT(!Pop());
      break;
    }
    case NO:
    {
      // Nothing to do
      break;
    }
    case SMART:
    {
      ASSERT(false);    // To do
      break;
    }
    default:
    {
      ASSERT(false);
      break;
    }
  }
}

