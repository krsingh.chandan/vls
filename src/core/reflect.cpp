/*******************************************************************************
* reflect.cpp: Reflective information support
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*-------------------------------------------------------------------------------
* This file provide classes to build and/or obtain reflective information
* about classes
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <stdio.h>

#include "common.h"
#include "debug.h"
#include "reflect.h"


/*******************************************************************************
* C_ClassField
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ClassField::C_ClassField(byte iType, u32 iOffset, u32 iSubTypes)
{
  ASSERT(iType != UNKNOWN);

  m_bFieldType = iType;
  m_iOffset = iOffset;
  m_aSubTypes = new C_ClassField[iSubTypes];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ClassField::C_ClassField()
{
  m_bFieldType = UNKNOWN;
  m_iOffset = 0;
  m_aSubTypes = NULL;
}




/*******************************************************************************
* C_ClassMethod
********************************************************************************
* 
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ClassMethod::C_ClassMethod(byte, byte[])
{
  // To write
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ClassMethod::C_ClassMethod()
{
  // To write
}





/*******************************************************************************
* C_ClassDescription
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ClassDescription::C_ClassDescription(unsigned int iFields, unsigned int iMethods)
{
  m_iCurrentField = 0;
  m_iCurrentMethod = 0;

  m_iFields = iFields;
  m_iMethods = iMethods;

  m_aFields = new C_ClassField[iFields];
  m_aMethods = new C_ClassMethod[iMethods];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_ClassDescription::~C_ClassDescription()
{
  delete[] m_aFields;
  delete[] m_aMethods;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_ClassDescription::AddField(const C_ClassField& cField)
{
  ASSERT(m_iCurrentField < m_iFields-1);

  m_aFields[m_iCurrentField] = cField;
  m_iCurrentField++;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_ClassDescription::AddMethod(const C_ClassMethod& cMethod)
{
  ASSERT(m_iCurrentMethod < m_iMethods-1);

  m_aMethods[m_iCurrentMethod] = cMethod;
  m_iCurrentMethod++;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const C_ClassField& C_ClassDescription::GetField(unsigned int iField) const
{
  ASSERT(iField < m_iFields);
  return m_aFields[iField];
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
const C_ClassMethod& C_ClassDescription::GetMethod(unsigned int iMethod) const
{
  ASSERT(iMethod < m_iMethods);
  return m_aMethods[iMethod];
}


