/*******************************************************************************
* socket.cpp: socket management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This class encapsulate the differences between the implementations of the
* BSD sockets
* TO DO: the gethostbyname function is not thread safe, protect it
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include <string.h>
#include <stdlib.h>

#if defined WIN32
#include <winsock2.h>
#else
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h> 
#include <netdb.h>
#include <unistd.h>     
#include <errno.h>
#endif

#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
#include <sys/un.h>
#endif

#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
#include <sys/uio.h>
#endif

#include <stdio.h>

#include "common.h"
#include "debug.h"
#include "string.h"
#include "exception.h"
#include "system.h"
#include "vector.h" 
#include "socket.h"

#include "vector.cpp"




//------------------------------------------------------------------------------
// Plateform specific issues
//------------------------------------------------------------------------------
#if defined WIN32
#   define ERR_INTR        WSAEINTR
#else
#   define ERR_INTR        EINTR
#endif


//******************************************************************************
// E_InetAddr class
//******************************************************************************
// 
//******************************************************************************
E_InetAddr::E_InetAddr(const C_String& strMsg) :
                            E_Exception(GetErrorCode(SYSTEM_NETWORK), strMsg)
{
}

E_InetAddr::E_InetAddr(const C_String& strMsg, const E_InetAddr& e) :
                            E_Exception(GetErrorCode(SYSTEM_NETWORK), strMsg, e)
{
}


//******************************************************************************
// E_Socket class
//******************************************************************************
// 
//******************************************************************************
E_Socket::E_Socket(const C_String& strMsg) :
                            E_Exception(GetErrorCode(SYSTEM_NETWORK), strMsg)
{
}

E_Socket::E_Socket(const C_String& strMsg, const E_Socket& e) :
                            E_Exception(GetErrorCode(SYSTEM_NETWORK), strMsg, e)
{
}

E_Socket::E_Socket(const C_String& strMsg, const E_InetAddr& e) :
                            E_Exception(GetErrorCode(SYSTEM_NETWORK), strMsg, e)
{
}


//******************************************************************************
// C_SocketBuff class
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_SocketBuff::C_SocketBuff(unsigned int iSlotCount)
{
  m_iSlotCount = iSlotCount;
  m_pBuffers = new sock_buff[iSlotCount];
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_SocketBuff::~C_SocketBuff()
{
  ASSERT(m_pBuffers);
  delete[] m_pBuffers;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_SocketBuff::AssignSlot(unsigned int iSlot, char* pBuff,
                              unsigned int iBuffSize)
{
  ASSERT(iSlot < m_iSlotCount);
  ASSERT(pBuff);
  
#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
  m_pBuffers[iSlot].iov_base = pBuff;
  m_pBuffers[iSlot].iov_len = iBuffSize;
#elif defined WIN32
  m_pBuffers[iSlot].buf = pBuff;
  m_pBuffers[iSlot].len = iBuffSize;
#endif
}




//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_SocketBuff::SetSlotBuff(unsigned int iSlot, char* pBuff)
{
  ASSERT(iSlot < m_iSlotCount);
  ASSERT(pBuff);
  
#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
  m_pBuffers[iSlot].iov_base = pBuff;
#elif defined WIN32
  m_pBuffers[iSlot].buf = pBuff;
#endif
}




//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_SocketBuff::SetSlotSize(unsigned int iSlot, unsigned int iBuffSize)
{
  ASSERT(iSlot < m_iSlotCount);
  
#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
  m_pBuffers[iSlot].iov_len = iBuffSize;
#elif defined WIN32
  m_pBuffers[iSlot].len = iBuffSize;
#endif
}


//******************************************************************************
// C_Inet4Addr class
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Inet4Addr::Build(const C_String& strHost, const C_String& strPort)
{
  // Init the structure
  memset(&m_sInetAddr, 0, sizeof(m_sInetAddr));
  m_sInetAddr.sin_family = AF_INET;

  // Set the port field
  int iPort = strPort.ToInt();
  if(iPort)
  {
    // Port is given by its number
    m_sInetAddr.sin_port = htons(iPort);
  }
  else
  {
    // It should be given by its service name
    struct servent* pPort = getservbyname(strPort.GetString(), NULL);
    if(pPort == NULL)
    {
      throw E_InetAddr("Unknown port: " + strPort);
    }
    else
      m_sInetAddr.sin_port = pPort->s_port;
  }

  // Set the address field
  // Assume the address is given in dotted-decimal format and try to convert
  m_sInetAddr.sin_addr.s_addr = inet_addr(strHost.GetString());
  if(m_sInetAddr.sin_addr.s_addr==(u32)-1 &&
     strHost.Find("255.255.255.255") < 0) 
  {
    // Address should be given by a host name
    struct hostent* pHostEnt = gethostbyname(strHost.GetString());
    if(pHostEnt == NULL)
    {
      throw E_InetAddr("Unknown host: " + strHost);
    }
    else
      memcpy(&m_sInetAddr.sin_addr, pHostEnt->h_addr, pHostEnt->h_length);
  }
}


//******************************************************************************
// C_Inet6Addr class
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//                                                                <
//------------------------------------------------------------------------------
#ifdef HAVE_IPV6
void C_Inet6Addr::Build(const C_String& strHost, const C_String& strPort,
                        int iType)
{
  struct addrinfo sHints;
  sHints.ai_flags = 0;
  sHints.ai_family = AF_INET6;
  sHints.ai_socktype = iType;
  sHints.ai_protocol = 0;
  sHints.ai_addrlen = 0;
  sHints.ai_addr = NULL;
  sHints.ai_canonname = NULL;
  sHints.ai_next = NULL;

  struct addrinfo *pRes = NULL;

  int iRc = getaddrinfo(strHost.GetString(), strPort.GetString(),
                        &sHints, &pRes);

  if(!iRc)
  {
    ASSERT(pRes->ai_family == AF_INET6);
    m_sInetAddr = *(sockaddr_in6*)(pRes->ai_addr);
    freeaddrinfo(pRes);
  }
  else
  {
    ASSERT(!pRes);
    throw E_InetAddr(C_String("Inet6 address error: ") + gai_strerror(iRc));
  }
}
#endif

//******************************************************************************
// C_UnixAddr class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
void C_UnixAddr::Build(const C_String& strPath)
{
  ASSERT(strPath.Length() <= sizeof(m_sUnixAddr.sun_path) /* 108 */);

  m_sUnixAddr.sun_family = AF_UNIX;
  strncpy(m_sUnixAddr.sun_path, strPath.GetString(),
                                sizeof(m_sUnixAddr.sun_path));
}
#endif



//******************************************************************************
// C_Socket class
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Socket::C_Socket()
{
  m_iDomain = AF_UNSPEC;
  ZERO(m_hSocket);
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::Open(int iDomain, int iType, int iProtocol/* = 0*/)
{
  m_iDomain = iDomain;
  m_iType = iType;

  m_hSocket = socket(m_iDomain, iType, iProtocol);

  if(m_hSocket < 0)
  {
    throw E_Socket("Unable to open socket: " + GetErrorMsg(SYSTEM_NETWORK));
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::Close()
{
#if defined WIN32
  int iRc = closesocket(m_hSocket);
#else
  int iRc = close(m_hSocket);
#endif

  if(iRc)
    throw E_Socket("Socket could not be closed properly: " +
                   GetErrorMsg(SYSTEM_NETWORK));
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::GetDomain()
{
	// returns the address family of this socket
	// or AF_UNSPEC (set in constructor)
	return m_iDomain;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::SetOption(int iLevel, int iOpt, const char* pOptVal, socklen_t iOptLen)
{
  int iRc = setsockopt(m_hSocket, iLevel, iOpt, pOptVal,
                       iOptLen);

  if(iRc)
  {
    throw E_Socket(C_String("Unable to change value for option ") +
                   iOpt + ": " + GetErrorMsg(SYSTEM_NETWORK));
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::GetOption(int iLevel, int iOpt, char* pOptVal, socklen_t* pOptLen)
{
  int iRc = getsockopt(m_hSocket, iLevel, iOpt, pOptVal,
                       pOptLen);

  if(iRc)
  {
    throw E_Socket(C_String("Unable to get value for option ") +
                   iOpt + ": " + GetErrorMsg(SYSTEM_NETWORK));
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::Bind(const C_String& strIntf, const C_String& strPort)
{
  // Try to resolve the given address
  struct sockaddr* pAddr = 0; /* Just to get rid of the compiler warning */
                              /* TODO : check whether it is a real error */
  int iAddrLen = 0;

  if(m_iDomain == AF_INET)
  {
    C_Inet4Addr cIntfAddr;

    try
    {
      cIntfAddr.Build(strIntf, strPort);
    }
    catch(E_InetAddr e)
    {
      throw E_Socket("Unable to bind to " + strIntf + " on port " + strPort, e);
    }

    pAddr = (struct sockaddr*)cIntfAddr.GetInetAddr();
    iAddrLen = sizeof(*cIntfAddr.GetInetAddr());
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    C_Inet6Addr cIntfAddr;

    try
    {
      cIntfAddr.Build(strIntf, strPort, m_iType);
    }
    catch(E_InetAddr e)
    {
      throw E_Socket("Unable to bind to " + strIntf + " on port " + strPort, e);
    }

    pAddr = (struct sockaddr*)cIntfAddr.GetInetAddr();
    iAddrLen = sizeof(*cIntfAddr.GetInetAddr());
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
    C_UnixAddr cIntfAddr;

    cIntfAddr.Build(strIntf);

    cIntfAddr.Unlink();
    
    pAddr = (struct sockaddr*)cIntfAddr.GetUnixAddr();
    iAddrLen = sizeof(*cIntfAddr.GetUnixAddr());
  }
#endif
  else
  {
    ASSERT(false);
  }
  
  // Bind the socket to the local address
  int iRc = bind(m_hSocket, pAddr, iAddrLen);
  if(iRc)
  {
    throw E_Socket("Unable to bind to " + strIntf + " on port " + strPort +
                   ": " + GetErrorMsg(SYSTEM_NETWORK));
  }
}
  

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::Connect(const C_String& strPeer, const C_String& strPort)
{
  // Try to resolve the given address
  struct sockaddr* pAddr = 0; /* Just to get rid of the compiler warning */
                              /* TODO : check whether it is a real error */
  int iAddrLen = 0;

  if(m_iDomain == AF_INET)
  {
    C_Inet4Addr cPeerAddr;

    try
    {
      cPeerAddr.Build(strPeer, strPort);
    }
    catch(E_InetAddr e)
    {
      throw E_Socket("Unable to connect to " + strPeer +
                     " on port "+ strPort, e);
    }

    pAddr = (struct sockaddr*)cPeerAddr.GetInetAddr();
    iAddrLen = sizeof(*cPeerAddr.GetInetAddr());
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    C_Inet6Addr cPeerAddr;

    try
    {
      cPeerAddr.Build(strPeer, strPort, m_iType);
    }
    catch(E_InetAddr e)
    {
      throw E_Socket("Unable to connect to " + strPeer +
                     " on port "+ strPort, e);
    }

    pAddr = (struct sockaddr*)cPeerAddr.GetInetAddr();
    iAddrLen = sizeof(*cPeerAddr.GetInetAddr());
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
    C_UnixAddr cPeerAddr;

    try
    {
      cPeerAddr.Build(strPeer);
    }
    catch(E_InetAddr e)
    {
      throw E_Socket("Unable to connect to " + strPeer, e);
    }

    pAddr = (struct sockaddr*)cPeerAddr.GetUnixAddr();
    iAddrLen = sizeof(*cPeerAddr.GetUnixAddr());
  }
#endif
  else
  {
    ASSERT(false);
  }

  // Bind the socket to the local address
  int iRc = connect(m_hSocket, pAddr, iAddrLen);
  if(iRc)
  {
    throw E_Socket("Unable to connect to " + strPeer + " on port " +
                   strPort+": " + GetErrorMsg(SYSTEM_NETWORK));
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Socket::Listen(int iBacklog)
{
  int iRc = listen(m_hSocket, iBacklog);
  if(iRc)
  {
    throw E_Socket("Unable to listen for "+GetPeerName()+"/"+
                   GetPeerPort()+" on "+GetLocalName()+"/"+GetLocalPort()+": "+
                   GetErrorMsg(SYSTEM_NETWORK));
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Socket* C_Socket::Accept()
{
  int iRc = -1;

  if(m_iDomain == AF_INET)
  {
    struct sockaddr_in sPeer;
    socklen_t iAddrLen = sizeof(sPeer);
  
    iRc = accept(m_hSocket, (struct sockaddr*)&sPeer, &iAddrLen);
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    struct sockaddr_in6 sPeer;
    socklen_t iAddrLen = sizeof(sPeer);

    iRc = accept(m_hSocket, (struct sockaddr*)&sPeer, &iAddrLen);
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
    struct sockaddr_un sPeer;
    socklen_t iAddrLen = sizeof(sPeer);

    iRc = accept(m_hSocket, (struct sockaddr*)&sPeer, &iAddrLen);
  }
#endif
  else
  {
    ASSERT(false);
  }

  C_Socket* pSocket = NULL;

  if(iRc < 0)
  {
    throw E_Socket("Unable to accept connection from " + GetPeerName() +
                   ": "+GetErrorMsg(SYSTEM_NETWORK));
  }
  else
  {
    pSocket = new C_Socket();
    pSocket->m_iDomain = m_iDomain;
    pSocket->m_hSocket = iRc;
  }
  
  return pSocket;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// This method waits for data until some are available or a signal is caught
// If you want the signal safe equivalent method, call the following one with
// a timeout set to 0
// Returns the number of byte read or throw an exception if an error occurs
//------------------------------------------------------------------------------
int C_Socket::Read(char* pBuff, int iBuffLen, int iFlags)
{
  ASSERT(pBuff);

  // Wait for data until something is received
  int iRc = recv(m_hSocket, pBuff, iBuffLen, iFlags);
  
  if(iRc < 0)
  {
    throw E_Socket("Unable to receive data: " + GetErrorMsg(SYSTEM_NETWORK));
  }

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::ReadTimeOut(char* pBuff, int iBuffLen, int iTimeOut, int iFlags)
{
  ASSERT(pBuff);
  
  int iRc = NO_ERR;

  // Put our socket in the set of socket to monitor
  fd_set sSockSet;
  FD_ZERO(&sSockSet);
  FD_SET(m_hSocket, &sSockSet);

  // Wait up to timeout seconds
  struct timeval sTv;
  sTv.tv_sec = iTimeOut;
  sTv.tv_usec = 0;

  // Select returns with error EINTR when a signal is caught: detect it
  // and retry a select when this occurs
  do
  {
    iRc = select(m_hSocket+1, &sSockSet, NULL, NULL, &sTv);

    if (iRc > 0)
    {
      // Data were received before timeout
      iRc = recv(m_hSocket, pBuff, iBuffLen, iFlags);
    }
    else
    {
      // Reinit the sTv and sockSet structures for the next loop
      sTv.tv_sec = iTimeOut;
      sTv.tv_usec = 0;
      FD_ZERO(&sSockSet);
      FD_SET(m_hSocket, &sSockSet);
    }
  }
  while (iRc == -1 && GetErrorCode(SYSTEM_NETWORK) == ERR_INTR);

  if(iRc < 0)
  {
    throw E_Socket("Unable to receive data: "+ GetErrorMsg(SYSTEM_NETWORK));
  }

  return iRc;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// This method waits for data until some are available or a signal is caught
// If you want the signal safe equivalent method, call the following one with
// a timeout set to 0
// Returns the number of byte written or throw an exception if an error occurs
//------------------------------------------------------------------------------
int C_Socket::Write(const char* pBuff, int iBuffLen, int iFlags)
{
  ASSERT(pBuff);

  // Wait until the data could be sent
  int iRc = send(m_hSocket, pBuff, iBuffLen, iFlags);

  if(iRc < 0)
  {
    throw E_Socket("Unable to send data: " + GetErrorMsg(SYSTEM_NETWORK));
  }

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::Send(const C_SocketBuff& cBuffers, unsigned int iSlotCount,
                                                 unsigned int iOffset)
{ 
  ASSERT(iSlotCount <= cBuffers.GetSlotCount());

  C_SocketBuff::sock_buff* pBuff = cBuffers.GetRawBuffers();
  
  // Send the data contained in the buffers
#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
  int iRc = writev(m_hSocket, &pBuff[iOffset], iSlotCount);
#elif defined WIN32
  unsigned long count;
  int iRc = WSASend(m_hSocket, pBuff+iOffset,
                    iSlotCount, &count, 0, NULL, NULL);
#endif

  if(iRc < 0)
  {
    throw E_Socket("Unable to send data: " + GetErrorMsg(SYSTEM_NETWORK));
  }
  
  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::Send(const C_SocketBuff& cBuffers)
{
  return Send(cBuffers, cBuffers.GetSlotCount(),0);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::WriteTo(C_Inet4Addr& cPeerAddr,
                      const char* pBuff, int iBuffLen, int iFlags)
{
  ASSERT(pBuff);
  ASSERT(m_iDomain == AF_INET);

  int iRc = sendto(m_hSocket, pBuff, iBuffLen, iFlags,
                   (const sockaddr*)cPeerAddr.GetInetAddr(),
                   sizeof(*cPeerAddr.GetInetAddr()));

/*  if(iRc < 0)
  {
    throw E_Socket("Unable to send data: " + GetErrorMsg(SYSTEM_NETWORK));
  }*/

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef HAVE_IPV6
int C_Socket::WriteTo(C_Inet6Addr& cPeerAddr,
                      const char* pBuff, int iBuffLen, int iFlags)
{
  ASSERT(pBuff);
  ASSERT(m_iDomain == AF_INET6);

  int iRc = sendto(m_hSocket, pBuff, iBuffLen, iFlags,
                   (const sockaddr*)cPeerAddr.GetInetAddr(),
                   sizeof(*cPeerAddr.GetInetAddr()));

/*  if(iRc < 0)
  {
    throw E_Socket("Unable to send data: " + GetErrorMsg(SYSTEM_NETWORK));
  }*/

  return iRc;
}
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
int C_Socket::WriteTo(C_UnixAddr& cPeerAddr,
                      const char* pBuff, int iBuffLen, int iFlags)
{
  ASSERT(pBuff);
  ASSERT(m_iDomain == AF_UNIX);

  int iRc = sendto(m_hSocket, pBuff, iBuffLen, iFlags,
                   (const sockaddr*)cPeerAddr.GetUnixAddr(),
                   sizeof(*cPeerAddr.GetUnixAddr()));

/*  if(iRc < 0)
  {
    throw E_Socket("Unable to send data: " + GetErrorMsg(SYSTEM_NETWORK));
  }*/

  return iRc;
}
#endif

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::SendTo(C_Inet4Addr&,
                     const C_SocketBuff&, unsigned int)
{
  // Further writing
  ASSERT(false);
  ASSERT(m_iDomain == AF_INET);
  int iRc = GEN_ERR;
  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::SendTo(C_Inet4Addr& cPeerAddr, const C_SocketBuff& cBuffers)
{
  return SendTo(cPeerAddr, cBuffers, cBuffers.GetSlotCount());
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::Recv(C_SocketBuff& cBuffers, unsigned int iSlotCount)
{
  ASSERT(iSlotCount <= cBuffers.GetSlotCount());

#ifdef STRUCT_IOVEC_IN_SYS_UIO_H
  C_SocketBuff::sock_buff* pBuff = cBuffers.GetRawBuffers();
  
  // Read the data and put them in the buffers
  int iRc = readv(m_hSocket, pBuff, iSlotCount);

#elif defined WIN32
  ASSERT(false);
  int iRc = GEN_ERR;
#endif

  if(iRc < 0)
  {
    throw E_Socket("Unable to read data: " + GetErrorMsg(SYSTEM_NETWORK));
  }
  
  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Socket::Recv(C_SocketBuff& cBuffers)
{
  return Recv(cBuffers, cBuffers.GetSlotCount());
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_Socket::operator == (const C_Socket& cArg)
{
  return cArg.m_hSocket == m_hSocket;
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Socket::GetLocalName() const
{
  C_String strLocalName("Unknown");

  if(m_iDomain == AF_INET)
  {
    struct sockaddr_in saIntf;
    socklen_t iNameLen = sizeof(saIntf);
    int iRc = getsockname(m_hSocket, (struct sockaddr*)&saIntf, &iNameLen);
    ASSERT(iNameLen == sizeof(saIntf));
    if(iRc)
    {
      throw E_Socket("Unable to get name of the intf: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
      strLocalName = inet_ntoa(saIntf.sin_addr);
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    struct sockaddr_in6 saIntf;
    socklen_t iNameLen = sizeof(saIntf);
    int iRc = getsockname(m_hSocket, (struct sockaddr*)&saIntf, &iNameLen);
    ASSERT(iNameLen == sizeof(saIntf));
    if(iRc)
    {
      throw E_Socket("Unable to get name of the intf: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
    {
      char pszDst[40]; // 40 characters is enough for an IPv6 address
      if(inet_ntop(AF_INET6, &saIntf.sin6_addr, pszDst, 40) == pszDst)
        strLocalName = pszDst;
      else if(errno != ENOSPC)
        throw E_Socket("Unable to get name of the intf: " +
                       GetErrorMsg(SYSTEM_NETWORK));
    }
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
    struct sockaddr_un saIntf;
    socklen_t iNameLen = sizeof(saIntf);
        
    int iRc = getsockname(m_hSocket, (struct sockaddr*)&saIntf, &iNameLen);
    ASSERT(iNameLen == sizeof(saIntf));
    if(iRc)
    {
      throw E_Socket("Unable to get name of the intf: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
    {
      strLocalName = saIntf.sun_path;
    }
  }
#endif
  else
  {
    ASSERT(false);
  }
  
  return strLocalName;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Socket::GetLocalPort() const
{
  C_String strLocalPort("Unknown");
  
  if(m_iDomain == AF_INET)
  {
    struct sockaddr_in saIntf;
    socklen_t iNameLen = sizeof(saIntf);
    int iRc = getsockname(m_hSocket, (struct sockaddr*)&saIntf, &iNameLen);
    ASSERT(iNameLen == sizeof(saIntf));
    if(iRc)
    {
      throw E_Socket("Unable to get port of the intf: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
      strLocalPort = ntohs(saIntf.sin_port);
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    struct sockaddr_in6 saIntf;
    socklen_t iNameLen = sizeof(saIntf);
    int iRc = getsockname(m_hSocket, (struct sockaddr*)&saIntf, &iNameLen);
    ASSERT(iNameLen == sizeof(saIntf));
    if(iRc)
    {
      throw E_Socket("Unable to get port of the intf: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
      strLocalPort = ntohs(saIntf.sin6_port);
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
    strLocalPort = 0;
  }
#endif
  else
  {
    ASSERT(false);
  }
  
  return strLocalPort;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Socket::GetPeerName() const
{
  C_String strPeerName("Unknown");

  if(m_iDomain == AF_INET)
  {
    struct sockaddr_in saPeer;
    socklen_t iNameLen = sizeof(saPeer);
    int iRc = getpeername(m_hSocket, (struct sockaddr*)&saPeer, &iNameLen);
    ASSERT(iNameLen == sizeof(saPeer));
    if(iRc)
    {
      throw E_Socket("Unable to get name of the peer: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
      strPeerName = inet_ntoa(saPeer.sin_addr);
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    struct sockaddr_in6 saPeer;
    socklen_t iNameLen = sizeof(saPeer);
    int iRc = getpeername(m_hSocket, (struct sockaddr*)&saPeer, &iNameLen);
    ASSERT(iNameLen == sizeof(saPeer));
    if(iRc)
    {
      throw E_Socket("Unable to get name of the peer: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
    {
      char pszDst[40]; // 40 characters is enough for an IPv6 address
      if(inet_ntop(AF_INET6, &saPeer.sin6_addr, pszDst, 40) == pszDst)
        strPeerName = pszDst;
      else if(errno != ENOSPC)
        throw E_Socket("Unable to get name of the intf: " +
                       GetErrorMsg(SYSTEM_NETWORK));
    }
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
    struct sockaddr_un saPeer;
    socklen_t iNameLen = sizeof(saPeer);
    int iRc = getsockname(m_hSocket, (struct sockaddr*)&saPeer, &iNameLen);
    ASSERT(iNameLen == sizeof(saPeer));
    if(iRc)
    {
      throw E_Socket("Unable to get name of the peer: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
    {
      strPeerName = saPeer.sun_path;
    }
  }
#endif
  else
  {
    ASSERT(false);
  }
  
  return strPeerName;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Socket::GetPeerPort() const
{
  C_String strPeerPort("Unknown");

  if(m_iDomain == AF_INET)
  {
    struct sockaddr_in saPeer;
    socklen_t iNameLen = sizeof(saPeer);
    int iRc = getpeername(m_hSocket, (struct sockaddr*)&saPeer, &iNameLen);
    ASSERT(iNameLen == sizeof(saPeer));
    if(iRc)
    {
      throw E_Socket("Unable to get port of the peer: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
      strPeerPort = ntohs(saPeer.sin_port);
  }
#ifdef HAVE_IPV6
  else if(m_iDomain == AF_INET6)
  {
    struct sockaddr_in6 saPeer;
    socklen_t iNameLen = sizeof(saPeer);
    int iRc = getpeername(m_hSocket, (struct sockaddr*)&saPeer, &iNameLen);
    ASSERT(iNameLen == sizeof(saPeer));
    if(iRc)
    {
      throw E_Socket("Unable to get port of the peer: " +
                     GetErrorMsg(SYSTEM_NETWORK));
    }
    else
      strPeerPort = ntohs(saPeer.sin6_port);
  }
#endif
#ifdef HAVE_SOCKADDR_UN_IN_SYS_UN_H
  else if(m_iDomain == AF_UNIX)
  {
      strPeerPort = 0;
  }
#endif
  else
  {
    ASSERT(false);
  }

  return strPeerPort;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Socket::GetName() const
{
  C_String strName;
  try
  {
    strName += GetPeerName();
  }
  catch(E_Exception e)
  {
    strName = "Unavailable";
  }

  return strName;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_String C_Socket::GetInfo() const
{
  C_String strInfo;
  try
  {
    strInfo += GetPeerName() + "/" + GetPeerPort();
    strInfo += "connected to " + GetLocalPort();
  }
  catch(E_Exception e)
  {
    strInfo = "Unavailable";
  }

  return strInfo;
}


//******************************************************************************
// 
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_SocketPool::C_SocketPool() : m_vRecvSockets(1), m_vSendSockets(1),
                               m_vExceptSockets(1), m_vWaitingSockets(5, false)
{
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_SocketPool::~C_SocketPool()
{
  // Remove the remaining Sockets from the cWaitingSockets list otherwise they
  // would be deleted twice
  //for(unsigned int i = m_vWaitingSockets.Size(); i > 0; i--)
  //  m_vWaitingSockets.Remove(i - 1);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_SocketPool::AddRecv(C_Socket* pSocket)
{
  m_vRecvSockets.Add(pSocket);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Socket* C_SocketPool::RemoveRecv(const C_Socket& cSocket)
{
  C_Socket* pSocket = NULL;

  int iPos = m_vRecvSockets.Find(cSocket);
  if(iPos != GEN_ERR)
    pSocket = m_vRecvSockets.Remove(iPos);

  return pSocket;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_SocketPool::AddSend(C_Socket* pSocket)
{
  m_vSendSockets.Add(pSocket);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Socket* C_SocketPool::RemoveSend(const C_Socket& cSocket)
{
  C_Socket* pSocket = NULL;
  
  int iPos = m_vSendSockets.Find(cSocket);
  if(iPos != GEN_ERR)
    pSocket = m_vSendSockets.Remove(iPos);
  
  return pSocket;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_SocketPool::AddExcept(C_Socket* pSocket)
{
  m_vExceptSockets.Add(pSocket);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Socket* C_SocketPool::RemoveExcept(const C_Socket& cSocket)
{
  C_Socket* pSocket = NULL;
  
  int iPos = m_vExceptSockets.Find(cSocket);
  if(iPos != GEN_ERR)
    pSocket = m_vExceptSockets.Remove(iPos);
  
  return pSocket;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Socket* C_SocketPool::Monitor()
{
  return InternalMonitor(NULL);
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Socket* C_SocketPool::Monitor(u32 iTimeOut)
{
  // Init the TimeOut
  struct timeval sTv;
  sTv.tv_sec = iTimeOut;
  sTv.tv_usec = 0;

  return InternalMonitor(&sTv);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Socket* C_SocketPool::InternalMonitor(struct timeval* pTimeOut)
{
  // To comment
  if(m_vWaitingSockets.Size() > 0)
  {
    C_Socket* pSocket = m_vWaitingSockets.Remove(m_vWaitingSockets.Size() - 1);
    return pSocket;
  }

  // Select return with error EINTR when a signal is caught: detect it
  // and retry a select when this occurs
  int iRc = NO_ERR;

  do
  {
    // Put our pools of sockets in the sets of socket to monitor
    // Also detect which is the higher handle among all
    C_Socket::sock_handle m_hHigherSocket = 0;

    fd_set sRecvSet;
    FD_ZERO(&sRecvSet);


    for(unsigned int i = 0; i < m_vRecvSockets.Size(); i++)
    {
      if(m_hHigherSocket < m_vRecvSockets[i].m_hSocket)
        m_hHigherSocket = m_vRecvSockets[i].m_hSocket;
    
      FD_SET(m_vRecvSockets[i].m_hSocket, &sRecvSet);
    }
  
    fd_set sSendSet;
    FD_ZERO(&sSendSet);
    for(unsigned int j = 0; j < m_vSendSockets.Size(); j++)
    {
      if(m_hHigherSocket < m_vSendSockets[j].m_hSocket)
        m_hHigherSocket = m_vSendSockets[j].m_hSocket;

      FD_SET(m_vSendSockets[j].m_hSocket, &sSendSet);
    }
  
    fd_set sExceptSet;
    FD_ZERO(&sExceptSet);
    for(unsigned int k = 0; k < m_vExceptSockets.Size(); k++)
    {
      if(m_hHigherSocket < m_vExceptSockets[k].m_hSocket)
        m_hHigherSocket = m_vExceptSockets[k].m_hSocket;

      FD_SET(m_vExceptSockets[k].m_hSocket, &sExceptSet);
    }

    // Monitor the sockets
    if(!pTimeOut)
      iRc = select(m_hHigherSocket+1, &sRecvSet, &sSendSet, &sExceptSet, NULL);
    else
    {
      struct timeval sTv;
      memcpy(&sTv, pTimeOut, sizeof(sTv));
      /* Select */
      iRc = select(m_hHigherSocket+1, &sRecvSet, &sSendSet, &sExceptSet, &sTv);
    }

    if(iRc > 0)
    {
      // An interesting event occured on a least one socket 
      for(unsigned int i = 0; i < m_vRecvSockets.Size(); i++)
      {
        if(FD_ISSET(m_vRecvSockets[i].m_hSocket, &sRecvSet))
        {
          C_Socket* pSocket = &m_vRecvSockets[i];
          m_vWaitingSockets.Add(pSocket);
        }
      }
      
      for(unsigned int j = 0; j < m_vSendSockets.Size(); j++)
      {
        if(FD_ISSET(m_vSendSockets[j].m_hSocket, &sSendSet))
        {
          C_Socket* pSocket = &m_vSendSockets[j];
          m_vWaitingSockets.Add(pSocket);
        }
      }
      
      for(unsigned int k = 0; k < m_vExceptSockets.Size(); k++)
      {
        if(FD_ISSET(m_vExceptSockets[k].m_hSocket, &sExceptSet))
        {
          C_Socket* pSocket = &m_vExceptSockets[k];
          m_vWaitingSockets.Add(pSocket);
        }
      }
    }
  }
  while(iRc == -1 && GetErrorCode(SYSTEM_NETWORK) == ERR_INTR);

   C_Socket* pSocket = NULL;

  if(iRc < 0)
  {
    throw E_Socket("Unable to receive data: " + GetErrorMsg(SYSTEM_NETWORK));
  }
  else if(iRc > 0)
  {
    ASSERT(m_vWaitingSockets.Size() > 0);
    pSocket = m_vWaitingSockets.Remove(m_vWaitingSockets.Size() - 1);
  }

  return pSocket;
}

