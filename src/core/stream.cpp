/*******************************************************************************
* stream.cpp: Stream class
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Notice: This file must be included in the source file with its header
*
*******************************************************************************/



//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
// There is no preamble since this file is to be included in the files which
// use the template: look at stream.h for further explanation



//******************************************************************************
// E_StreamException class
//******************************************************************************
// 
//******************************************************************************
template <class IOStream>
 E_Stream<IOStream>::E_Stream(int iCode, const C_String& strMsg) :
                    E_Exception(iCode, strMsg)
{
}

template <class IOStream>
 E_Stream<IOStream>::E_Stream(int iCode, const C_String& strMsg,
                    const E_Exception& eException) :  E_Exception(iCode, strMsg)
{
}






//******************************************************************************
// C_Stream class
//******************************************************************************
// 
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>::C_Stream(IOStream* pIOStream)
{
  ASSERT(pIOStream);
  m_pIOStream = pIOStream;
  m_bState = STREAM_READY;
  
  // Default marker is set to '\0' since this is the one used for strings
  SetMarker('\0');
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>::~C_Stream()
{
  ASSERT(m_pIOStream);
  delete m_pIOStream;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
template <class IOStream>
 void C_Stream<IOStream>::Close()
{
  try
  {
    m_pIOStream->Close();
  }
  catch(E_Exception e)
  {
    int iError = e.GetCode();
    throw E_Stream<IOStream>(iError, "Could not close stream", e);
  }
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_String C_Stream<IOStream>::GetName() const
{
  return m_pIOStream->GetName();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_String C_Stream<IOStream>::GetInfo() const
{
  return m_pIOStream->GetInfo();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>& C_Stream<IOStream>::operator << (const C_Buffer<byte>& cBuff)
{
  try
  {
    unsigned int iSize = cBuff.GetSize();
    char* pRawBuff = (char *)&cBuff[0];
    do
    {
      int iRc = m_pIOStream->Write(pRawBuff, iSize);
      iSize -= iRc;
      pRawBuff += iRc;
    }
    while(iSize > 0);
  }
  catch(E_Exception e)
  {
    m_bState = STREAM_ERROR;
    int iCode = e.GetCode();
    throw E_Stream<IOStream>(iCode, "Unable to write buffer");
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>& C_Stream<IOStream>::operator >> (C_Buffer<byte>& cBuff)
{
  // Try to fill the buffer
  unsigned int iSize = cBuff.GetCapacity();
  try
  {
    char* pRawBuff = (char *)&cBuff[0];
    int iRc = m_pIOStream->Read(pRawBuff, iSize);
    cBuff.SetSize(iRc);
  }
  catch(E_Exception e)
  {
    m_bState = STREAM_ERROR;
    int iCode = e.GetCode();
    throw E_Stream<IOStream>(iCode, "Unable to fill in buffer");    
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>& C_Stream<IOStream>::operator << (const C_String& strBuff)
{
  unsigned int iStrLen = strBuff.Length() + 1;
  const char* pszStrBuf = strBuff.GetString();

  try
  {
    do
    {
      int iRc = m_pIOStream->Write((const char*)pszStrBuf, iStrLen);
      iStrLen -= iRc;
      pszStrBuf += iRc;
    }
    while(iStrLen > 0);
  }
  catch(E_Exception e)
  {
    m_bState = STREAM_ERROR;
    int iCode = e.GetCode();
    throw E_Stream<IOStream>(iCode, "Unable to write string");
  }                
  
  return *this;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>& C_Stream<IOStream>::operator >> (C_String& strBuff)
{
  byte bBuf;

  // Read the string char by char
  try
  {
    int iRc = m_pIOStream->Read(&bBuf, 1);
    while(iRc == 1 && bBuf != m_bMarker)
    {
      strBuff += (char)bBuf;
      iRc = m_pIOStream->Read(&bBuf, 1);
    }
     
    if(iRc == FILE_EOF)
      m_bState = STREAM_EOF;

    ASSERT(iRc == 1 || iRc == FILE_EOF);
  }
  catch(E_Exception e)
  {
    m_bState = STREAM_ERROR;
    int iError = e.GetCode();
    throw E_Stream<IOStream>(iError, "Unable to read string", e);
  }
  
  return *this;
}



//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>& C_Stream<IOStream>::operator << (const C_Serializable& cObject)
{
  C_ClassDescription cObjectDescription = cObject.Reflect();
  C_Serializer cSerializer(&cObject, cObjectDescription);

  try
  {
    u32 iByteCount = cSerializer.NextBytesCount();
    while(iByteCount > 0)
    {
      // Get the data to serialize
      const byte* pBytes = cSerializer.GetNextBytes();

      // Write them on the stream
      do
      {
        int iRc = m_pIOStream->Write(pBytes, iByteCount);
        ASSERT(iRc >= 0);
        iByteCount -= iRc;
        pBytes += iRc;
      }
      while(iByteCount > 0);

      // Next iteration
      iByteCount = cSerializer.NextBytesCount();
    }
  }
  catch(E_Exception e)
  {
    m_bState = STREAM_ERROR;
    int iCode = e.GetCode();
    throw E_Stream<IOStream>(iCode, "Unable to write object");
  }

  return *this;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
template <class IOStream>
 C_Stream<IOStream>& C_Stream<IOStream>::operator >> (C_Serializable& cObject)
{
  try
  {
    u32 iByteCount = this->cSerializer.NextBytesCount();
    while(iByteCount > 0)
    {
      const byte aBytes[iByteCount];
      u32 iOffset = 0;

      // Read the data to deserialize on the stream
      do
      {
        int iRc = m_pIOStream->Read(aBytes+iOffset, iByteCount-iOffset);
        ASSERT(iRc >= 0 || iRc == FILE_EOF);
        iOffset += iRc;
      }
      while(iByteCount > 0 && this->iRc != FILE_EOF);

      // Deserialize them
      this->cSerializer.SetNextBytes(&aBytes);

      // Next iteration
      iByteCount = this->cSerializer.NextBytesCount();
    }
  }
  catch(E_Exception e)
  {
    m_bState = STREAM_ERROR;
    int iError = e.GetCode();
    throw E_Stream<IOStream>(iError, "Unable to read object", e);
  }

  return *this;
}

