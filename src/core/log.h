/*******************************************************************************
* log.h: Log class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: log.h,v 1.3 2002/09/10 11:56:28 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* The C_Log class provides a global logging service for all the 'clients' of
* application. The service is thread safe.
*
*******************************************************************************/


#ifndef _LOG_H_
#define _LOG_H_


//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------

// Maximum size allowed for a log message
#define LOG_MSGSIZE     128

// Log severity levels
#define LOG_DBG       1
#define LOG_NOTE        2
#define LOG_WARN        4
#define LOG_ERROR       8

// Log flags used at registration time
// First four bytes are used to control the filtering
#define LOG_QUIET       0       // Disable all logging for the client
#define LOG_DBGMSG      1       // Enable debug logging
#define LOG_NOTEMSG     2       // Enable logging of notice messages
#define LOG_WARNMSG     4       // Enable notice of warning messages
#define LOG_ERRORMSG    8       // Enable logging of error messages

// Four other bytes are used to ???
#define LOG_FILE        16      // Append message to the log file
#define LOG_SCR         32      // Log must be send to stderr
#define LOG_SYSTEM      64      // Send message to the system Log


//------------------------------------------------------------------------------
// C_Log class: provides the application with a unified logging format
//------------------------------------------------------------------------------
// Register ???
//------------------------------------------------------------------------------

class C_Log
{
 public:
  C_Log();
  ~C_Log();
  
  // Initialisation / termination
  int Init(const C_String& strLogName);
  int End();

  // Client registration / unregistration
  handle Register(const C_String& strClientDescr, u8 iFlags);
  void Unregister(handle hModule);

  // Message logging
  void Append(handle hModule, u8 iLevel, const C_String& strMsg);

 protected:
  C_File* m_pLogFile;
};



#else
#error "Multiple inclusions of log.h"
#endif

