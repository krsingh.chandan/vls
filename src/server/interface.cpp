/*******************************************************************************
* adminintf.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id$
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/



//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
//#include <ctype.h>

#include "config.h"
#include "../core/core.h"
#include "../core/network.h"
#include "request.h"
#include "program.h"
#include "repository.h"
#include "admin.h"
#include "interface.h"

#include "directory.h"

#include "../core/network.cpp"

#include "repository.cpp"

/*******************************************************************************
* E_Interface exception
********************************************************************************
*
*******************************************************************************/
E_Interface::E_Interface(const C_String& strMsg) :
                      E_Exception(GEN_ERR, strMsg)
{
}

E_Interface::E_Interface(const C_String& strMsg, const E_Exception& e) :
                      E_Exception(GEN_ERR, strMsg, e)
{
}

//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
//class C_Admin;


/*******************************************************************************
* C_InterfaceSession class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_InterfaceSession::C_InterfaceSession(C_Socket* pConnection,
                                           void* pAdmin) :
                            C_AdminSession((C_Admin*)pAdmin),
                            m_strPeerName("[Unknown]"), m_cStream(pConnection)
{
  ASSERT(pConnection);

  m_pConnection = pConnection;

  // Starting mode
  m_iPhase = INIT_PHASE;
}


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void C_InterfaceSession::OnInit()
{
      m_iPhase = LOGIN_PHASE;
}


//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void C_InterfaceSession::OnClose()
{
    try
    {
        m_pConnection->Close();
    }
    catch(E_Exception e)
    {
        throw E_Interface("Unable to close connection with "
                                                           + m_strPeerName, e);
    }
}

//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
//
//-----------------------------------------------------------------------------
void C_InterfaceSession::SendEvent(const C_Event& cEvent)
{
    try
    {
        C_Message cMessage(cEvent);
        C_String strEvent = cMessage.GetString();
        if(m_cStream.GetState() == STREAM_READY)
        {
            m_cStream << strEvent;
        }
    }
    catch(E_Exception e)
    {
        C_String strPeer = m_pConnection->GetPeerName();
        throw E_Interface("Unable to send data to "+strPeer, e);
    }
}

//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_InterfaceSession::~C_InterfaceSession()
{
}
/*******************************************************************************
* C_Interface class
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Interface::C_Interface(C_Module* pModule, C_InterfaceConfig& cConfig) //:
{
  ASSERT(cConfig.hLog);
  ASSERT(cConfig.pAdmin);

  m_hLog = cConfig.hLog;
  m_pAdmin = cConfig.pAdmin;
  m_pModule = pModule;
}

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Interface::~C_Interface() //:
{
  m_pModule->Unref();;
}
//------------------------------------------------------------------------------
// Parameters
//------------------------------------------------------------------------------

void C_Interface::SetConfig(C_String sModuleName)
{
  m_sInterfaceName = sModuleName;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Interface::PropagateEvent(const C_Event&)
{
}

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
int C_Interface::Run()
{
  int iRc = NO_ERR;

  try
  {
      RunSession();
  }
  catch(E_Exception e)
  {
    iRc = GEN_ERR;
    Log(m_hLog, LOG_ERROR, m_sInterfaceName
                              + "interface launch failed:\n" + e.Dump());
  }
  return iRc;
}


//------------------------------------------------------------------------------
// Stop
//------------------------------------------------------------------------------
int C_Interface::StopInterface()
{
  int iRc = NO_ERR;

  try
  {
    //We have to stop all sessions
    StopSession();
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_ERROR, "Could not stop interface:\n" + e.Dump());
    iRc = GEN_ERR;
  }

  if(!iRc)
    Log(m_hLog, LOG_NOTE, m_sInterfaceName + " interface stopped");

  return iRc;

}


//------------------------------------------------------------------------------
// Cleaning
//------------------------------------------------------------------------------
int C_Interface::Destroy()
{
  int iRc = NO_ERR;

  try
  {
    DestroySession();
  }
  catch(E_Exception e)
  {
    Log(m_hLog, LOG_ERROR, "Error during "+m_sInterfaceName
                                    +" interface destruction:\n" + e.Dump());
    iRc = GEN_ERR;
  }

  if(!iRc)
    Log(m_hLog, LOG_NOTE, m_sInterfaceName+" interface destroyed");

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Interface::InitWork()
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Interface::DoWork()
{

  try
  {
    RunSession();
  }
  catch(E_Exception e)
  {
    throw E_Interface(m_sInterfaceName + " interface startup failed", e);
  }

}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Interface::StopWork()
{
  StopSession();
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Interface::CleanWork()
{
}

