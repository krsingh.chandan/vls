/*******************************************************************************
* manager_backwardcomp.cpp: Vls manager backward compatibilty functions
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id$
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*          Damien Lucas <damien.lucas@anevia.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifdef EXTRA_INIT
  AddFunction("browse",            &C_Manager::Browse);
  AddFunction("show",              &C_Manager::Show);
  AddFunction("program",           &C_Manager::Program);
  AddFunction("input",             &C_Manager::Input);
  AddFunction("channel",           &C_Manager::Channel);
  AddFunction("start",             &C_Manager::Start);
  AddFunction("broadcast",         &C_Manager::Broadcast);
#endif /* EXTRA_INIT */

#ifdef EXTRA_DECL
    C_Answer Browse(C_Request& cRequest);
    C_Answer Show(C_Request& cRequest);
    C_Answer Program(C_Request& cRequest);
    C_Answer Input(C_Request& cRequest);
    C_Answer Channel(C_Request& cRequest);
    C_Answer Start(C_Request& cRequest);
    C_Answer Broadcast(C_Request& cRequest);
#endif /* EXTRA_DECL*/


#ifdef EXTRA_DEF
  // browse
  pCmdDesc = new C_CommandDesc(
              "browse",
              "gives the program list",
              "Called with no argument, \"browse\" gives all programs of "
              "inputs. Called with one argument it only gives the programs "
              "of the specified input. Each program is given with its status.");
  pStr = new C_String("input");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // show
  pCmdDesc = new C_CommandDesc(
              "show",
              "show broadcasts, channels, programs or inputs",
              "Called with one argument, \"show\" gives information about "
              "broadcasts or available channels, programs or inputs.");
  pStr = new C_String("command");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // program
  pCmdDesc = new C_CommandDesc(
              "program",
              "configure an input with programs.",
              "\"program\" adds a program definition to a specified input "
              "it becomes available for broadcast.");
  pStr = new C_String("name");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("input");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("filename");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("type");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("add");
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("delete");
  pCmdDesc->m_vBooleans.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // input
  pCmdDesc = new C_CommandDesc(
              "input",
              "configure a new input.",
              "\"input\" adds a input definition of type local, video or dvb."
              " it becomes available for broadcast and program configuration.");
  pStr = new C_String("name");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("type");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  // for type=local
  pStr = new C_String("dir");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  // for type=video
  pStr = new C_String("device");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("format");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  // for type=dvb
  pStr = new C_String("device_number");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("device_type");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("frequency");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("polarization");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("symbol_rate");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("use_disecq");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("lnb_lof1");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("lnb_lof2");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("lnb_slof");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("add");
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("delete");
  pCmdDesc->m_vBooleans.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // channel
  pCmdDesc = new C_CommandDesc(
              "channel",
              "configure a new channel.",
              "\"channel\" adds a channel definition. "
              "it becomes available for broadcast.");
  pStr = new C_String("name");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("type");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("dest_host");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("dest_port");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("streamtype");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("domain");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("ttl");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("interface");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("add");
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("delete");
  pCmdDesc->m_vBooleans.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // -- start <program> <channel> <input> [--loop] [--end] [--rtp]
  pCmdDesc = new C_CommandDesc(
              "start",
              "launches a program.",
              "\"start\" launches the specified program of the specified input "
              "and broadcasts it through the specified channel.");
  pStr = new C_String("program");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("channel");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("input");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("loop");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("end"); // jump to end of file
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("rtp");
  pCmdDesc->m_vBooleans.Add(pStr);
  pCmdDesc->m_bExtendedOptions = true;
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // -- broadcast <name> <channel> <program> <input> [--add] [--delete] [--test]
  pCmdDesc = new C_CommandDesc(
              "broadcast",
              "configure a new broadcast.",
              "\"broadcast\" adds a broadcast definition. "
              "it becomes available for streaming.");
  pStr = new C_String("name");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("channel");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("program");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("input");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("add");
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("delete");
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("test");
  pCmdDesc->m_vBooleans.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

#endif

#ifdef EXTRA_CODE
C_Answer C_Manager::Browse(C_Request&)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);
  cAnswer.AddMessage("Program Table");

  m_cProgramList.Lock();

  C_PgrmDirectoryBrowser cBrowser = m_cProgramList.CreateBrowser();

  while(cBrowser.HasNext())
  {
    C_PgrmInfo* pPgrmInfo = cBrowser.GetNext();
    ASSERT(pPgrmInfo);

    // Build a subanswer with the pgrm description and the list of providers
    const C_Program* pPgrm = pPgrmInfo->GetProgram();
    C_String strPgrmName = pPgrm->GetName();
    ASSERT(strPgrmName != "");
    C_Answer cPgrmDescr(strPgrmName);
    cPgrmDescr.SetStatus(NO_ERR);
    cPgrmDescr.AddMessage(pPgrm->GetDescription());

    const C_Vector<C_Input>& cProviders = pPgrmInfo->GetProviders();
    C_String strProviders = "Provided by";
    for(unsigned int i = 0; i < cProviders.Size(); i++)
    {
      strProviders += " " + cProviders[i].GetName();
    }
    cPgrmDescr.AddMessage(strProviders);

    // Add it to the answer
    cAnswer.Add(cPgrmDescr);
  }

  m_cProgramList.UnLock();

  return cAnswer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Show(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  C_String strCommand = cRequest.GetArg("command");
  ASSERT(strCommand != "");

  if (strCommand.ToLower() == "broadcast")
  {
    cAnswer = Broadcast_Ls(cRequest);
  }
  else if (strCommand.ToLower() == "channel")
  {
    cAnswer = Channel_Ls(cRequest);
  }
  else if (strCommand.ToLower() == "program")
  {
    cAnswer.AddMessage("programs");

    m_cProgramList.Lock();
    C_PgrmDirectoryBrowser cBrowser = m_cProgramList.CreateBrowser();
    while(cBrowser.HasNext())
    {
      C_PgrmInfo* pPgrmInfo = cBrowser.GetNext();
      ASSERT(pPgrmInfo);

      // Build a subanswer with the pgrm description and the list of providers
      const C_Program* pPgrm = pPgrmInfo->GetProgram();
      C_String strPgrmName = pPgrm->GetName();
      ASSERT(strPgrmName != "");
      C_Answer cPgrmDescr(strPgrmName);
      cPgrmDescr.SetStatus(NO_ERR);
      cPgrmDescr.AddMessage(pPgrm->GetDescription());

      //TODO - What about the optional args ???
      // nitrox
      const C_Vector<C_Input>& cProviders = pPgrmInfo->GetProviders();
      C_String strProviders;
      for(unsigned int i = 0; i < cProviders.Size(); i++)
      {
        strProviders += "provider"+C_String(i)+" "+ cProviders[i].GetName();
      }
      cPgrmDescr.AddMessage(strProviders);

      // Add it to the answer
      cAnswer.Add(cPgrmDescr);
      cAnswer.SetStatus(ANS_NO_ERR);
    }
    m_cProgramList.UnLock();
  }
  else if (strCommand.ToLower() == "input")
  {
    cAnswer.AddMessage("inputs");

    // Lock the input repository
    m_cInputList.Lock();

    // Get the name of all the inputs
    C_Application* pApp = C_Application::GetApp();
    ASSERT(pApp);
    C_Vector<C_Setting> vInputs = pApp->GetSettings("inputs");

    // Create the corresponding inputs
    for(unsigned int i = 0; i < vInputs.Size(); i++)
    {
      // Get input name and type
      C_Setting cCurrentInput = vInputs[i];
      C_String strInputName = cCurrentInput.GetName();
      C_String strInputType = cCurrentInput.GetValue();


      // Get all the specific settings
      C_Vector<C_Setting> vSettings = pApp->GetSettings(strInputName);
      C_String strSettings="";
      for(unsigned int i = 0; i < vSettings.Size(); i++)
      {
        if(i!=0) strSettings += " ";
        strSettings += vSettings[i].GetName()+" "+ vSettings[i].GetValue();
      }

      // Create answer
      C_Answer cInputDescr(strInputName);
      cInputDescr.SetStatus(NO_ERR);
      if(strSettings!="")
      {
        cInputDescr.AddMessage("input "+strInputName+
                                       " type "+strInputType+" "+strSettings);
      }
      else
      {
        cInputDescr.AddMessage("input "+strInputName+
                                                       " type "+strInputType);
      }
      cAnswer.Add(cInputDescr);
    }
    cAnswer.SetStatus(ANS_NO_ERR);

    // Unlock the input repository
    m_cInputList.UnLock();
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("What to show? Use: broadcast, channel, program or input.");
  }
  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Start(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  // Get the mandatory arguments to create the broadcast
  C_String strChannel = cRequest.GetArg("channel");
  C_String strPgrm = cRequest.GetArg("program");
  C_String strInput = cRequest.GetArg("input");
  C_String strLoop = cRequest.GetArg("loop");
  cRequest.SetArg("loop", C_String("-1"));

  // Create the broadcast
  // Since we have just one broadcast per channel, we name it by default
  // as the channel name
  C_Broadcast* pBroadcast = CreateBroadcast (strChannel, strChannel,
                                                   strPgrm, strInput, &cAnswer);

  // Set optional arguments and start the streaming
  if(pBroadcast)
  {
    m_cBroadcastsList.Lock();
    pBroadcast->SetOptions(cRequest.GetArgs());

    C_Answer cInputAnswer = pBroadcast->GetInput()->StartStreaming(pBroadcast);
    m_cBroadcastsList.UnLock();
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Program(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strProgram = cRequest.GetArg("name");
  ASSERT(strProgram != "");

  C_String strFileName = cRequest.GetArg("filename");
  ASSERT(strFileName != "");

  C_String strType = cRequest.GetArg("type");
  ASSERT(strType != "");

  C_String strInput = cRequest.GetArg("input");
  C_Input* pInput = m_cInputList.Get(strInput);
  if(!pInput)
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("input doesn't exist");
    return cAnswer;
  }

  // optional arguments
  C_String strAdd = cRequest.GetArg("add");
  C_String strDelete = cRequest.GetArg("delete");

  if ((strAdd == "1") && (strDelete == "1"))
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Channel does not allow use of --add and --delete at the same time.");
    return cAnswer;
  }

  if (strAdd == "1") // Add Program
  {
    printf("Adding program name:%s\n", strProgram.GetString());
    // Check if program is already present
    const C_Program* pProgram = m_cProgramList.GetPgrm(strProgram);
    if(pProgram)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Ignoring command program already added.");
      m_cProgramList.ReleasePgrm(pProgram->GetName());
      return cAnswer;
    }

    // Lock the input repository and the program list
    m_cProgramList.Lock();
    m_cInputList.Lock();

    // First update C_Settings in pInput
    C_Answer cInputAnswer = pInput->UpdateProgram(strProgram,strFileName,strType);
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
    }
    cAnswer.Add(cInputAnswer);

    // Then Update m_cProgramList
    C_List<C_Program> cInputPgrms = pInput->GetAvailablePgrms();
    unsigned int iPgrmNumber = cInputPgrms.Size();
    if (iPgrmNumber>0)
      m_cProgramList.Add(cInputPgrms[iPgrmNumber-1], pInput);
    else
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("ProgramList is empty.");
    }
    // debug
    Log(m_hLog, LOG_NOTE, "Program configuration changed.");
    for(unsigned int j = 0; j < iPgrmNumber; j++)
    {
       Log(m_hLog, LOG_NOTE, "Program: " + cInputPgrms[j].GetName());
    }
    // Unlock the input repository and the program list
    m_cInputList.UnLock();
    m_cProgramList.UnLock();
  }
  else
  if (strDelete == "1") // Delete Program
  {
    Log(m_hLog, LOG_NOTE, "Program being deleted.");
    // Check if program is known
    const C_Program* pProgram = m_cProgramList.GetPgrm(strProgram);
    if(!pProgram)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Cannot delete unknown program.");
      return cAnswer;
    }
    m_cProgramList.ReleasePgrm(pProgram->GetName());

    // Lock the input repository and the program list
    m_cProgramList.Lock();
    m_cInputList.Lock();

    // First update C_Settings in pInput
    C_Answer cInputAnswer = pInput->DeleteProgram(strProgram);
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
      cAnswer.AddMessage("Error removing program from input.");
      m_cProgramList.UnLock();
      m_cInputList.UnLock();
      return cAnswer;
    }
    cAnswer.Add(cInputAnswer);

    // Then Delete from m_cProgramList
    if(!cInputAnswer.GetStatus())
    {
      int iRc = m_cProgramList.Remove(*pProgram, pInput);
      if(iRc == GEN_ERR)
      {
        cAnswer.SetStatus(GEN_ERR);
        cAnswer.AddMessage("Error removing program from configuration.");
        m_cProgramList.UnLock();
        m_cInputList.UnLock();
        return cAnswer;
      }
    }
    // Unlock the input repository and the program list
    m_cInputList.UnLock();
    m_cProgramList.UnLock();

    // debug
    Log(m_hLog, LOG_NOTE, "Program configuration changed.");
    C_List<C_Program> cDebugInputPgrms = pInput->GetAvailablePgrms();
    unsigned int iPgrmNumber = cDebugInputPgrms.Size();
    if (iPgrmNumber <= 0)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("ProgramList is empty.");
    }
    for(unsigned int j = 0; j < iPgrmNumber; j++)
    {
      Log(m_hLog, LOG_NOTE, "Program: " + cDebugInputPgrms[j].GetName());
    }
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Commaned expects --add or --delete.");
    Log(m_hLog, LOG_ERROR, "Command Syntax Error.");
  }
  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Channel(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strAdd = cRequest.GetArg("add");
  C_String strDelete = cRequest.GetArg("delete");

  if ((strAdd == "1") && (strDelete == "1"))
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Channel does not allow use of --add and --delete at the same time.");
    return cAnswer;
  }

  if (strAdd == "1") // Add Channel
  {
      Channel_Add(cRequest);
  }
  else
  if (strDelete == "1") // Delete Channel
  {
      Channel_Del(cRequest);
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Command expects --add or --delete.");
    Log(m_hLog, LOG_ERROR, "Command Syntax Error.");
  }
  return cAnswer;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Broadcast(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strAdd = cRequest.GetArg("add");

  // Add case
  if(strAdd ==  "1")
  {
    Broadcast_Add(cRequest);
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Manager::Input(C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strInputName = cRequest.GetArg("name");
  ASSERT( strInputName != "" );

  C_String strInputType = cRequest.GetArg("type");
  ASSERT( strInputType != "" );

  // for type=local
  C_String strConfigPath;
  if (strInputType == "local")
  {
    strConfigPath = cRequest.GetArg("dir");
    ASSERT( strConfigPath != "" );
    if (strConfigPath == "")
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage( "Input type \"local\" expects \"config path\" to input.cfg file." );
    }
  }
  // for type=video
  C_String strDevice;
  C_String strFormat;
  if (strInputType == "video")
  {
    strDevice = cRequest.GetArg("device");
    ASSERT( strDevice != "" );
    if (strDevice == "")
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage( "Input type \"video\" expects \"device\"." );
    }
    strFormat = cRequest.GetArg("format");
    ASSERT( strFormat != "" );
    if (strFormat == "")
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage( "Input type \"video\" expects \"stream format type\" (Mpeg1-PS, Mpeg2-PS, Mpeg2-TS)." );
    }
  }
  // for type=dvb
  C_String strDeviceNumber;
  C_String strDeviceType;
  C_String strFrequency;
  C_String strPolarization;
  C_String strSymbolRate;
  C_String strUseDiSEqC;
  C_String strLNB_Lof1;
  C_String strLNB_Lof2;
  C_String strLNB_SLof;
  if (strInputType == "dvb")
  {
    strDeviceNumber = cRequest.GetArg("device_number");
    ASSERT( strDeviceNumber != "" );
    if (strDeviceNumber == "")
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage( "Input type \"dvb\" expects \"device number\"." );
    }
  }
  // Check for errors in command syntax
  if (cAnswer.GetStatus() == GEN_ERR)
  {
    Log(m_hLog, LOG_ERROR, "Input Command Syntax Error.");
    cAnswer.AddMessage( "Input Command Syntax Error." );
    return cAnswer;
  }

  // Optional arguments
  C_String strAdd = cRequest.GetArg("add");
  C_String strDelete = cRequest.GetArg("delete");
  if ((strAdd == "1") && (strDelete == "1"))
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Input does not allow use of --add and --delete at the same time.");
    Log(m_hLog, LOG_ERROR, "Input Command Syntax Error.");
    return cAnswer;
  }

  // Get the names of all the sources
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);

  // Process input commands
  if (strAdd == "1")
  {
    Log(m_hLog, LOG_NOTE,"Adding input: \"" +strInputName+ "\"");

    // check if input is already known
    C_Input* pInput = m_cInputList.Get(strInputName);
    if(pInput)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Input already present");
      m_cInputList.Release(strInputName);
      return cAnswer;
    }

    // Lock the input repository
    m_cInputList.Lock();

    C_String strKey = C_String("inputs.") + strInputName;
    pApp->SetSettings( strKey, strInputType );
    // for type=local
    if (strInputType == "local")
    {
      strKey = strInputName+C_String(".configpath");
      pApp->SetSettings( strKey, strConfigPath );
    }
    else // for type=video
    if (strInputType == "video")
    {
      strKey = strInputName+C_String(".device");
      pApp->SetSettings( strKey, strDevice );
      strKey = strInputName+C_String(".type");
      pApp->SetSettings( strKey, strFormat );
    }
    else // for type=dvb
    if (strInputType == "dvb")
    {
      strKey = strInputName+C_String(".devicenumber");
      pApp->SetSettings( strKey, strDeviceNumber );
    }

    // Create the input for the given source
    C_InputModule* pModule = (C_InputModule*)
                             C_Application::GetModuleManager()
                                        ->GetModule("input", strInputType);
    if(pModule)
    {
      C_Input* pNewInput = NULL;

      pNewInput = pModule->NewInput(strInputName);
      ASSERT(pNewInput);
      pNewInput->SetEventHandler(this);
      cAnswer.AddMessage("Initializing input '"+strInputName+"'");
      try
      {
        // Initialize the input
        pNewInput->Init();

        // And register it
        m_cInputList.Add(strInputName, pNewInput);

        cAnswer.AddMessage("Input '" + strInputName + "' sucessfully initialised");
        Log(m_hLog, LOG_NOTE,"Adding input: \"" +strInputName+ "\" succeeded.");
      }
      catch(E_Exception e)
      {
        cAnswer.SetStatus(GEN_ERR);
        cAnswer.AddMessage("Unable to add input '" + strInputName + "': " + e.Dump());
        Log(m_hLog, LOG_ERROR,"Adding input: \"" +strInputName+ "\" failed.");
        delete pNewInput;
      }
    }
    else
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Input type \"" + strInputType + "\" invalid");
      Log(m_hLog, LOG_ERROR,"Input type: \"" +strInputName+ "\" invalid");
    }
    // Unlock the input repository
    m_cInputList.UnLock();
  }
  else if (strDelete == "1")
  {
    Log(m_hLog, LOG_NOTE,"Deleting input: \"" +strInputName+ "\"");

    // check if input is already known
    C_Input* pInput = m_cInputList.Get(strInputName);
    if(!pInput)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Input unknown present");
      return cAnswer;
    }
    else if(!pInput->IsFree())
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Input is busy");
      pInput = NULL;
      m_cInputList.Release(strInputName);
      return cAnswer;
    }
    m_cInputList.Release(strInputName);

    // Lock the input repository
    m_cInputList.Lock();

    // So now, we can remove input from all the lists
    int iRc = m_cInputList.Remove(strInputName);
    if (iRc)
    {
      Log(m_hLog, LOG_ERROR, "Input '" + strInputName + "' can not be deleted !");
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Input can not be deleted");
    }
    else
    {
      C_String strKey = C_String("inputs.")+strInputName;
      if (pApp->GetSetting(strKey, "") != "")
        pApp->DeleteSetting(strKey);
      // for type=local
      if (strInputType == "local")
      {
        strKey = strInputName+C_String(".configpath");
        if (pApp->GetSetting(strKey, "") != "")
          pApp->DeleteSetting(strKey);
      }
      else // for type=video
      if (strInputType == "video")
      {
        strKey = strInputName+C_String(".device");
        if (pApp->GetSetting(strKey, "") != "")
          pApp->DeleteSetting(strKey);
        strKey = strInputName+C_String(".type");
        if (pApp->GetSetting(strKey, "") != "")
          pApp->DeleteSetting(strKey);
      }
      else // for type=dvb
      if (strInputType == "dvb")
      {
        strKey = strInputName+C_String(".devicenumber");
        if (pApp->GetSetting(strKey, "") != "")
          pApp->DeleteSetting(strKey);
      }
      Log(m_hLog, LOG_NOTE, "Input '" + strInputName + "' removed !");
    }
    m_cInputList.UnLock();
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Command expects --add or --delete.");
    Log(m_hLog, LOG_ERROR, "Command Syntax Error.");
  }
  return cAnswer;
}

#endif /*EXTRA_CODE*/
