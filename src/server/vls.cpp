/*******************************************************************************
* vls.cpp: vls main file
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: vls.cpp,v 1.18 2003/10/29 14:00:34 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* This file implements the application object and launches the program
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#ifdef GETPROTOBYNAME_IN_NETDB_H
#include <netdb.h>
#endif
#ifdef _WIN32
#include <winsock2.h>
#endif

#include "../core/core.h"

#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"

#include "program.h"
#include "buffer.h"
#include "output.h"
#include "channel.h"
#include "request.h"
#include "broadcast.h"
#include "input.h"
#include "repository.h"
#include "directory.h"
#include "admin.h"
#include "manager.h"

#ifndef  _WIN32
	#include "daemon.h"
#endif
#include "vls.h"

#include "repository.cpp"

#ifdef GETPROTOBYNAME_IN_NETDB_H
#include <netdb.h>
#endif


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Vls::C_Vls(const C_String& strName) : C_Application(strName)
{
  m_pManager = NULL;
  m_pAdministrator = NULL;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_Vls::~C_Vls()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_Vls::OnAppInit()
{
  // Startup message
  fprintf(stderr, "%s\n", COPYRIGHT_MESSAGE);

  // Check environnement
  int iRc = CheckEnvir();

#ifdef _WIN32
  // Initialize Winsock
  WSADATA data;
  int version = (u16)((u8)1 | (u16((u8)(1))) << 8);

  iRc = WSAStartup(version, &data);
  if (iRc)
  {
     Log(m_hLog, LOG_NOTE, "Unable to load Winsock 1.1");
  }
#endif

  if(!iRc)
  {
    LoadBuiltins();
    // Parse these in case VLS wasn't installed
    m_pModuleManager->BrowseDirectory("./src/modules");
    m_pModuleManager->BrowseDirectory("./modules");
    // The default installed plugins
    m_pModuleManager->BrowseDirectory(PLUGIN_PATH);
  }

  // Init the manager
  if(!iRc)
  {
    m_pManager = new C_Manager(m_hLog, this);
    iRc = m_pManager->Init(); 
  }

  // Init the administrator
  if(!iRc)
  {
    m_pAdministrator = new C_Admin(m_hLog, this);
    iRc = m_pAdministrator->Init();
  }

  return iRc;
}


//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
int C_Vls::OnAppRun()
{
  // Run the administrator
    int iRc = m_pAdministrator->Run();

  // Run the manager
  if(!iRc)
    iRc = m_pManager->Run();

  return iRc;
}


//------------------------------------------------------------------------------
// Stop the execution
//------------------------------------------------------------------------------
int C_Vls::OnAppExit()
{
  int iRc = NO_ERR;

  if(m_pManager)
    iRc = m_pManager->Stop();

#ifdef _WIN32
  WSACleanup();
#endif

  return iRc;
}


//------------------------------------------------------------------------------
// Stop the execution
//------------------------------------------------------------------------------
int C_Vls::SafeStop()
{
  int iRc = NO_ERR;

  if(m_pAdministrator)
  {
    m_pAdministrator->DisableRequests();
    iRc = m_pAdministrator->Stop();
  }

  return iRc;
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
int C_Vls::OnAppDestroy()
{
  int iRc = NO_ERR;

  // Destroy the administrator if any
  if(m_pAdministrator)
  {
    Log(m_hLog, LOG_NOTE, "Destroying administrator module");
    iRc = m_pAdministrator->Destroy();
    delete m_pAdministrator;
    m_pAdministrator = NULL;
  }

  // Destroy the manager if any
  if(m_pManager)
  {
    Log(m_hLog, LOG_NOTE, "Destroying manager module");
    iRc |= m_pManager->Destroy();
    delete m_pManager;
    m_pManager = NULL;
  }

  return iRc;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Vls::ForwardRequest(C_Request& cRequest)
{
  // For now, request only come from the admin and go to the manager: simply
  // forward the request to the manager
  ASSERT(m_pManager);
  return m_pManager->HandleRequest(cRequest);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Vls::ForwardEvent(const C_Event& cEvent)
{
  // For now, events only come from the manager and go to the admin: simply
  // forward the request to the admin
  ASSERT(m_pAdministrator);
  m_pAdministrator->HandleEvent(cEvent);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Vls::CheckEnvir()
{
  int iRc = 0;

#ifdef GETPROTOBYNAME_IN_NETDB_H
  if(!getprotobyname("tcp"))
  {
    Log(m_hLog, LOG_NOTE, "Unknown protocol: TCP");
    iRc = GEN_ERR;
  }

  else if(!getprotobyname("udp"))
  {
    Log(m_hLog, LOG_NOTE, "Unknown protocol: UDP");
    iRc = GEN_ERR;
  }

#elif defined WIN32
  // TO DO

#endif

  return iRc;
}



//------------------------------------------------------------------------------
// Usage
//------------------------------------------------------------------------------
void C_Vls::Usage(const C_String& strProgName)
{
#define HELP ""                         \
"Usage : %s\n"                          \
"[-c <cfg>]: Config file\n"

  printf(HELP, strProgName.GetString());
}


#include "vls_builtins.cpp"



//------------------------------------------------------------------------------
// Main function
//------------------------------------------------------------------------------
int main(int iArgc, char* paArgv[])
{
  int iRc = NO_ERR;

#ifdef DEBUG
  try
  {
#endif
    C_Vls cApplication("Vls");

#ifndef  _WIN32
    // if application is started as "vlsd" then run as daemon process.
    // Do this before opening any files or terminals.
    C_String name(paArgv[0]);
    if (name.EndsWith("vlsd"))
    {
       if (cApplication.StartDaemon() < 0)
          printf("Error could not start as deamon\n");
    }
#endif

    // Init the application
    iRc = cApplication.Init(iArgc, paArgv);
  
    // Start the program unless something went wrong during the initialisation
    if (!iRc)
      iRc = cApplication.Run();

    // Safe stop executed by the main thread
    iRc |= cApplication.SafeStop();

    // Free all ressources used by the program
    iRc |= cApplication.Destroy();

#ifndef _WIN32
    // Cleanup Daemon process
    // after unclean shutdown
    if (iRc)
    {
      if (cApplication.isDaemon())
        cApplication.StopDaemon();
    }
#endif

#ifdef DEBUG
  }
  catch(E_Exception e)
  {
    printf("Unhandled Exception : %s\n", e.Dump().GetString());
  }
#endif

  return iRc<0 ? iRc:0;
}

