/*******************************************************************************
* manager_broadcast.cpp: Brodacast mtehods for Vls manager
*-------------------------------------------------------------------------------
* (c)1999-2003 VideoLAN
* $Id$
*
* Authors: Damien Lucas <damien.lucas@anevia.com>
*          Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


C_Answer C_Manager::Program_Ls(C_Request&)
{
  C_Answer cAnswer("Manager");
  cAnswer.AddMessage("programs");

  m_cProgramList.Lock();
  C_PgrmDirectoryBrowser cBrowser = m_cProgramList.CreateBrowser();
  while(cBrowser.HasNext())
  {
    C_PgrmInfo* pPgrmInfo = cBrowser.GetNext();
    ASSERT(pPgrmInfo);

    // Build a subanswer with the pgrm description and the list of providers
    const C_Program* pPgrm = pPgrmInfo->GetProgram();
    C_String strPgrmName = pPgrm->GetName();
    ASSERT(strPgrmName != "");
    C_Answer cPgrmDescr(strPgrmName);
    cPgrmDescr.SetStatus(NO_ERR);
    cPgrmDescr.AddMessage(pPgrm->GetDescription());

    //TODO - What about the optional args ???
    // nitrox
    const C_Vector<C_Input>& cProviders = pPgrmInfo->GetProviders();
    C_String strProviders;
    for(unsigned int i = 0; i < cProviders.Size(); i++)
    {
      strProviders += "provider"+C_String(i)+" "+ cProviders[i].GetName();
    }
    cPgrmDescr.AddMessage(strProviders);

    // Add it to the answer
    cAnswer.Add(cPgrmDescr);
    cAnswer.SetStatus(ANS_NO_ERR);
  }
  m_cProgramList.UnLock();
  return cAnswer;
}
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
#if 0
C_Answer C_Manager::Program(const C_Request& cRequest)
{
  C_Answer cAnswer("Manager");
  cAnswer.SetStatus(ANS_NO_ERR);

  C_String strProgram = cRequest.GetArg("name");
  ASSERT(strProgram != "");

  C_String strFileName = cRequest.GetArg("filename");
  ASSERT(strFileName != "");

  C_String strType = cRequest.GetArg("type");
  ASSERT(strType != "");

  C_String strInput = cRequest.GetArg("input");
  C_Input* pInput = m_cInputList.Get(strInput);
  if(!pInput)
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("input doesn't exist");
    return cAnswer;
  }

  // optional arguments
  C_String strAdd = cRequest.GetArg("add");
  C_String strDelete = cRequest.GetArg("delete");

  if ((strAdd == "1") && (strDelete == "1"))
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Channel does not allow use of --add and --delete at the same time.");
    return cAnswer;
  }

  if (strAdd == "1") // Add Program
  {
    printf("Adding program name:%s\n", strProgram.GetString());
    // Check if program is already present
    const C_Program* pProgram = m_cProgramList.GetPgrm(strProgram);
    if(pProgram)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Ignoring command program already added.");
      m_cProgramList.ReleasePgrm(pProgram->GetName());
      return cAnswer;
    }

    // Lock the input repository and the program list
    m_cProgramList.Lock();
    m_cInputList.Lock();

    // First update C_Settings in pInput
    C_Answer cInputAnswer = pInput->UpdateProgram(strProgram,strFileName,strType);
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
    }
    cAnswer.Add(cInputAnswer);

    // Then Update m_cProgramList
    C_List<C_Program> cInputPgrms = pInput->GetAvailablePgrms();
    unsigned int iPgrmNumber = cInputPgrms.Size();
    if (iPgrmNumber>0)
      m_cProgramList.Add(cInputPgrms[iPgrmNumber-1], pInput);
    else
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("ProgramList is empty.");
    }
    // debug
    Log(m_hLog, LOG_NOTE, "Program configuration changed.");
    for(unsigned int j = 0; j < iPgrmNumber; j++)
    {
       Log(m_hLog, LOG_NOTE, "Program: " + cInputPgrms[j].GetName());
    }
    // Unlock the input repository and the program list
    m_cInputList.UnLock();
    m_cProgramList.UnLock();
  }
  else
  if (strDelete == "1") // Delete Program
  {
    Log(m_hLog, LOG_NOTE, "Program being deleted.");
    // Check if program is known
    const C_Program* pProgram = m_cProgramList.GetPgrm(strProgram);
    if(!pProgram)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("Cannot delete unknown program.");
      return cAnswer;
    }
    m_cProgramList.ReleasePgrm(pProgram->GetName());

    // Lock the input repository and the program list
    m_cProgramList.Lock();
    m_cInputList.Lock();

    // First update C_Settings in pInput
    C_Answer cInputAnswer = pInput->DeleteProgram(strProgram);
    if(cInputAnswer.GetStatus())
    {
      cAnswer.SetStatus(cInputAnswer.GetStatus());
      cAnswer.AddMessage("Error removing program from input.");
      m_cProgramList.UnLock();
      m_cInputList.UnLock();
      return cAnswer;
    }
    cAnswer.Add(cInputAnswer);

    // Then Delete from m_cProgramList
    if(!cInputAnswer.GetStatus())
    {
      int iRc = m_cProgramList.Remove(*pProgram, pInput);
      if(iRc == GEN_ERR)
      {
        cAnswer.SetStatus(GEN_ERR);
        cAnswer.AddMessage("Error removing program from configuration.");
        m_cProgramList.UnLock();
        m_cInputList.UnLock();
        return cAnswer;
      }
    }
    // Unlock the input repository and the program list
    m_cInputList.UnLock();
    m_cProgramList.UnLock();

    // debug
    Log(m_hLog, LOG_NOTE, "Program configuration changed.");
    C_List<C_Program> cDebugInputPgrms = pInput->GetAvailablePgrms();
    unsigned int iPgrmNumber = cDebugInputPgrms.Size();
    if (iPgrmNumber <= 0)
    {
      cAnswer.SetStatus(GEN_ERR);
      cAnswer.AddMessage("ProgramList is empty.");
    }
    for(unsigned int j = 0; j < iPgrmNumber; j++)
    {
      Log(m_hLog, LOG_NOTE, "Program: " + cDebugInputPgrms[j].GetName());
    }
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Commaned expects --add or --delete.");
    Log(m_hLog, LOG_ERROR, "Command Syntax Error.");
  }
  return cAnswer;
}
#endif
