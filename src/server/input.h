/*******************************************************************************
* input.h: Input class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: input.h,v 1.10 2003/09/10 12:57:52 nitrox Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _INPUT_H_
#define _INPUT_H_


// Events
#define EOF_EVENT              0xa00
#define CONNEXION_LOST_EVENT   0xa10


//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
class C_Input;


//------------------------------------------------------------------------------
// E_Input
//------------------------------------------------------------------------------
class E_Input : public E_Exception
{
 public:
  E_Input(const C_String& strInputName, const C_String& strMsg);
  E_Input(const C_String& strInputName, const C_String& strMsg,
          const E_Exception& eException);
};


//------------------------------------------------------------------------------
// C_Input
//------------------------------------------------------------------------------
// Provides a global framework to develop specific inputs such as the FileInput.
// The base mechanisms are completely thread safe, so that the inputs can be
// threaded without having to take care of concurrency problems.
// An input has to handle the OnXXX calls. It has as its disposal 2 thread safe
// callbacks to handle pgrm table updates and end of streaming
//------------------------------------------------------------------------------
class C_Input
{
  //friend class C_PgrmStopper;

 public:
  // Construction / Destruction
  C_Input(C_Module* pModule, const C_String& strName);
  C_Input(const C_Input& cInput);
  virtual ~C_Input();

  void SetEventHandler(C_EventHandler* pEventHandler);

  // Initialisation/termination
  void Init();
  void Destroy();

  // Streaming control
  C_Answer StartStreaming(C_Broadcast* pBroadcast);
  C_Answer ResumeStreaming(C_Broadcast* pBroadcast);
  C_Answer SuspendStreaming(C_Broadcast* pBroadcast);
  C_Answer ForwardStreaming(C_Broadcast* pBroadcast, int speed);
  C_Answer RewindStreaming(C_Broadcast* pBroadcast, int speed);
  C_Answer StopStreaming(C_Broadcast* pBroadcast);
  C_Answer UpdateProgram(C_String strProgram, C_String strFileName, C_String strType);
  C_Answer DeleteProgram(C_String strProgram);

  // List of available programs
  C_List<C_Program> GetAvailablePgrms();

  // Is input already used ?
  bool IsFree() const
  { return m_bIsFree; }

  // Misc
  const C_String& GetName() const;
  virtual C_String GetType() = 0;
  bool operator == (const C_Input& cInput) const;
  //virtual C_Input* Clone() const = 0;

 protected:
  // Child initialisation/termination
  virtual void OnInit() = 0;
  virtual void OnDestroy() = 0;
  // Child program reception
  virtual void OnStartStreaming(C_Broadcast* pBroadcast) = 0;
  virtual void OnResumeStreaming(C_Broadcast* pBroadcast) = 0;
  virtual void OnSuspendStreaming(C_Broadcast* pBroadcast) = 0;
  virtual void OnForwardStreaming(C_Broadcast* pBroadcast, int speed) = 0;
  virtual void OnRewindStreaming(C_Broadcast* pBroadcast, int speed) = 0;
  virtual void OnStopStreaming(C_Broadcast* pBroadcast) = 0;
  virtual void OnUpdateProgram(C_String strProgram, C_String strFileName,
                    C_String strType) = 0;
  virtual void OnDeleteProgram(C_String strProgram) = 0;
  // Child program table browsing
  virtual C_List<C_Program> OnGetAvailablePgrms() = 0;
  // Input internal failure management
  //virtual void OnFailure(int iError) = 0;
  //virtual void OnRecovery() = 0;

  // Event notifications. Should only be used by inputs internal components
  // Event are EndOfStream, ProgramTableUpdate, Failure, Recovery
  void SendEvent(const C_Event& cEvent);

 protected:
  // Log endpoint
  handle m_hLog;
  bool m_bIsFree;
  C_EventHandler* m_pEventHandler;

  // List of programs
  //C_PgrmDirectory m_cDirectory;
  // Program stopper
  //C_PgrmStopper m_cPgrmStopper;

 private:
  // Input name
  const C_String m_strName;

  // Parent module
  C_Module* m_pModule;

  // Input status
  int m_iStatus;
  C_String m_strErrorMsg;

  // List of outputs
  //C_HashTable<u16, C_Output> cOutputList;
  // Default output configuration
  //C_OutputCfg* m_pOutputCfg;
};


// Declaration and implementation of C_InputModule which has to be
// inherited by each input module => C_????InputModule
DECLARE_VIRTUAL_MODULE(Input, "input", const C_String&);


#else
#error "Multiple inclusions of input.h"
#endif

