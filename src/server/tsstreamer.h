/*******************************************************************************
* tsstreamer.h:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: tsstreamer.h,v 1.6 2004/01/21 14:18:21 adq Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _TSSTREAMER_H_
#define _TSSTREAMER_H_


//------------------------------------------------------------------------------
// C_TsStreamer
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
class C_TsStreamer : public C_Thread
{
 public:
  C_TsStreamer(handle hLog, C_Broadcast* pBroadcast,
               C_NetList* pTsProvider, C_SyncFifo* pBuffer,
               C_EventHandler* pEventHandler,
               bool m_bOwnProvider, bool m_bUsePcr);
  ~C_TsStreamer();

 protected:
  // Thread internal implementation
  virtual void InitWork();
  virtual void DoWork();
  virtual void StopWork();
  virtual void CleanWork();

 private:
  // void Exit(int iRc);
  inline void AdjustClock(C_TsPacket* pPacket);
  inline void InitClock(C_TsPacket* pPacket);
  inline void WaitSendDate(C_TsPacket* pPacket);
  inline s64 GetDate();

  bool InterpolateSlope(s64 iPCRTime);

  bool m_bStop;
  bool m_bFirstPCR;

  handle m_hLog;
  C_Broadcast* m_pBroadcast;
  C_NetList* m_pTsProvider;
  C_SyncFifo* m_pBuffer;
  C_Output* m_pOutput;

  C_EventHandler* m_pEventHandler;

  bool m_bOwnProvider;
  bool m_bUsePcr;

  u64 m_uiByteRead;
  s64 m_iLastTime;
  s64 m_iNextPCRTime;
  s64 m_iDeltaClock;
  double m_dSlope;

  unsigned int m_iHowMany;
};

#else
#error "Multiple inclusions of tsstreamer.h"
#endif

