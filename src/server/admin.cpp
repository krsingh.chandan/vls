/*******************************************************************************
* admin.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: admin.cpp,v 1.35 2003/10/30 20:45:06 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

//temporary, for kill
#include <signal.h>

#ifdef HAVE_CRYPT_H
#include <crypt.h>
#endif

#include "../core/core.h"
#include "../core/network.h"
#include "request.h"
#include "repository.h"
#include "admin.h"
#include "interface.h"

#include "../core/network.cpp"

#include "repository.cpp"

/*******************************************************************************
* C_C_CommandDesc class
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_CommandDesc::C_CommandDesc(const C_String& strName,
                             const C_String& strHelp,
                             const C_String& strLongHelp) :
                     m_strName(strName),
                     m_strHelp(strHelp),
                     m_strLongHelp(strLongHelp)
{
  m_bExtendedOptions = false;
}


//------------------------------------------------------------------------------
// Usage's auto-build from the current description
//------------------------------------------------------------------------------
void C_CommandDesc::BuildUsage()
{
  m_strUsage = "Usage: " + m_strName;

  unsigned int ui;

  for(ui = 0; ui < m_vMandatoryArgs.Size(); ui++)
    m_strUsage += " <" + m_vMandatoryArgs[ui] + ">";

  for(ui = 0; ui < m_vOptionalArgs.Size(); ui++)
    m_strUsage += " [" + m_vOptionalArgs[ui] + "]";

  for(ui = 0; ui < m_vOptions.Size(); ui++)
    m_strUsage += " [--" + m_vOptions[ui] + " value]";

  for(ui = 0; ui < m_vBooleans.Size(); ui++)
    m_strUsage += " [--" + m_vBooleans[ui] + "]";

  if(m_bExtendedOptions)
    m_strUsage += " [--... value] [--...]";
}


/*******************************************************************************
* C_AdminGroup class
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_AdminGroup::C_AdminGroup(const C_String& strName) : m_strName(strName)
{
}


/*******************************************************************************
* C_AdminUser class
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_AdminUser::C_AdminUser(const C_String& strLogin,
                         const C_String& strEncPasswd,
                         C_AdminGroup* pGroup) : m_strLogin(strLogin),
                                                 m_strEncPasswd(strEncPasswd)
{
  m_pGroup = pGroup;
}


/*******************************************************************************
* C_AdminSession class
********************************************************************************
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_AdminSession::C_AdminSession(C_Admin* pAdmin)
{
  ASSERT(pAdmin);

  m_pAdmin = pAdmin;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_AdminSession::~C_AdminSession()
{
  // Does nothing but has to be virtual
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AdminSession::Init()
{
  OnInit();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_AdminSession::Close()
{
  OnClose();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_AdminSession::Authenticate(const C_String& strLogin,
                                   const C_String& strPasswd)
{
  return m_pAdmin->Authenticate(this, strLogin, strPasswd);
}


/*******************************************************************************
* C_Admin class
********************************************************************************
*
*******************************************************************************/

          
//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Admin::C_Admin(handle hLogger, C_RequestHub* pRequestHub)
{
  ASSERT(hLogger);
  ASSERT(pRequestHub);

  m_hLog = hLogger;
  m_pRequestHub = pRequestHub;
  m_bRequestsEnabled = true;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_Admin::~C_Admin()
{
}


//------------------------------------------------------------------------------
// Initialization
//------------------------------------------------------------------------------
int C_Admin::Init()
{
  C_Application* pApp = C_Application::GetApp();
  int iRc = 0;

  // Commands' descriptions
  // -- help [command]
  C_CommandDesc* pCmdDesc = new C_CommandDesc("help",
        "gives help on the specified command.",
        "Called with no argument, \"help\" gives the list of all the commands"
                " (available or not). Called with one argument it gives"
                " details about how to use the specified command.");
  C_String* pStr = new C_String("command");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // cd (interface specific command)
  pCmdDesc = new C_CommandDesc("cd",
        "change directories.",
        "Called with one argument, \"cd\" changes the current directory.");
  pStr = new C_String("path");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- ls
  pCmdDesc = new C_CommandDesc("ls",
        "gives a global overview of the configuration state.",
        "gives a global overview of the configuration state.");
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- config <filename> [--load] [--save]
  pCmdDesc = new C_CommandDesc("config",
        "load and save configuration file.",
        "\"config\" loads or saves a configuration to a specified file."
                " It stops all broadcasts before clearing the current configuration."
                " After load the new configuration becomes available for broadcast.");
  pStr = new C_String("filename");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("load");
  pCmdDesc->m_vBooleans.Add(pStr);
  pStr = new C_String("save");
  pCmdDesc->m_vBooleans.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- resume <channel>
  pCmdDesc = new C_CommandDesc("broadcast_resume",
        "resumes streaming.",
        "\"resume\" resumes the specified broadcast.");
  pStr = new C_String("broadcast");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- suspend <channel>
  pCmdDesc = new C_CommandDesc("broadcast_suspend",
        "suspends streaming.",
        "\"suspend\" suspends the specified broadcast.");
  pStr = new C_String("broadcast");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- forward <channel> <speed>
  pCmdDesc = new C_CommandDesc("broadcast_forward",
        "forwards streaming.",
        "\"forward\" forward the specified broadcast with the"
                " desired speed [0..N].");
  pStr = new C_String("broadcast");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("speed");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- rewind <channel> <speed>
  pCmdDesc = new C_CommandDesc("broadcast_rewind",
        "rewinds streaming.",
        "\"rewind\" rewind the specified broadcast with the"
                " desired speed [0..N].");
  pStr = new C_String("broadcast");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("speed");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- stop <channel>
  pCmdDesc = new C_CommandDesc("broadcast_stop",
        "stops a program.",
        "\"stop\" ends the specified broadcast.");
  pStr = new C_String("broadcast");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- shutdown
  pCmdDesc = new C_CommandDesc("shutdown",
        "stops the server.",
        "\"shutdown\" stops all the programs and then the VideoLAN Server"
                " stops.");
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- logout
  pCmdDesc = new C_CommandDesc("logout",
        "terminates the administration session.",
        "\"logout\" closes the current administration session and the remote"
                " connection.");
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

  // ******** BROADCAST LOW LEVEL COMMANDS ******
  // -- broadcast_ls
  pCmdDesc = new C_CommandDesc("broadcast_ls",
        "lists all the defined broadcasts and their states.",
        "\"broadcast_ls\" lists all the defined broadcasts and their states");
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- broadcast_add
    pCmdDesc = new C_CommandDesc("broadcast_add",
        "adds a new broadcast.",
        "\"broadcast_add\" adds a new broadcast to the list."
        "It becomes available for streaming\n");
    pStr = new C_String("name");
    pCmdDesc->m_vMandatoryArgs.Add(pStr);
    pStr = new C_String("channel");
    pCmdDesc->m_vMandatoryArgs.Add(pStr);
    pStr = new C_String("program");
    pCmdDesc->m_vMandatoryArgs.Add(pStr);
    pStr = new C_String("input");
    pCmdDesc->m_vMandatoryArgs.Add(pStr);
    pStr = new C_String("loop");
    pCmdDesc->m_vOptionalArgs.Add(pStr);
    pCmdDesc->BuildUsage();
    m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- broadcast_del
    pCmdDesc = new C_CommandDesc("broadcast_del",
        "deletes a configured broadcast.",
        "\"broadcast_del\" deletes a configures broadcast."
        "\n");
    pStr = new C_String("broadcast");
    pCmdDesc->m_vMandatoryArgs.Add(pStr);
    pStr = new C_String("force");
    pCmdDesc->m_vBooleans.Add(pStr);
    pCmdDesc->BuildUsage();
    m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- broadcast_help
    pCmdDesc = new C_CommandDesc("broadcast_help",
        "display the list of availables commands.",
        "\"broadcast_help\" displays help on the broadcast commands."
        "\n");
    pStr = new C_String("command");
    pCmdDesc->m_vOptionalArgs.Add(pStr);
    pCmdDesc->BuildUsage();
    m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // ******** CHANNEL LOW LEVEL COMMANDS ******
  // -- channel_ls
  pCmdDesc = new C_CommandDesc("channel_ls",
        "lists all the defined channels and their properties.",
        "\"channel_ls\" lists all the defined channels and their properties");
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- channel_add <name> <type> <dest_host> <dest_port> <streamtype>
  //                                                      [domain] [ttl] [intf]
  pCmdDesc = new C_CommandDesc("channel_add",
        "adds a new channel.",
        "\"add\" adds a channel definition."
                " it becomes available for broadcast.");
  pStr = new C_String("name");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("type");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("dest_host");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("dest_port");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("streamtype");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pStr = new C_String("domain");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("ttl");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pStr = new C_String("interface");
  pCmdDesc->m_vOptionalArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- broadcast_del
    pCmdDesc = new C_CommandDesc("channel_del",
        "deletes a configured channel.",
        "\"del\" deletes a configured channel."
        "\n");
    pStr = new C_String("name");
    pCmdDesc->m_vMandatoryArgs.Add(pStr);
    pCmdDesc->BuildUsage();
    m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- broadcast_start <broadcast>
  pCmdDesc = new C_CommandDesc("broadcast_start",
        "start a configured broadcast.",
        "\"broadcast_start\" starts a previously configured broadcast.");
  pStr = new C_String("broadcast");
  pCmdDesc->m_vMandatoryArgs.Add(pStr);
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);
  // -- program_ls
  pCmdDesc = new C_CommandDesc("program_ls",
        "lists all the defined programs and their properties.",
        "\"program_ls\" lists all the defined programs and their properties");
  pCmdDesc->BuildUsage();
  m_cCmdDescriptions.Add(pCmdDesc->m_strName, pCmdDesc);

#ifdef MANAGER_EXTRA
# define EXTRA_DEF
#  include MANAGER_EXTRA
#  undef EXTRA_DEF
#endif

  // Groups
  C_Vector<C_Setting> vGroups = pApp->GetSettings("groups");

  for(unsigned int ui = 0; ui < vGroups.Size(); ui++)
  {
    C_Setting cCurrent = vGroups[ui];
    C_String strGroupName = cCurrent.GetName();
    if(m_cGroups.Get(strGroupName))
    {
      Log(m_hLog, LOG_ERROR,
          "Admin group \"" + strGroupName + "\" already exists");
      iRc = GEN_ERR;
      break;
    }

    C_AdminGroup* pGroup = new C_AdminGroup(strGroupName);
    ASSERT(pGroup);

    C_String strCommands = cCurrent.GetValue();
    C_StringTokenizer cTokenizer(strCommands, '|');
    while(cTokenizer.HasMoreToken())
    {
      C_String strCmd = cTokenizer.NextToken();
      pCmdDesc = m_cCmdDescriptions.Get(strCmd);
      if(!pCmdDesc)
      {
        Log(m_hLog, LOG_ERROR, "Admin group \"" + strGroupName +
            "\" not valid (the command \"" + strCmd + "\" doesn't exist)");
        delete pGroup;
        iRc = GEN_ERR;
        break;
      }

      C_String* pstrCmd = new C_String(strCmd);
      pGroup->m_vCommands.Add(pstrCmd);
    }

    Log(m_hLog, LOG_NOTE, "New admin group \"" + strGroupName + "\" is ok");
    m_cGroups.Add(strGroupName, pGroup);
  }

  // Users
  if(!iRc)
  {
    C_Vector<C_Setting> vUsers = pApp->GetSettings("users");

    for(unsigned int ui = 0; ui < vUsers.Size(); ui++)
    {
      C_Setting cCurrent = vUsers[ui];
      C_String strUserLogin = cCurrent.GetName();
      if(m_cUsers.Get(strUserLogin))
      {
        Log(m_hLog, LOG_ERROR,
            "Admin user \"" + strUserLogin + "\" already exists");
        iRc = GEN_ERR;
        break;
      }

      C_String strUserInfo = cCurrent.GetValue();
      C_StringTokenizer cTokenizer(strUserInfo, ':');
      if(cTokenizer.CountTokens() != 2)
      {
        Log(m_hLog, LOG_ERROR, "Admin user \"" + strUserLogin + "\" not valid");
        iRc = GEN_ERR;
        break;
      }

      C_String strEncPasswd = cTokenizer.NextToken();
      C_String strGroup = cTokenizer.NextToken();

      C_AdminGroup* pGroup = m_cGroups.Get(strGroup);
      if(!pGroup)
      {
        Log(m_hLog, LOG_ERROR, "Admin user \"" + strUserLogin +
            "\" not valid (the group \"" + strGroup + "\" doesn't exists");
        iRc = GEN_ERR;
        break;
      }

      C_AdminUser* pUser = new C_AdminUser(strUserLogin, strEncPasswd, pGroup);
      ASSERT(pUser);

      Log(m_hLog, LOG_NOTE, "New admin user \"" + strUserLogin + "\" is ok");
      m_cUsers.Add(strUserLogin, pUser);
    }
  }

  InitInterfaces();

  // Automatic launch of commands
  C_Vector<C_Setting> vLaunch = pApp->GetSettings("launchonstartup");

  vLaunch.Sort();

  for(unsigned int i = 0; i < vLaunch.Size(); i++)
  {
    C_String strCmd   = vLaunch[i].GetValue();

    Log(m_hLog, LOG_NOTE,"Executing " +vLaunch[i].GetName()+ " : '"
             + strCmd +"'");

    C_Request cRequest("");

    // Get input name and type
    C_Answer cAnswer = ParseCmdLine(NULL,vLaunch[i].GetValue(),cRequest);

    if (cAnswer.GetStatus() == ADMIN_WELLFORMED_COMMAND)
        cAnswer = HandleRequest(cRequest);

    C_List<C_String> cMessageList = cAnswer.GetMessages();

    unsigned int iSize = cMessageList.Size();
    for(unsigned int i = 0; i < iSize; i++)
    {
      Log(m_hLog, LOG_WARN,cMessageList[i]);
    }

  }
  return iRc;
}


//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------
int C_Admin::Run()
{
  /*m_cInterfaceList.Lock();
  C_RepositoryBrowser<C_String, C_Interface> cIterator =
                                              m_cInterfaceList.CreateBrowser();
  m_cInterfaceList.UnLock();

  while(cIterator.HasNextItem())
  {
    C_Interface* pInterface = cIterator.GetNextItem();

    try
    {
      pInterface->Create();
      //pInterface->Run();
    }
    catch(E_Exception e)
    {
       C_String strInterfaceName = pInterface->GetName();
      Log(m_hLog, LOG_ERROR,"Unable to run  "+strInterfaceName);
    }
  }*/

  // Unlock the Interface repository
  //
  // hem, ... this part of code is never reached untill the shutdown
  // that's a bit late for unlocking, isn't it ?
  // -- nitrox
  // m_cInterfaceList.UnLock();

  return 0;
}


//------------------------------------------------------------------------------
// Stop the execution of the telnet daemon (main thread)
//------------------------------------------------------------------------------
int C_Admin::Stop()
{
    m_cInterfaceList.Lock();
    C_RepositoryBrowser<C_String, C_Interface> cIterator =
                                            m_cInterfaceList.CreateBrowser();

  while(cIterator.HasNextItem())
  {
    C_Interface* pInterface = cIterator.GetNextItem();
    C_String strInterfaceName = pInterface->GetName();

    try
    {
      pInterface->StopInterface();
      pInterface->Stop();
      //m_cInterfaceList.Remove(strInterfaceName); 
    }
    catch(E_Exception e)
    {
      Log(m_hLog, LOG_ERROR, "Unable to stop "+strInterfaceName);
    }
  }
  // Unlock the Interface repository
  m_cInterfaceList.UnLock();

  return NO_ERR;
}

//------------------------------------------------------------------------------
// Stop the execution of the remaining daemons
//------------------------------------------------------------------------------
int C_Admin::FullStop()
{
  return NO_ERR;
}


//------------------------------------------------------------------------------
// Destruction
//------------------------------------------------------------------------------
int C_Admin::Destroy()
{
  m_cInterfaceList.Lock();
  C_RepositoryBrowser<C_String, C_Interface> cIterator =
                                                m_cInterfaceList.CreateBrowser();

  while(cIterator.HasNextItem())
  {
    C_Interface* pInterface = cIterator.GetNextItem();

    try
    {
      pInterface->Destroy();
    }
    catch(E_Exception e)
    {
       C_String strInterfaceName = pInterface->GetName();
      Log(m_hLog, LOG_ERROR,"Unable to destroy  "+strInterfaceName);
    }
  }

  // Unlock the Interface repository
  m_cInterfaceList.UnLock();

  return NO_ERR;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
int C_Admin::Authenticate(C_AdminSession *pSession,
                          const C_String& strLogin,
                          const C_String& strPasswd)
{
  C_AdminUser* pUser = m_cUsers.Get(strLogin);
  if(!pUser)
  {
    Log(m_hLog, LOG_WARN,
        "Authentication failed for user \"" + strLogin + "\"");
    return GEN_ERR;
  }

#ifdef _WIN32
  C_String strCrypt(strPasswd);
#else
  C_String strCrypt(crypt(strPasswd.GetString(),
                          pUser->m_strEncPasswd.GetString()));
#endif
  if(strCrypt == pUser->m_strEncPasswd.GetString())
  {
    Log(m_hLog, LOG_NOTE,
        "User \"" + strLogin + "\" successfully authenticated");
    pSession->m_strUser = strLogin;
    pSession->m_strGroup = strLogin;
    pSession->m_vCommands = pUser->m_pGroup->m_vCommands;
    return NO_ERR;
  }

  Log(m_hLog, LOG_WARN,
      "Authentication failed for user \"" + strLogin + "\"");
  return GEN_ERR;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Admin::ParseCmdLine(C_AdminSession* pSession,
                               const C_String& strCmdLine,
                               C_Request& cRequest)
{
//  ASSERT(pSession);

  C_Answer cAnswer("Admin");

  // Hash the command line
  // #### to be replaced by a shell-like hash -- bozo
  C_Vector<C_String> vArgs;
  C_StringTokenizer cTokenizer(strCmdLine, ' ');
  while(cTokenizer.HasMoreToken())
  {
    C_String* pStr = new C_String(cTokenizer.NextToken());
    vArgs.Add(pStr);
  }

  // Empty command line ?
  if(vArgs.Size() == 0)
  {
    cAnswer.SetStatus(ADMIN_EMPTY_COMMAND);
    cAnswer.AddMessage(" empty command.");
    return cAnswer;
  }

  // Unknown command ?
  C_String strCmd = vArgs[0];
 
  C_CommandDesc* pCmdDesc = m_cCmdDescriptions.Get(strCmd);
  if(!pCmdDesc)
  {
    cAnswer.SetStatus(ADMIN_UNKNOWN_COMMAND);
    cAnswer.AddMessage(strCmd + ":    unknown command.");
    return cAnswer;
  }

  // The user is not allowed to run this command ?
  if (pSession != NULL)
    if((!m_bIsSuperUser) && pSession->m_vCommands.Find(strCmd) < 0)
    {
      cAnswer.SetStatus(ADMIN_COMMAND_DENIED);
      cAnswer.AddMessage(strCmd + ": permission denied.");
      return cAnswer;
    }

  // Command is OK, now parse the arguments
  C_Request cRq(strCmd);
  
  // Default boolean value is "0"
  unsigned int ui;
  for(ui = 0; ui < pCmdDesc->m_vBooleans.Size(); ui++)
    cRq.SetArg(pCmdDesc->m_vBooleans[ui], "0");

  int iError = 0;
  unsigned int uiMandatory = 0;
  unsigned int uiOptional = 0;

  for(ui = 1; ui < vArgs.Size(); ui++)
  {
    C_String strArg(vArgs[ui]);
    if(strArg.StartsWith("--"))
    {
      C_String strTmp(strArg.SubString(2, strArg.Length()));
      if(pCmdDesc->m_vOptions.Find(strTmp) >= 0)
      {
        // The argument is an option
        if(++ui < vArgs.Size())
        {
          cRq.SetArg(strTmp, vArgs[ui]);
        }
        else
        {
          // Missing value
          cAnswer.AddMessage(strCmd + ": missing value for option \"" +
                             strTmp + "\"");
          cAnswer.AddMessage(pCmdDesc->m_strUsage);
          iError = GEN_ERR;
          break;
        }
      }
      else if(pCmdDesc->m_vBooleans.Find(strTmp) >= 0)
      {
        // The argument is a boolean
        cRq.SetArg(strTmp, "1");
      }
      else if(pCmdDesc->m_bExtendedOptions)
      {
        // Extended option or boolean
        if(++ui < vArgs.Size())
        {
          strArg = vArgs[ui];
          if(strArg.StartsWith("--"))
          {
            // The argument is a boolean
            cRq.SetArg(strTmp, "1");
            ui--;
          }
          else
          {
            // The argument is an option
            cRq.SetArg(strTmp, vArgs[ui]);
          }
        }
        else
        {
          // The argument is a boolean
          cRq.SetArg(strTmp, "1");
        }
      }
      else
      {
        // Bad argument
        cAnswer.AddMessage(strCmd + ": unknown option \"" + strTmp + "\"");
        cAnswer.AddMessage(pCmdDesc->m_strUsage);
        iError = GEN_ERR;
        break;
      }
    }
    else
    {
      if(uiMandatory < pCmdDesc->m_vMandatoryArgs.Size())
      {
        // Mandatory argument
        cRq.SetArg(pCmdDesc->m_vMandatoryArgs[uiMandatory++], strArg);
      }
      else if(uiOptional < pCmdDesc->m_vOptionalArgs.Size())
      {
        // Optional argument
        cRq.SetArg(pCmdDesc->m_vOptionalArgs[uiOptional++], strArg);
      }
      else
      {
        // Standalone arguments overflow
        cAnswer.AddMessage(strCmd + ": too many arguments");
        cAnswer.AddMessage(pCmdDesc->m_strUsage);
        iError = GEN_ERR;
        break;
      }
    }
  }

  // Check if there are enough arguments
  if(uiMandatory < pCmdDesc->m_vMandatoryArgs.Size())
  {
    cAnswer.AddMessage(strCmd + ": too few arguments");
    cAnswer.AddMessage(pCmdDesc->m_strUsage);
    iError = GEN_ERR;
  }

  if(iError)
  {
    cAnswer.SetStatus(ADMIN_COMMAND_NOT_VALID);
  }
  else
  {
    cRequest = cRq;
    cAnswer.SetStatus(ADMIN_WELLFORMED_COMMAND);
  }

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Admin::ValidateRequest(C_AdminSession* pSession,
                                  C_Request& cRequest)
{
  ASSERT(pSession);

  C_Answer cAnswer("Admin");

  C_String strCmd = cRequest.GetCmd();

   // Empty command line ?
  if(strCmd.Length() == 0)
  {
    cAnswer.SetStatus(ADMIN_EMPTY_COMMAND);
    return cAnswer;
  }

  // Unknown command ?
  C_CommandDesc* pCmdDesc = m_cCmdDescriptions.Get(strCmd);
  if(!pCmdDesc)
  {
    cAnswer.SetStatus(ADMIN_UNKNOWN_COMMAND);
    cAnswer.AddMessage(strCmd + ":    unknown command.");
    return cAnswer;
  }

  // The user is not allowed to run this command ?
  if((!m_bIsSuperUser) && pSession->m_vCommands.Find(strCmd) < 0)
  {
    cAnswer.SetStatus(ADMIN_COMMAND_DENIED);
    cAnswer.AddMessage(strCmd + ": permission denied.");
    return cAnswer;
  }

  // Check arguments' validity
  //   - mandatory arguments shouldn't be empty;
  //   - booleans should be "0" or "1";
  //   - we don't check arguments which aren't in the description because we
  //     don't have access to the hashtable in the C_Request. It doesn't matter
  //     because they are ignored.
  int iError = 0;

  unsigned int ui;

  for(ui = 0; ui < pCmdDesc->m_vMandatoryArgs.Size(); ui++)
  {
    C_String strArg = cRequest.GetArg(pCmdDesc->m_vMandatoryArgs[ui]);
    if(strArg.Length() == 0)
    {
      cAnswer.AddMessage(strCmd + ": \"" +
                         pCmdDesc->m_vMandatoryArgs[ui] + "\" is mandatory");
      cAnswer.AddMessage(pCmdDesc->m_strUsage);
      printf("%s missing \n", pCmdDesc->m_vMandatoryArgs[ui].GetString());
      iError = GEN_ERR;
    }
  }

  for(ui = 0; ui < pCmdDesc->m_vBooleans.Size(); ui++)
  {
    C_String strArg = cRequest.GetArg(pCmdDesc->m_vBooleans[ui]);
    if((strArg != "0") && (strArg != "1"))
    {
      cAnswer.AddMessage(strCmd + ": \"" +
                         strArg + " isn't a boolean value (\"0\" or \"1\")");
      cAnswer.AddMessage(pCmdDesc->m_strUsage);
      iError = GEN_ERR;
    }
  }


  if(iError)
  {
    cAnswer.SetStatus(ADMIN_COMMAND_NOT_VALID);
  }
    else
    cAnswer.SetStatus(ADMIN_WELLFORMED_COMMAND);

  return cAnswer;
}


//------------------------------------------------------------------------------
// Request treatment
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_Answer C_Admin::HandleRequest(C_Request& cRequest)
{
  C_Answer cAnswer("Admin");

  if(m_bRequestsEnabled)
  {
    // Lock the mutex so that only one command can be processed at a time
    // No more needed, done in the manager
    m_cRequestLock.Lock();

    C_String strCmd = cRequest.GetCmd();
    if(strCmd.Find("help") != GEN_ERR || strCmd == "help")
    {
      C_String strArg = cRequest.GetArg("command");
      if(strArg.Length() == 0)
      {
        cAnswer.SetStatus(NO_ERR);
        C_HashTableIterator<C_String, C_CommandDesc> cIterator =
                        m_cCmdDescriptions.CreateIterator();
        while(cIterator.HasNext())
        {
          C_HashTableNode<C_String, C_CommandDesc>* pNode =
                        cIterator.GetNext();
          C_CommandDesc* pCmdDesc = pNode->GetValue();
          int iHelp = strCmd.Find("_");
          int iCmd = pCmdDesc->m_strName.Find("_");
          if(iHelp == GEN_ERR && iCmd == GEN_ERR)
          {
           cAnswer.AddMessage(pCmdDesc->m_strName + ": " + pCmdDesc->m_strHelp);
          }
          else if (iHelp != GEN_ERR && iCmd != GEN_ERR)
          {
              C_String helpRoot = strCmd.SubString(0, iHelp);
              C_String cmdRoot =  pCmdDesc->m_strName.SubString(0, iCmd);
              C_String strMessage;

              strMessage = pCmdDesc->m_strName.SubString(iCmd+1,
                                                pCmdDesc->m_strName.Length());
              strMessage += ": ";
              strMessage += pCmdDesc->m_strHelp;
              if(helpRoot == cmdRoot) cAnswer.AddMessage(strMessage);
          }
        }
      }
      else
      {
        int iCmd;
        if((iCmd = strCmd.Find("_")) != GEN_ERR)
              strArg = strCmd.SubString(0, iCmd+1) + strArg;
        C_CommandDesc* pCmdDesc = m_cCmdDescriptions.Get(strArg);
        if(pCmdDesc)
        {
          cAnswer.SetStatus(NO_ERR);
          cAnswer.AddMessage(pCmdDesc->m_strUsage);
          cAnswer.AddMessage(pCmdDesc->m_strLongHelp);
        }
        else
        {
          cAnswer.AddMessage(strArg + ": unknown command.");
        }
      }
    }
    else if(strCmd == "shutdown")
    {
      Log(m_hLog, LOG_NOTE, "Received 'shutdown' command: stopping vls");
      C_Application* pApp = C_Application::GetApp();
      pApp->Stop();

      cAnswer.SetStatus(NO_ERR);
      cAnswer.AddMessage("Shutdowning the server");
    }
    else
    {
      // Forward the request to the app
      cAnswer = m_pRequestHub->ForwardRequest(cRequest);
    }
  }
  else
  {
    cAnswer.SetStatus(GEN_ERR);
    cAnswer.AddMessage("Requests are disabled");
  }

  // Unlock the mutex to allow another request to be processed
  m_cRequestLock.UnLock();

  return cAnswer;
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Admin::HandleEvent(const C_Event& cEvent)
{
  m_cInterfaceList.Lock();
  C_RepositoryBrowser<C_String, C_Interface> cIterator =
                                              m_cInterfaceList.CreateBrowser();

  while(cIterator.HasNextItem())
  {
    C_Interface* pInterface = cIterator.GetNextItem();

    try
    {
      pInterface->PropagateEvent(cEvent);
    }
    catch(E_Exception e)
    {
       C_String strInterfaceName = pInterface->GetName();
      Log(m_hLog, LOG_ERROR,
          "Unable to propagate event in  "+strInterfaceName+": "+e.Dump());
    }
  }

  // Unlock the Interface repository
  m_cInterfaceList.UnLock();

  // TODO: send something to the users loggued by telnet
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Admin::InitInterfaces()
{
  // Get the names of all the sources
  C_Application* pApp = C_Application::GetApp();
  ASSERT(pApp);
  C_Vector<C_Setting> vInterfaces = pApp->GetSettings("interfaces");

  // Lock the interface repository
  m_cInterfaceList.Lock();

  // If no interface is defined, launch DummyInterface
  // Otherwise, the vls would stop immediately, as
  // last interface is responsible for the main thread to keep running.
  if (vInterfaces.Size() == 0)
  {
    C_Setting * cDummyInterface = new C_Setting("dummy","dummy");
    vInterfaces.Add(cDummyInterface);
  }

  // Create the corresponding interfaces
  for(unsigned int i = 0; i < vInterfaces.Size(); i++)
  {
    // Current interface
    C_Interface* pInterface = NULL;

    // Get interface name and type
    C_Setting cCurrentInterface = vInterfaces[i];
    C_String strInterfaceName = cCurrentInterface.GetName();
    C_String strInterfaceType = cCurrentInterface.GetValue();

    // Create the interface for the given source
    C_InterfaceModule* pModule = (C_InterfaceModule*)
                             C_Application::GetModuleManager()
                                   ->GetModule("interface", strInterfaceType);
    if(pModule)
    {
      C_InterfaceConfig cConfig;
      cConfig.hLog = m_hLog;
      cConfig.pAdmin = this;
      pInterface = pModule->NewInterface(cConfig);
      ASSERT(pInterface);
//TODO tooney
      //pInterface->SetEventHandler(this);
      Log(m_hLog, LOG_NOTE, "Starting interface '"+strInterfaceName+"'");
      try
      {
        pInterface->SetConfig( strInterfaceName );

        // Initialize the interface
        pInterface->Init();

        // Last Interface shall not be run as a separate thread
//        if ( i != vInterfaces.Size()-1 )
        pInterface->Create();

        // And register it
        m_cInterfaceList.Add(strInterfaceName, pInterface);

        Log(m_hLog, LOG_NOTE, "Interface '" + strInterfaceName +
            "' sucessfully initialised");
      }
      catch(E_Exception e)
      {
        Log(m_hLog, LOG_ERROR, "Unable to start interface '" +
                                  strInterfaceName + "': " + e.Dump());
        delete pInterface;
      }
    }
    else
    {
      Log(m_hLog, LOG_ERROR, "Interface type \"" +
                                             strInterfaceType + "\" invalid");
    }
  }

  // Unlock the input repository
  m_cInterfaceList.UnLock();
}

