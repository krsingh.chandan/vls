/*******************************************************************************
* daemon.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2002 VideoLAN
* $Id: daemon.cpp,v 1.4 2003/04/03 12:27:11 jpsaman Exp $
*
* Authors: Jean-Paul Saman <saman@natlab.research.philips.com>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#include <stdio.h>
#include <stdlib.h>

#ifndef  _WIN32
#   include <sys/time.h>
#   include <sys/resource.h>
#   include <sys/stat.h>
#   include <fcntl.h>
#   include <unistd.h>
#endif

#include "daemon.h"

C_Daemon::C_Daemon()
{
  m_isDaemon = false;
}

int C_Daemon::StartDaemon()
{
  int status = -1;

  status = DaemonFork();
  if (status != -1)
    status = DaemonLeaveGroup();
  if (status != -1)
    status = DaemonFork();
  if (status != -1)
    DaemonCloseTerminal();

  m_isDaemon = (status == 0);
  return status;
}

int C_Daemon::StopDaemon()
{
  return 0;
}

bool C_Daemon::isDaemon()
{
  return m_isDaemon;
}

int C_Daemon::DaemonFork()
{
  int status = -1;

#ifndef  _WIN32
  status = fork();
  switch (status)
  {
    case -1:
      perror("fork()");
      exit(1);
    case 0: /* child process */
      break;
    default: /* parent process */
      exit(0);
  }
#endif
  return status;
}

int C_Daemon::DaemonLeaveGroup()
{
  int status = -1;

#ifndef  _WIN32
  status = setsid();
  if (-1 == status)
  {
    perror("setsid()");
    exit(1);
  }
#endif
  return status;
}

void C_Daemon::DaemonCloseTerminal()
{
#ifndef  _WIN32
  int fileDesc = -1;
  /* 
   * close all opened file descriptors we inherited from the parent process
   * they are not needed anymore. Messages should be send to the syslog daemon
   * instead.
   */
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);
  /*
   * now we are in a new session and process
   * group than process that started the
   * daemon. We also have no controlling
   * terminal 
   * */
  chdir("/");
  umask(0);
  fileDesc = open("/dev/null", O_RDWR);/* stdin */
  (void) dup(fileDesc);	/* stdout */
  (void) dup(fileDesc);	/* stderr */
#endif
}
