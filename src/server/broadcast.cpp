/*******************************************************************************
* broadcast.cpp: Broadcast class
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: broadcast.cpp,v 1.3 2003/10/27 10:58:12 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "program.h"
#include "broadcast.h"


//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_Broadcast::C_Broadcast(const C_Program* pProgram, C_Input* pInput,
                         C_Channel* pChannel, C_String sName)
{
  ASSERT(pProgram);
  ASSERT(pInput);
  ASSERT(pChannel);

  m_pProgram = pProgram;
  m_pInput = pInput;
  m_pChannel = pChannel;
  m_strName = sName;

  m_bStatus = BROADCAST_WAITING;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Broadcast::SetOption(const C_String& strName, const C_String strValue)
{
  C_String* pStr = new C_String(strValue);
  m_cOptions.Update(strName, pStr);
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
const C_String C_Broadcast::GetOption(const C_String& strName) const
{
  C_String* pStr = m_cOptions.Get(strName);
  if(pStr)
    return *pStr;
  else
    return "";
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_Broadcast::SetOptions(const C_HashTable<C_String, C_String>& cOptions)
{
  m_cOptions = cOptions;
}


