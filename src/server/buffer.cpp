/*******************************************************************************
* buffer.cpp: Buffers management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: buffer.cpp,v 1.13 2003/10/27 10:58:12 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "buffer.h"

#if defined(HAVE_NANOSLEEP) && !defined(HAVE_DECL_NANOSLEEP)
#include <time.h>
#endif


//******************************************************************************
// C_SyncFifo class
//******************************************************************************
// Works even if several threads push data and several ones pop them
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_SyncFifo::C_SyncFifo(unsigned int iSize) : m_cNotEmptySignal(0),
                                             m_cNotFullSignal(iSize),
                                             m_cPrefilling(),
                                             m_cFifo(iSize)
{
  ASSERT(iSize > 1);
  m_bShutdown = false;
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_SyncFifo::~C_SyncFifo()
{
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_SyncFifo::HandlePacket(C_TsPacket* pPacket)
{
  int iRc;
  ASSERT(pPacket);

  // Push the data (wait for a pop if the fifo is full)
  // wait for a pop without blocking the thread permanently
  while(((iRc = m_cNotFullSignal.TryWait()) == -1) && (!m_bShutdown))
  {
#ifdef WIN32
    Sleep((int)(1 / 1000));
#elif defined(HAVE_NANOSLEEP)
    struct timespec tsDelay;
    tsDelay.tv_sec = 0;
    tsDelay.tv_nsec = 50 * 1000 * 1000; // sleep 50 ms

    // Wait for the given delay
    nanosleep(&tsDelay, NULL);
#else
#   error nanosleep not present !
#endif
  }
  ASSERT(!iRc);
  if (m_bShutdown) return false;

  iRc = m_cFifo.Push(pPacket);
  ASSERT(!iRc);

  // Warn the waiting threads if any that they have data to pop
  iRc = m_cNotEmptySignal.Post();
  ASSERT(!iRc);

  return true;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
bool C_SyncFifo::HandlePrefillPacket(C_TsPacket* pPacket)
{
  int iRc;
  ASSERT(pPacket);

  // see if there is any space left (don't block though)
  if (m_cNotFullSignal.TryWait() == -1) return false;

  // add on the packet
  iRc = m_cFifo.Push(pPacket);
  ASSERT(!iRc);

  // Warn the waiting threads if any that they have data to pop
  iRc = m_cNotEmptySignal.Post();
  ASSERT(!iRc);

  return true;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_SyncFifo::PrefillStart() {
  m_cPrefilling.Protect();
}

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_SyncFifo::PrefillComplete() {
  m_cPrefilling.Release();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_SyncFifo::Shutdown()
{
  m_bShutdown = true;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_TsPacket* C_SyncFifo::Pop()
{
  m_cPrefilling.Protect();

  // Pop the data (wait for them if the fifo is empty)
  int iRc;
  while( (iRc = m_cNotEmptySignal.TryWait()) == -1)
  {
#ifdef WIN32
    Sleep((int)(1 / 1000));
#elif defined(HAVE_NANOSLEEP)
    struct timespec tsDelay;
    tsDelay.tv_sec = 0;
    tsDelay.tv_nsec = 50 * 1000 * 1000; // sleep 50 ms

    // Wait for the given delay
    nanosleep(&tsDelay, NULL);
#else
#   error nanosleep not present !
#endif
  }
  ASSERT(!iRc);

  C_TsPacket* pPacket = m_cFifo.Pop();
  ASSERT(pPacket);

  // Warn the waiting threads if any that they have data to push
  iRc = m_cNotFullSignal.Post();
  ASSERT(!iRc);

  m_cPrefilling.Release();

  return pPacket;
}

C_TsPacket* C_SyncFifo::Peek(unsigned int uiIndex)
{
  C_TsPacket* pPacket = m_cFifo.Peek(uiIndex);
  ASSERT(pPacket);

  return pPacket;
}

//******************************************************************************
// C_DatedFifo class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------
C_DatedFifo::C_DatedFifo(unsigned int uiSize) : m_cFifo(uiSize, YES, true),
                                                m_cMutex()
{
  ASSERT(uiSize > 0);
}


//------------------------------------------------------------------------------
// Destructor
//------------------------------------------------------------------------------
C_DatedFifo::~C_DatedFifo()
{
}

C_DatedBuffer *C_DatedFifo::Pop(void)
{
	m_cMutex.Lock();
    C_DatedBuffer *pDatedBuffer = m_cFifo.Pop();
	m_cMutex.UnLock();
	ASSERT(pDatedBuffer);

	return pDatedBuffer;
}

int C_DatedFifo::Push(C_DatedBuffer *bufDated)
{
    int iRc = NO_ERR;
	ASSERT(bufDated);

	m_cMutex.Lock();
    if( m_cFifo.Size() == m_cFifo.Capacity() )
    	iRc = GEN_ERR;
    if (!iRc)
    	iRc = m_cFifo.Push(bufDated);
	m_cMutex.UnLock();

	return iRc;
}

//******************************************************************************
// C_NetList class
//******************************************************************************
//
//******************************************************************************

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_NetList::C_NetList(unsigned int iSize/* = 65536*/) : m_sMutex()
{
  iBuffSize = iSize;
  
  // Create the buffer of TS packets
  aPackets = new C_TsPacket[iSize];
  ASSERT(aPackets);

  // Create the buffer of pointers to free TS packets
  apFreePackets = new C_TsPacket* [iSize];
  ASSERT(apFreePackets);
  
  // Now init the second buffer
  for(unsigned int iIndex = 0; iIndex < iSize; iIndex++)
  {
    apFreePackets[iIndex] = &aPackets[iIndex];
  }

  // All the packets are free
  iFirstFree = 0;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_NetList::~C_NetList()
{
  ASSERT(aPackets);
  delete[] aPackets;

  ASSERT(apFreePackets);
  delete[] apFreePackets;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Return the address of the fetched packet or NULL if the netlist is empty 
//------------------------------------------------------------------------------
C_TsPacket* C_NetList::GetPacket()
{
  C_TsPacket* pFreePacket = NULL;

  m_sMutex.Lock();

  // Get the addree of the first free packet if any
  if(iFirstFree < iBuffSize)
  {
    pFreePacket = apFreePackets[iFirstFree];
    iFirstFree++;

    pFreePacket->Ref();
    ASSERT(pFreePacket->RefCount() == 1);
  }

  m_sMutex.UnLock();

  return pFreePacket;
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_NetList::RefPacket(C_TsPacket* pPacket)
{
  ASSERT(pPacket);

  m_sMutex.Lock();

  pPacket->Ref();

  m_sMutex.UnLock();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_NetList::ReleasePacket(C_TsPacket* pPacket)
{
  ASSERT(pPacket);

  m_sMutex.Lock();

  if(!pPacket->Unref())
  {

    ASSERT(iFirstFree <= iBuffSize);                   // Check everything is OK
    ASSERT(iFirstFree > 0);
  
    iFirstFree--;
    apFreePackets[iFirstFree] = pPacket;
  }

  m_sMutex.UnLock();
}

