/*******************************************************************************
* tsstreamer.cpp:
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: tsstreamer.cpp,v 1.25 2004/01/21 14:18:20 adq Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#include "../core/core.h"
#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"
#include "program.h"
#include "buffer.h"
#include "output.h"
#include "channel.h"
#include "broadcast.h"
#include "request.h"
#include "input.h"
#include "tsstreamer.h"


#if defined(HAVE_NANOSLEEP) && !defined(HAVE_DECL_NANOSLEEP)
#include <time.h>
#endif

    
/*******************************************************************************
*
********************************************************************************
*
*******************************************************************************/

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// The thread is created in detached state to allow itself to stop its execution
// before the end of the stream if an error occurs
//------------------------------------------------------------------------------
C_TsStreamer::C_TsStreamer(handle hLog, C_Broadcast* pBroadcast,
                           C_NetList* pTsProvider, C_SyncFifo* pBuffer,
                           C_EventHandler* pEventHandler,
                           bool bOwnProvider, bool bUsePcr)
{
  ASSERT(pBroadcast);
  ASSERT(pTsProvider);
  ASSERT(pBuffer);
  ASSERT(pEventHandler);

  ASSERT(pTsProvider->Capacity() > pBuffer->Size());

  m_hLog = hLog;
  m_pBroadcast = pBroadcast;
  m_pTsProvider = pTsProvider;
  m_pBuffer = pBuffer;

  m_pEventHandler = pEventHandler;

  C_Channel* pChannel = m_pBroadcast->GetChannel();
  ASSERT(pChannel);
  m_pOutput = pChannel->GetOutput();

  m_bStop = 0;

  m_uiByteRead = 0;
  m_bFirstPCR = 1;

  // Set those values to 0 in order to send the packets that come before
  // the first one with a PCR as fast as possible (best behaviour)
  m_iLastTime = 0;
  m_iDeltaClock = 0;
  m_dSlope = 0;

  m_iHowMany = 0;

  m_bOwnProvider = bOwnProvider;
  m_bUsePcr = bUsePcr;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
C_TsStreamer::~C_TsStreamer()
{
  // The output is no more needed
  C_Channel* pChannel = m_pBroadcast->GetChannel();
  ASSERT(pChannel);
  pChannel->ReleaseOutput();

  // The TsProvider is shared by the Reader and the Streamer's output: it will
  // therefore be destroyed by the Streamer since this is the last stopped
  if(m_bOwnProvider)
    delete m_pTsProvider;

  // The Buffer is shared by the Reader and the Streamer: it will be
  // destroyed by the Streamer since this is the last stopped
  delete m_pBuffer;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TsStreamer::InitWork()
{
  try
  {
    m_pOutput->Init(m_pTsProvider);
  }
  catch(E_Exception e)
  {
    throw E_Exception(GEN_ERR,  "Unable to init streamer", e);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TsStreamer::DoWork()
{
  int iRc = NO_ERR;
  bool bRtp = (m_pBroadcast->GetOption("rtp") == "1");

  // Get the packet from the buffer and send them to the output
  while(!m_bStop && (iRc >= 0))
  {
    // Log buffer underruns  
#if 0
    if ((m_pBuffer->Size()<1) && (!m_bStop))
    {
      C_String strChannelName = m_pBroadcast->GetChannel()->GetName();
      C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
      Log(m_hLog, LOG_ERROR, C_String("SyncFifo has buffer underrun!! ") +
          C_String("For channel ") + strChannelName +
         C_String(" streaming program ") + strPgrmName);
    }
#endif

    C_TsPacket* pPacket = m_pBuffer->Pop();
    ASSERT(pPacket);

    if(m_bUsePcr &&  pPacket != NULL)
    {
      // Increases the counters
      m_uiByteRead += TS_PACKET_LEN;
      m_iHowMany ++;

      // Read the PCR info if any
      if(pPacket->HasPCR())
      {
        if(m_bFirstPCR)
        {
          InitClock(pPacket);
          m_bFirstPCR = 0;
        }
        AdjustClock(pPacket);
      }

      // Wait until the date at which the packet must be sent is reached
      // For better perf, doesn't sleep for each TS but only when the output
      // buffer is ready to be send
      if(m_iHowMany == m_pOutput->GetBuffCapacity())
      {
        if (!m_bFirstPCR)
          WaitSendDate(pPacket);
        m_iHowMany = 0;
      }
      else
        m_iHowMany++;
    }

    // TS packet is ok, send it
    try
    {
      m_pOutput->Send(pPacket, bRtp );
    }
    catch(E_Exception e)
    {
      iRc = GEN_ERR;
      C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
      Log(m_hLog, LOG_NOTE, C_String("Unable to send stream for pgrm ")+
          strPgrmName + ": "/* + e.Dump()*/);
    }
  }

  // If a problem occurred in streaming and no stop signal is currently
  // pending, ask for program termination
  if((iRc <= 0) && !m_bStop)
  {
    C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
    Log(m_hLog, LOG_NOTE, C_String("Error when streaming program ")+
        strPgrmName+": aborting");
    m_bStop = true;
    C_Event cEvent(m_pBroadcast->GetInput()->GetName());
    cEvent.SetCode(CONNEXION_LOST_EVENT);
    cEvent.SetBroadcast(m_pBroadcast);
    m_pEventHandler->HandleEvent(cEvent);
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TsStreamer::StopWork()
{
  if(!m_bStop)
  {
    m_bStop = true;
    
    // The streamer should be blocked at this time. That's why we force its
    // interruption instead of using the DelayedInterruption() method.
    Interrupt();    
  }
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
void C_TsStreamer::CleanWork()
{
  m_pOutput->Close();
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// Init the clock: compute the DeltaClock and init LastTime variable
// Must therefore be called instead of AdjustClock when the first PCR is
// received
//------------------------------------------------------------------------------
inline void C_TsStreamer::InitClock(C_TsPacket* pPacket)
{
  ASSERT(pPacket);
  ASSERT(pPacket->HasPCR());

  s64 iPCRTime = pPacket->GetPCRTime();

  // Compute the delta between the clock of the PC and the one of the encoder
  m_iDeltaClock = GetDate() - iPCRTime;

  // Update the data for the next PCR
  m_uiByteRead = 0;
  m_iLastTime = iPCRTime;
  m_iNextPCRTime = 0;
}



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// The clock is adjusted for the time to go with the help of the current PCR
// and of the previous one, not of the current one and the next one: this can
// lead to some little pbs if the throughtput is not constant and if the buffer
// of the client are to small
//------------------------------------------------------------------------------
inline void C_TsStreamer::AdjustClock(C_TsPacket* pPacket)
{
  ASSERT(pPacket);
  ASSERT(pPacket->HasPCR());

  s64 iPCRTime = pPacket->GetPCRTime();

  if ( (iPCRTime < m_iLastTime) ||
       ((iPCRTime - m_iLastTime) > 300000))
  {
    LogDbg(m_hLog, "Odd PCR " + (int)iPCRTime + " found ! Previous one was "
                  +  (int)m_iLastTime + "."); 
    pPacket->SetDiscontinuityFlag();
  }
    

  if(pPacket->IsDiscontinuity())
  {
    // Reajust the delta between the clock of the PC and the one of the encoder
    C_String strPgrmName = m_pBroadcast->GetProgram()->GetName();
    LogDbg(m_hLog, "Adjusting timer discontinuity for pgrm "+strPgrmName);
    m_iDeltaClock = GetDate() - iPCRTime;
  }
  else if (!InterpolateSlope(iPCRTime))
  {
    // Fallback to extrapolation when no next PCR can be found.
    ASSERT(m_uiByteRead > 0);
    if (iPCRTime > m_iLastTime)
    {
      // (Re)evaluate the slope
#ifdef _WIN32
      m_dSlope = ((double)iPCRTime - m_iLastTime) / (s64)m_uiByteRead;
#else
      m_dSlope = ((double)iPCRTime - m_iLastTime) / m_uiByteRead;
#endif
    } // else keep current slope it is the best guess.
      // Probably due to --loop wrap around
  }
  // Update the data for the next PCR

  m_uiByteRead = 0;
  m_iLastTime = iPCRTime;
}


//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
inline void C_TsStreamer::WaitSendDate(C_TsPacket* pPacket)
{
  s64 iSendDate = m_iLastTime + m_iDeltaClock + (s64) (m_dSlope*m_uiByteRead);
  s64 iWait = iSendDate - GetDate();

  // reset the clock if we get a runaway wait
  if (iWait < -100000) {
    Log(m_hLog, LOG_NOTE, C_String("Runwaway wait detected and aborted. \
         Your server is likely too overloaded for correct vls streaming."));
    InitClock(pPacket);
  }

  // Wait only if the delay becomes > 50 us, for the select will make we late
  // (it usually takes more than 1 ms)
  if (iWait > 1000000)
    LogDbg(m_hLog,"Supposed to wait more than 1 seconde... This is very \
            strange, please report it to the vls-devel@videolan.org ml !");
  else if(iWait > 50)
  {
    // Sleep during the given time
#ifdef WIN32
    Sleep((int)(iWait / 1000));
#elif defined(HAVE_NANOSLEEP)
    struct timespec tsDelay;
    tsDelay.tv_sec = iWait / 1000000;
    tsDelay.tv_nsec = 1000 * (iWait % 1000000);

    // Wait for the given delay
    nanosleep(&tsDelay, NULL);
#else
#   error nanosleep not present !
#endif
  }
}

//------------------------------------------------------------------------------
// Calculate the slope between two PCR's from the stream
//------------------------------------------------------------------------------
bool C_TsStreamer::InterpolateSlope(s64 iPCRTime)
{
  //ASSERT (iPCRTime>=m_iNextPCRTime);

  unsigned int uiIndex;
  unsigned int uiSize;
  unsigned int uiBytesToSend = 0;
  s64 iPotentialNextPCRTime;

  uiSize = m_pBuffer->Size();
  for (uiIndex = 1; uiIndex < uiSize; uiIndex++)
  {
    C_TsPacket *pPacket = m_pBuffer->Peek(uiIndex);
    ASSERT(pPacket);

    uiBytesToSend += TS_PACKET_LEN;
    if (pPacket->HasPCR())
    {
      // Remember the next PCR time, and use it in calculations for sending.
      ASSERT(uiBytesToSend>0);

      iPotentialNextPCRTime = pPacket->GetPCRTime();

      // Make sure there is an increase in PCR values and that it is within 
      // a reasonable time frame. The MPEG spec says a PCR about 40 ms apart.
      // The absolute limit here is 100ms, this allows some jitter in PCR's.
      //
      // Mmm... Section 2.7.3 says 100ms, so I modified to 300ms --tooney
      if ( (iPotentialNextPCRTime>iPCRTime) &&
           ((iPotentialNextPCRTime-iPCRTime) < 300000) )
      {
        m_iNextPCRTime = iPotentialNextPCRTime;
        m_dSlope = ((double) m_iNextPCRTime - iPCRTime) / (s64)uiBytesToSend;  
        return true;
      }
      /* else
        LogDbg(m_hLog,"Skipped interpolation for PCR " + (s32)iPCRTime +
                       " -> " + (s32)iPotentialNextPCRTime + " (delta : "
                      + (s32)(iPotentialNextPCRTime-iPCRTime) + ")");
      */
    }
  }
  return false;
}

//------------------------------------------------------------------------------
// Returns the current date in microseconds
//------------------------------------------------------------------------------
inline s64 C_TsStreamer::GetDate()
{
#ifdef _WIN32
  s64 freq, usec_time;

  if( QueryPerformanceFrequency( (LARGE_INTEGER *)&freq ) )
  {
    // Microsecond resolution
    QueryPerformanceCounter( (LARGE_INTEGER *)&usec_time );
    return ( usec_time * 1000000 ) / freq;
  }
  // Milisecond resolution
  return 1000 * GetTickCount();
#else
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return( (s64)(tv.tv_sec * 1000000) + (s64)tv.tv_usec );
#endif
}
