/*******************************************************************************
* manager.h: Manager class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: manager.h,v 1.13 2003/10/31 16:44:31 tooney Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/

#ifndef _MANAGER_H_
#define _MANAGER_H_


//------------------------------------------------------------------------------
// C_Manager : manage all the inputs and their programs
//------------------------------------------------------------------------------
class C_Manager : public C_RequestHandler, public C_EventHandler
{
 public:
  C_Manager(handle hLog, C_EventHub* pEventHub);
  virtual ~C_Manager();

  int Init();
  int Run();
  int Stop();
  int Destroy();

  // Handlers
  virtual C_Answer HandleRequest(C_Request& cRequest);
  virtual void HandleEvent(const C_Event& cEvent);

 protected:
  // Pgrm table management
  void InitPgrmTable();
  void DestroyPgrmTable();
  // Inputs management
  void InitInputs();
  void DestroyInputs();
  // Channels management
  void InitChannels();
  void DestroyChannels();
  // Commands management
  void InitCommands();
  
  
  void StopPrograms();

  void SpawnHelper(C_DoublePipe * cPipe, C_String strHelperExec);

  // Commands processing
  C_Answer Ls(C_Request& cRequest);
  C_Answer Config(C_Request& cRequest);

  C_Answer Broadcast_Ls(C_Request& cRequest);
  C_Answer Broadcast_Add(C_Request& cRequest);
  C_Answer Broadcast_Del(C_Request& cRequest);
  C_Answer Broadcast_Start(C_Request& cRequest);
  C_Answer Broadcast_Resume(C_Request& cRequest);
  C_Answer Broadcast_Suspend(C_Request& cRequest);
  C_Answer Broadcast_Stop(C_Request& cRequest);
  C_Answer Broadcast_Forward(C_Request& cRequest);
  C_Answer Broadcast_Rewind(C_Request& cRequest);

  C_Answer Channel_Ls(C_Request& cRequest);
  C_Answer Channel_Add(C_Request& cRequest);
  C_Answer Channel_Del(C_Request& cRequest);

  C_Answer Program_Ls(C_Request& cRequest);
#ifdef MANAGER_EXTRA
#  define EXTRA_DECL
#  include MANAGER_EXTRA
#  undef EXTRA_DECL
#endif

  virtual void PrivHandleEvent(const C_Event& cEvent);
  C_Broadcast* CreateBroadcast(C_String&,C_String&,
                                                C_String&,C_String&,C_Answer*);
private:

  handle m_hLog;
  C_EventHub* m_pEventHub;
  C_Mutex m_cCmdLock;

  bool m_bStop;

  C_Mutex m_cEventFifoLock;
  C_Semaphore m_cEventQueued;
  C_Vector<C_Event> m_cEventFifo;

  // Running broadcasts
  C_Repository<C_String, C_Broadcast> m_cBroadcastsList;

  // Ressources
  C_Repository<C_String, C_Input> m_cInputList;
  C_Repository<C_String, C_Channel> m_cChannelList;

  //Functions
  C_HashTable<C_String, C_Answer (C_Manager::*)(C_Request&)> m_cFunctions;

  // Programs
  C_PgrmDirectory m_cProgramList;

  C_HashTable<C_String, C_DoublePipe> m_cHashDoublePipe;
  int m_iChildPid;

};


// Macro for writing configuration file
// TODO should be integrated in the parser part
#define WRITE_BEGINS(fd, iT, cT, v)                                          \
    for ( unsigned int km=0; km<v; km++)                                     \
    {                                                                        \
        cT->NextToken();                                                     \
    }                                                                        \
   for ( unsigned int km=v; km<iT-1; km++)                                   \
    {                                                                        \
        for (unsigned int l=0; l<km; l++) fprintf(fd, "  ");                 \
        fprintf(fd,"BEGIN \"%s\"\n", cT->NextToken().GetString());           \
    }

#define WRITE_ENDS(fd, iT, cT, v)                                           \
    for(unsigned int km=iT-1; km>v; km--)                                  \
    {                                                                       \
        for (unsigned int l=0; l<km-1; l++) fprintf(fd, "  ");              \
        fprintf(fd,"END\n");                                                \
    }                                                                       \
    if(v!=iT-1) fprintf(fd, "\n");

#define WRITE_PROPERTY(fd, iT, cT)                                          \
    for(unsigned int km=0;km<iT-1;km++) fprintf(fd, "  ");                  \
    fprintf(fd, "%s = \"%s\"\n", cT->NextToken().GetString(),               \
                                 cConfig[i].GetValue().GetString());

#else
#error "Multiple inclusions of manager.h"
#endif

