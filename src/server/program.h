/*******************************************************************************
* program.h: program management
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: program.h,v 1.2 2002/10/01 08:09:11 jpsaman Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


#ifndef _PROGRAM_H_
#define _PROGRAM_H_



//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
// The C_Program should be locked before an access is tried to its properties.
// To lock it, you can either use the [] operator of the C_PgrmDirectory or one
// of its GetPgrm methods
// Note that a program is locked when it is not stored in a directory.
//------------------------------------------------------------------------------
class C_Program
{
  //friend class C_PgrmDirectory;
  //friend class C_List<C_Program>;
 
 public:
  C_Program(/*u16 iPgrmNumber,*/ const C_String& strName,
            u16 iPmtPid = (u16)-1, u16 iPcrPid = (u16)-1);
  ~C_Program();

  C_Program* Clone() const;

  // Read access to protected data member
  const C_String& GetName() const;
  C_String GetDescription() const;

  // Properties reader
  //u16 GetPgrmNumber() const;
  u16 GetPcrPid() const;
  u16 GetPmtPid() const;
  //u32 GetProviderId() const;
  
  // Properties modifiers
  //void UpdateName(const C_String& strName);
  //void SetPcrPid(u16 iPcrPid);
  //void SetPmtPid(u16 iPmtPid);
  //void SetProviderId(u32 iProviderId);

  // Compare 2 programs on their Id
  //bool operator == (const C_Program& cProgram) const;

 protected:
  // Should only be accessed from the directory to ensure proper locking
  //void Lock();
  //void UnLock();

 private:
  // Program name, if available
  C_String m_strName;
  // Program Number
  //u16 m_iPgrmNumber;
  // PID carrying the PCR for that program
  u16 m_iPcrPid;
  // PID of the PMT for that program
  u16 m_iPmtPid;
  // Id of the provider
  //u32 m_iProviderId;

  // Directory locking
  //C_Mutex m_cLock;
  
  //#ifdef DEBUG
  //  bool m_bIsLocked;
  //  C_Thread* m_pLockOwner;
  //#endif
};

#else
#error "Multiple inclusions of program.h"
#endif

