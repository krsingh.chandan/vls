/*******************************************************************************
* output.cpp: output streams
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: output.cpp,v 1.7 2003/10/27 10:58:12 sam Exp $
*
* Authors: Benoit Steiner <benny@via.ecp.fr>
*          Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
*
*******************************************************************************/


//------------------------------------------------------------------------------
// Preamble
//------------------------------------------------------------------------------
#include "config.h"

#ifdef HAVE_SYSTIME_H
#include <sys/time.h>
#endif

#include "../core/core.h"
#include "../mpeg/mpeg.h"
#include "../mpeg/ts.h"
#include "../mpeg/rtp.h"
#include "buffer.h"
#include "output.h"



//******************************************************************************
// E_Output class
//******************************************************************************
//
//******************************************************************************
E_Output::E_Output(const C_String& strMsg, const E_Exception& e) :
                                                 E_Exception(GEN_ERR, strMsg, e)
{
}


//******************************************************************************
// C_Output class
//******************************************************************************
//
//******************************************************************************


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Output::C_Output(unsigned int iBuffSize) : m_cTsBuff(iBuffSize)
{
  // Byte loss couter initialisation
  m_iByteLost = 0;

  ZERO(m_pTsProvider);

  m_pRtpHeader = new C_RtpHeader;
  m_iRtpCounter = 1+(int) (65535.0*rand()/(RAND_MAX+1.0));
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
C_Output::~C_Output()
{
  ZERO(m_pTsProvider);

  ASSERT(m_cTsBuff.Size() == 0);
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Output::Init(C_NetList* pTsProvider)
{
  ASSERT(pTsProvider);
  m_pTsProvider = pTsProvider;

  ASSERT(m_cTsBuff.Size() == 0);
  OnInit();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Output::Close()
{
  Flush();
  OnClose();
}


//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
// Bla Bla sur buffer
//------------------------------------------------------------------------------
void C_Output::Send(C_TsPacket* pPacket, bool RtpEncapsulation)
{
  ASSERT(pPacket);

  // Append the packet to the buffer for later write to the output
#ifdef DEBUG
  int iRc = m_cTsBuff.Push(pPacket);
#else
  m_cTsBuff.Push(pPacket);
#endif
  ASSERT(!iRc);

  if(m_cTsBuff.Size() == m_cTsBuff.Capacity())
  {
    // Send the buffered packets to the device and give them back to the
    // netlist
    WriteToPort(RtpEncapsulation, GetDate());
  }
}

//------------------------------------------------------------------------------
// 
//------------------------------------------------------------------------------
void C_Output::Flush()
{
  WriteToPort(0, 0);
}

inline s64 C_Output::GetDate()
{
#ifdef _WIN32
  s64 freq, usec_time;

  if( QueryPerformanceFrequency( (LARGE_INTEGER *)&freq ) )
  {
    // Microsecond resolution
    QueryPerformanceCounter( (LARGE_INTEGER *)&usec_time );
    return ( usec_time * 1000000 ) / freq;
  }
  // Milisecond resolution
  return 1000 * GetTickCount();
#else
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return( (s64)tv.tv_sec * 1000000 + (s64)tv.tv_usec );
#endif
}


