/*******************************************************************************
* admin.h: Admin class definition
*-------------------------------------------------------------------------------
* (c)1999-2001 VideoLAN
* $Id: admin.h,v 1.7 2003/10/30 20:45:06 tooney Exp $
*
* Authors: Arnaud de Bossoreille de Ribou <bozo@via.ecp.fr>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*
*-------------------------------------------------------------------------------
* Manage the different modules in charge of the communication with vls
* administrators
* This module is responsible for the serialization of the requests that may
* be sent at the same time by different administrators so that the inputs
* don't need to take care of that potentialy difficult problem
*
*******************************************************************************/


#ifndef _ADMIN_H_
#define _ADMIN_H_


//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------
#define ADMIN_WELLFORMED_COMMAND         0
#define ADMIN_EMPTY_COMMAND              1
#define ADMIN_UNKNOWN_COMMAND           -1
#define ADMIN_COMMAND_NOT_VALID         -2
#define ADMIN_COMMAND_DENIED            -3

//------------------------------------------------------------------------------
// Forward declaration
//------------------------------------------------------------------------------
class C_Interface;
class C_InterfaceSession;
class C_InterfaceModule;
class C_InterfaceConfig;

class C_Admin;


//------------------------------------------------------------------------------
// C_CommandDesc class
//------------------------------------------------------------------------------
class C_CommandDesc
{
public:
  C_CommandDesc(const C_String& strName,
                const C_String& strHelp, const C_String& strLongHelp);

  void BuildUsage();

  C_String m_strName;

  C_Vector<C_String> m_vMandatoryArgs;
  C_Vector<C_String> m_vOptionalArgs;
  C_Vector<C_String> m_vOptions;
  C_Vector<C_String> m_vBooleans;

  bool m_bExtendedOptions;

  C_String m_strUsage;
  C_String m_strHelp;
  C_String m_strLongHelp;
};

//------------------------------------------------------------------------------
// C_AdminGroup class
//------------------------------------------------------------------------------
class C_AdminGroup
{
  friend class C_Admin;
public:
  C_AdminGroup(const C_String& strName);

protected:
  C_String m_strName;

  C_Vector<C_String> m_vCommands;
};


//------------------------------------------------------------------------------
// C_AdminUser class
//------------------------------------------------------------------------------
class C_AdminUser
{
  friend class C_Admin;
public:
  C_AdminUser(const C_String& strLogin, const C_String& strEncPasswd,
              C_AdminGroup* pGroup);

protected:
  C_String m_strLogin;
  C_String m_strEncPasswd;

  C_AdminGroup* m_pGroup;
};


//------------------------------------------------------------------------------
// C_AdminSession class
//------------------------------------------------------------------------------
class C_AdminSession
{
  friend class C_Admin;
public:
  C_AdminSession(C_Admin* pAdmin);
  virtual ~C_AdminSession();

  void Init();
  void Close();

protected:
  virtual void OnInit() {};
  virtual void OnClose() {};

  int Authenticate(const C_String& strLogin, const C_String& strPasswd);

  C_Admin* m_pAdmin;

private:
  C_String m_strUser;
  C_String m_strGroup;
  C_Vector<C_String> m_vCommands;
};


//------------------------------------------------------------------------------
// C_Admin class
//------------------------------------------------------------------------------
class C_Admin : public C_EventHandler, public C_RequestHandler
{
 public:
  C_Admin(handle hLogger, C_RequestHub* pRequestHub);
  virtual ~C_Admin();

  // Initialization
  int Init();
  // Execution
  int Run();
  // Stops only the main thread to avoid thread suicides.
  int Stop();
  // Complete the Stop process.
  int FullStop();
  // Destruction
  int Destroy();

  int Authenticate(C_AdminSession *pSession,
                   const C_String& strLogin, const C_String& strPasswd);

  void DisableRequests() { m_bRequestsEnabled = false; };

  void SetSuperUser() {m_bIsSuperUser = true; };

  // Request handling
  C_Answer ParseCmdLine(C_AdminSession* pSession, const C_String& strCmdLine,
                    C_Request& cRequest);
  C_Answer ValidateRequest(C_AdminSession* pSession, C_Request& cRequest);
  C_Answer HandleRequest(C_Request& cRequest);
  // Event handling
  void HandleEvent(const C_Event& cEvent);

 protected:
  handle m_hLog;

  void InitInterfaces();
  
  C_HashTable<C_String, C_CommandDesc> m_cCmdDescriptions;

  C_HashTable<C_String, C_AdminGroup> m_cGroups;
  C_HashTable<C_String, C_AdminUser> m_cUsers;

  C_RequestHub* m_pRequestHub;
  bool m_bRequestsEnabled;

  C_Mutex m_cRequestLock;
  C_Mutex m_cSessionLock;

  bool m_bIsSuperUser;

  C_Repository<C_String, C_Interface> m_cInterfaceList;
};


#else
#error "Multiple inclusions of admin.h"
#endif

