#! /bin/sh

echo "begin debug"
make distclean 1> /dev/null
./configure 1> /dev/null
make 1> /dev/null
echo "end debug"


echo "begin normal"
make distclean 1> /dev/null
./configure --disable-debug 1> /dev/null
make 1> /dev/null
echo "end normal"

