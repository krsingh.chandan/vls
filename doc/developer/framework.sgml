<chapter><title>VLS framework</title>

<sect1><title>Overview</title>

<para>
The VLS framework is a set of low-level classes used by every other classes.
Currently, it can handle:

<itemizedlist>
  <listitem>
    <para>Buffers (<filename>src/core/buffers.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Configuration files (<filename>src/core/settings.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Exceptions (<filename>src/core/exception.*</filename>)</para>
  </listitem> 
  <listitem>
    <para>Files (<filename>src/core/file.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Hash tables (<filename>src/core/hashtable.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Dynamic libraries (<filename>src/core/library.*</filename>)</para>
  </listitem>
  <listitem>
    <para>IO streams (<filename>src/core/stream.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Lists (<filename>src/core/list.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Logging (<filename>src/core/log.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Modules (<filename>src/core/module.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Parsing (<filename>src/core/parsers.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Session handling (<filename>src/core/network.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Serialization (<filename>src/core/serialization.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Sockets (<filename>src/core/socket.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Stacks (<filename>src/core/stack.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Threads (<filename>src/core/thread.*</filename>)</para>
  </listitem>
  <listitem>
    <para>Vectors (<filename>src/core/vector.*</filename>)</para>
  </listitem>
</itemizedlist>
</para>

</sect1>

<sect1><title>Threads</title>

<para>
A class that must be run in a new thread has to extend the C_Thread
abstract class. Pure virtual functions that must be implemented by such a class
are:

<variablelist>
  <varlistentry>
    <term><function>void InitWork()</function></term>
    <listitem><para>
      This method is called before the thread is created.
    </para></listitem>
  </varlistentry>
  <varlistentry>
    <term><function>void DoWork()</function></term>
    <listitem><para>
      It is called just after the thread is created. The main 
      loop of the class should be put here.
    </para></listitem>
  </varlistentry>
  <varlistentry>
    <term><function>void StopWork()</function></term>
    <listitem><para>
      This method is called when the thread is to be stopped or canceled.
    </para></listitem>
  </varlistentry>
  <varlistentry>
    <term><function>void CleanWork()</function></term>
    <listitem><para>
      This method is called after the shutdown of the thread.
    </para></listitem>
  </varlistentry>
</variablelist>

To create the thread, the derived class just has to call the <function>Create()
</function> method. To stop the thread, just call <function>Stop()</function>.
</para>

</sect1>

<sect1><title>Modules</title>

<para>
Whenever a class is optional, it should be implemented as a module. A module
can be:
<itemizedlist>
  <listitem>
    <para>a built-in module: it means the module is linked statically, though
    classes are of course instanciated dynamically.</para>
  </listitem>
  <listitem>
    <para>a plug-in module: in this case, the classes are linked in a shared
    library, and loaded dynamically at runtime.</para>
  </listitem>
</itemizedlist>

Here are the differents steps required to add a new module:

<orderedlist>
  <listitem><para>
    Declare a new module type. Indeed, modules of the same type are derived
    from the same class, called a "virtual module". For instance, DvdMpegReader
    and FileMpegReader are both derived from the module type "MpegReader".
    To declare a class as a virtual module, use the DECLARE_VIRTUAL_MODULE
    macro:
    
    <programlisting>
   
// in classtype.h
   
C_<emphasis>ClassType</emphasis> 
{
 public:
  C_<emphasis>ClassType</emphasis>(C_Module *pModule, <emphasis>argType</emphasis> val)
  // Class declaration
};
    
DECLARE_VIRTUAL_MODULE(<emphasis>ClassType</emphasis>, "<emphasis>strType</emphasis>", <emphasis>argType</emphasis>);
    </programlisting>
    
    The virtual module constructor must have two arguments: C_Module *pModule 
    and <emphasis>argType</emphasis> val, where <emphasis>argType</emphasis> can
    be anything (defined in the DECLARE_VIRTUAL_MODULE macro). The virtual
    module destructor have to call <function>pModule->Unref()</function>. 
    <emphasis>strType</emphasis> is the name given to the module type.
  </para></listitem>
  <listitem><para>
  Declare the module itself. Like virtual modules, use the DECLARE_MODULE macro
  after the class declaration:

    <programlisting>

// in classname.h

C_<emphasis>ClassName</emphasis><emphasis>ClassType</emphasis> : public <emphasis>ClassType</emphasis>
{
 public:
  C_<emphasis>ClassName</emphasis><emphasis>ClassType</emphasis>(C_Module *pModule, <emphasis>argType</emphasis> val)
  // Class declaration
};
    
DECLARE_MODULE(<emphasis>ClassName</emphasis>, <emphasis>ClassType</emphasis>, "<emphasis>strType</emphasis>", <emphasis>argType</emphasis>);
    </programlisting>
 
  </para></listitem>
  
  <listitem><para>
    Add some declarations for libraries and built-in modules:
    <programlisting>

// in classname.cpp

//------------------------------------------------------------------------------
// Library declaration
//------------------------------------------------------------------------------
#ifdef PIC
GENERATE_LIB_ARGS(C_<emphasis>ClassName</emphasis><emphasis>ClassType</emphasis>Module, handle);
#endif


//------------------------------------------------------------------------------
// Builtin declaration
//------------------------------------------------------------------------------
#ifndef PIC
C_Module* NewBuiltin_<emphasis>classname</emphasis><emphasis>classtype</emphasis>(handle hLog)
{
  return new C_<emphasis>ClassName</emphasis><emphasis>ClassType</emphasis>Module(hLog);
}
#endif
    </programlisting>
  
  </para></listitem>
  
  <listitem><para>
  Add the module in <filename>configure.in</filename>. For a built-in module,
  add the file name in the line BUILTINS=... For a plug-in module, it is more
  complicated; look what is done for existing plug-ins.
  </para></listitem>
</orderedlist>

</para>

</sect1>

</chapter>

